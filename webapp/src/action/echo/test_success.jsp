﻿<!DOCTYPE html>
<%--支持的jsp与java web开发中的jsp比较类似，只是不支持标签，觉得没用。因为服务器端的任务是产生数据，不是做ui，简单的支持嵌入c++，及方便提取数据就行了--%>
<%!
	const char * g_poem[] = 
	{
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。"
	};
%>
<%
	const char * url = pReqTag->str( "http_url" );
%>
<html>
  <head>
  	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
  	<link type="text/css" rel="stylesheet" href="../../../html/css/web.css">
  	<title>test_success</title>
  </head>
   
  <body>
    <img src="../../../html/img/flower.jpg" border="0" width=100% height=213px>
    <a href="../../../html/baidu.htm">百度</a><br />
    <a href="../../../html/test.htm">ogre</a><br/>
    <%--loop demo--%>
    <% for( int i = 0; i < sizeof( g_poem ) / sizeof( g_poem[0] ); ++i ) { %>
        poem <%= i %> = <%= g_poem[i] %><br/>
    <% } %>
    <%--系统支持几个固定名称变量：task,request,response,param,tag,application,http_url是http请求时，程序生成的标签名--%>
    supper variable, url = <%$ request.tag.http_url %><br/>
    <%--对于所有变量，也可以这样访问，这样更方便。框架会帮助查找这个名称的变量--%>
    <%--查找顺序为：-> 当前action或page -> action同步调用栈（按action调用栈出栈顺序） -> request.param -> request.tag -> application--%>
    <%--见代码task.cpp::helper_vt()--%>
    supper variable, url = <%$ http_url %><br/>
    <%--variable from action--%>
    xoskit = <%$ xoskit %><br/>
    <p><%= url %></p>
  	<div id="container">
          <div id="banner">banner</div>
          <div id="link">link</div>
          <div id="content">content</div>
          <div id="footer">footer</div>
  	</div>
  </body>
</html>
