
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 
:: 关掉命令显示
:: 
@echo off

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 
:: 全是局部变量
::
Setlocal

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::
:: 设置环境变量
:: 
call "%VS140COMNTOOLS%vsvars32.bat"

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 
:: 设置默认值
:: 
set "cur_path=%cd%"

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::
:: 获得用户设置的编译模式( U_R_MT U_D_MD )和编译类型( rebuild build )
::
if "%1" EQU "r" set "COMPILE_TYPE=rebuild"
if "%1" EQU "b" set "COMPILE_TYPE=build"
if "%2" EQU "r" set "COMPILE_MODE=U_R_MT"
if "%2" EQU "d" set "COMPILE_MODE=U_D_MD"
if "%2" EQU "a" set "COMPILE_MODE=a"

if "%COMPILE_TYPE%" EQU "" ( set "COMPILE_TYPE=rebuild" )
if "%COMPILE_MODE%" EQU "" ( set "COMPILE_MODE=a" )

echo cur_path = %cur_path%
echo compile type = %COMPILE_TYPE%
echo compile mode = %COMPILE_MODE%

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 
:: 编译跨平台库
::

:: devenv "..\xos_core\xos_win\project\xos_core_win_%COMPILE_PROJECT%.vcxproj" /%COMPILE_TYPE% "%COMPILE_MODE%|x86" /project xos_core_win_%COMPILE_PROJECT% /projectconfig "%COMPILE_MODE%|Win32"
:: if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)

:: devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "%COMPILE_MODE%|x86"

if "%COMPILE_MODE%" EQU "a" (
	echo -------------------------------compile command-------------------------------
	echo devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_D_MD|x86"
	devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_D_MD|x86"
	if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)

	echo -------------------------------compile command-------------------------------
	echo devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_R_MT|x86"
	devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_R_MT|x86"
	if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)
	
	echo -------------------------------compile command-------------------------------
	echo devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_D_MD|x64"
	devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_D_MD|x64"
	if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)
	
	echo -------------------------------compile command-------------------------------
	echo devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_R_MT|x64"
	devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "U_R_MT|x64"
	if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)
) else (
	echo -------------------------------compile command-------------------------------
	echo devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "%COMPILE_MODE%|x86"
	devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "%COMPILE_MODE%|x86"
	if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)
	
	echo -------------------------------compile command-------------------------------
	echo devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "%COMPILE_MODE%|x64"
	devenv "..\sln\vs_2015.sln" /%COMPILE_TYPE% "%COMPILE_MODE%|x64"
	if %ERRORLEVEL% equ 0 ( echo %date% %time% ) else (goto :eof)
)

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: 
:: 全是局部变量
::
Endlocal
