/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_PROC_HELPER_PROC_H__
#define __HELPER_PROC_HELPER_PROC_H__

namespace jsp
{

    class helper_proc
    {
    public:
        helper_proc();
        ~helper_proc();

    public:
        int proc( xos::i_msg *& pMsg );

    };

} // jsp

#endif // __HELPER_PROC_HELPER_PROC_H__
