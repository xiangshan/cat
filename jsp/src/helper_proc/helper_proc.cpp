/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "../helper_pass_through/head.h"
#include "../helper_jsp/head.h"
#include "helper_proc.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    helper_proc::helper_proc()
    {
    }

    helper_proc::~helper_proc()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int helper_proc::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsgType = msg::get_msg_type( pMsg );

        switch( nMsgType )
        {
        case MSG_TYPE_FLUSH:
            {
                mgr::xos()->log()->flush();
                msg::notify( pMsg, MSG_TYPE_QUIT, true );
            }
            break;
        case MSG_PASS_THROUGH:
            {
                helper_pass_through obj;
                obj.proc( pMsg );
            }
            break;
        case MSG_TYPE_QUIT:
            {
                state::set_stopped();
            }
            break;
        case MSG_TYPE_WORK:
            {
                helper_jsp obj;
                obj.proc( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        xos_stl::release_interface( pMsg );

        return ret;
    }

} // jsp
