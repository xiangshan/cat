/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "thread_lock.h"

namespace jsp
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    thread_lock::thread_lock()
    {
        mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_MUTX_LOCK, ( void ** )&m_pLock );
    }

    thread_lock::~thread_lock()
    {
        if( m_pLock )
        {
            m_pLock->release();
            m_pLock = 0;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int thread_lock::un_lock()
    {
        int ret = 0;

        if( m_pLock )
        {
            m_pLock->un_lock();
        }

        return ret;
    }

    int thread_lock::lock()
    {
        int ret = 0;

        if( m_pLock )
        {
            m_pLock->lock();
        }

        return ret;
    }
} // jsp
