/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_pass_through/head.h"
#include "../data_struct/pool.h"
#include "../macro/head.h"
#include "../xos/head.h"
#include "hook.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    hook::hook()
    {
    }

    hook::~hook()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int hook::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = os::init();
        }

        if( 0 == ret )
        {
            mgr::xos()->log()->add_log_module( LOG_NAME, 1 );
            mgr::xos()->log()->add_log_module( LOG_NAME, 2 );
            mgr::xos()->log()->add_log_module( LOG_NAME, 3 );
            mgr::xos()->log()->add_log_module( LOG_NAME, 4 );
        }

        if( 0 == ret )
        {
            ret = helper_pass_through::init();
        }

        if( 0 == ret )
        {
            ret = pool::init();
        }

        return ret;
    }

    int hook::term()
    {
        int ret = 0;

        helper_pass_through::term();
        pool::term();
        os::term();

        return ret;
    }

} // jsp
