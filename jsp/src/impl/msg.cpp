/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_proc/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "msg.h"
#include "mgr.h"

namespace jsp
{
    namespace msg
    {
        int notify( xos::i_msg *& pMsg, int nMsg, bool bPassThrough )
        {
            int ret = 0;
            fill( pMsg, nMsg );
            ret = mgr::notify( pMsg, bPassThrough );
            return ret;
        }

        int notify( int nMsg, bool bPassThrough )
        {
            int ret = 0;
            xos::i_msg * pMsg = mgr::xos()->msg();
            ret = notify( pMsg, nMsg, bPassThrough );
            return ret;
        }

        int fill( xos::i_msg * pMsg, int nMsg )
        {
            int ret = 0;
            pMsg->set_int( 0, nMsg );
            return ret;
        }

        int get_msg_type( xos::i_msg * pMsg )
        {
            int ret = pMsg->get_int( 0, 0 );
            return ret;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // ��Ϣ��Ӧ����
        // 

        int on_msg( xos::i_msg *& pMsg )
        {
            int ret = 0;
            helper_proc obj;
            obj.proc( pMsg );
            return ret;
        }

    } // msg
} // xos
