/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_MSG_H__
#define __IMPL_MSG_H__

namespace jsp
{
    namespace msg
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int notify( xos::i_msg *& pMsg, int nMsg, bool bPassThrough );
        int notify( int nMsg, bool bPassThrough );

        int fill( xos::i_msg * pMsg, int nMsg );

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int get_msg_type( xos::i_msg * pMsg );

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // ��Ϣ��Ӧ����
        // 
        int on_msg( xos::i_msg *& pMsg );

    } // msg
} // jsp

#endif // __IMPL_MSG_H__
