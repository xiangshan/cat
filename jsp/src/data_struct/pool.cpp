/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "head.h"
#include "pool.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    pool::pool()
    {
    }

    pool::~pool()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int pool::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = data::init_all();
        }

        return ret;
    }

    int pool::term()
    {
        int ret = 0;

        data::term_all();

        return ret;
    }

} // jsp
