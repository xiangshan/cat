/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_MSG_H__
#define __MSG_MSG_H__

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    enum enumJspType
    {
        JSP_SUPPER_VARIABLE,
        JSP_CPLUSPLUS,
        JSP_VARIABLE,
        JSP_DECLARE,
        JSP_HTML,
        JSP_TEXT,
        JSP_END
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 消息类型定义
    // 
    enum enumMsgType
    {
        MSG_TYPE_BEGIN,

        MSG_PASS_THROUGH,
        MSG_TYPE_FLUSH,
        MSG_TYPE_WORK,
        MSG_TYPE_QUIT,

        MSG_TYPE_END
    };
}

#endif // __MSG_MSG_H__
