/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_JSP_JSP_CMD_H__
#define __HELPER_JSP_JSP_CMD_H__

namespace jsp
{

    class helper_jsp;
    class data;
    class jsp_cmd
    {
    public:
        jsp_cmd( helper_jsp * pJsp );
        ~jsp_cmd();

    protected:
        int load_file( char * lpszInfo, int nSize, const char * lpszFile );

    public:
        int parse( xos::i_big_buf * pBigBuf, xos::i_buf * pBuf );

    protected:
        helper_jsp * m_pJsp;

    };

} // jsp

#endif // __HELPER_JSP_JSP_CMD_H__
