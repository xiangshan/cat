/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_supper_variable.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_supper_variable::jsp_supper_variable( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_supper_variable::~jsp_supper_variable()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    // 
    // <%= variable %>
    // 
    int jsp_supper_variable::parse( xos::i_buf * pBuf )
    {
        int result = 0;
        int ret = 0;

        xos_common::i_misc * pMisc = mgr::xos()->common_misc();
        xos::i_crt * pCrt = mgr::xos()->crt();
        const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
        const char * lpszBegin = 0;
        const char * lpszEnd = 0;
        int nLen = pBuf->get_len( 0 );
        char buf[4096];

        if( ( 0 == ret ) && ( 0 != pMisc->begin_with( lpszBuf, "<%$" ) ) )
        {
            ret = 1;
        }

        if( ( 0 == ret ) && ( 0 != pMisc->end_with( lpszBuf, "%>" ) ) )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            lpszEnd = lpszBuf + nLen - 2;
            lpszBegin = lpszBuf + 3;
        }

        if( 0 == ret )
        {
            int len = ( int )( lpszEnd - lpszBegin );
            buf[0] = 0;
            pCrt->strncpy( buf, sizeof( buf ), lpszBegin, len );
            mgr::xos()->common_misc()->trim( buf, " " );
        }

        if( 0 == ret )
        {
            data * pD = m_pJsp->push_item( lpszBuf, nLen, JSP_SUPPER_VARIABLE );
            pD->m_str = buf;
        }

        if( 0 == ret )
        {
            result = 1;
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    int jsp_supper_variable::to_function( data * pData )
    {
        int ret = 0;

        const char * lpszInfo = "\r\n\
    supper_variable_to_html( pTask, \"%s\" );";

        char buf[4096];
        int len = mgr::xos()->crt()->sprintf( buf, sizeof( buf ), lpszInfo, pData->m_str.c_str() );
        m_pJsp->push_text( buf, len, JSP_TEXT );

        return ret;
    }

    int jsp_supper_variable::to_array( data * pData )
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszInfo = "\r\n\
0";
        m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );

        return ret;
    }

} // jsp
