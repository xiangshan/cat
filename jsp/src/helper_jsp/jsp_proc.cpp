/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"

#include "jsp_supper_variable.h"
#include "jsp_cplusplus.h"
#include "jsp_variable.h"
#include "jsp_declare.h"
#include "jsp_comment.h"
#include "jsp_html.h"

#include "helper_jsp.h"
#include "jsp_proc.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_proc::jsp_proc( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_proc::~jsp_proc()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int jsp_proc::proc()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = add_head();
        }

        if( 0 == ret )
        {
            ret = add_array();
        }

        if( 0 == ret )
        {
            ret = add_declare();
        }

        if( 0 == ret )
        {
            ret = add_function_head();
        }

        if( 0 == ret )
        {
            ret = to_function();
        }

        if( 0 == ret )
        {
            ret = add_function_tail();
        }

        if( 0 == ret )
        {
            ret = add_tail();
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int jsp_proc::add_head()
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();
        const char * lpszInfo = "\
/*----------------------------------------------------------------------------------------\r\n\
*\r\n\
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.\r\n\
*  https://gitee.com/helloworldghh/cat.git\r\n\
*  Use of this source code is governed by a MIT license\r\n\
*  that can be found in the License file.\r\n\
*\r\n\
----------------------------------------------------------------------------------------*/\r\n\
#include \"../import/head.h\"\r\n\
#include \"jsp_functions.h\"";

        m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );

        return ret;
    }

    int jsp_proc::add_tail()
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();
        const char * lpszInfo = "\r\n";
        m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );

        return ret;
    }

    int jsp_proc::add_array()
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();
        data::LIST * pList = m_pJsp->item();

        if( 0 == ret )
        {
            const char * lpszInfo = "\r\n\r\n\
const int JSP_BODY_NUM = %d;\r\n\
int g_jsp_body_num = JSP_BODY_NUM;\r\n\
xos::i_buf * g_pHtmlBuf[JSP_BODY_NUM] = { 0 };\r\n\
const char * g_pszBase64Html[JSP_BODY_NUM] = \r\n\
{";
            int nNum = ( int )pList->size();
            char buf[4096];
            int len = mgr::xos()->crt()->sprintf( buf, sizeof( buf ), lpszInfo, nNum );
            m_pJsp->push_text( buf, len, JSP_TEXT );
        }

        if( 0 == ret )
        {
            to_array();
        }

        if( 0 == ret )
        {
            const char * lpszInfo = "\r\n\
};";
            m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );
        }

        return ret;
    }

    int jsp_proc::add_declare()
    {
        int ret = 0;

        data::LIST * pList = m_pJsp->declare();
        jsp_declare obj( m_pJsp );

        for( data::ITER iter = pList->begin(); iter != pList->end(); ++iter )
        {
            data * pD = *iter;
            obj.to_declare( pD );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int jsp_proc::to_function()
    {
        int ret = 0;

        data::LIST * pList = m_pJsp->item();
        int nIndex = 0;

        for( data::ITER iter = pList->begin(); iter != pList->end(); ++iter )
        {
            data * pD = *iter;
            pD->m_nIndex = nIndex++;
            switch( pD->m_eType )
            {
            case JSP_SUPPER_VARIABLE:
                {
                    jsp_supper_variable obj( m_pJsp );
                    obj.to_function( pD );
                }
                break;
            case JSP_CPLUSPLUS:
                {
                    jsp_cplusplus obj( m_pJsp );
                    obj.to_function( pD );
                }
                break;
            case JSP_VARIABLE:
                {
                    jsp_variable obj( m_pJsp );
                    obj.to_function( pD );
                }
                break;
            case JSP_HTML:
                {
                    jsp_html obj( m_pJsp );
                    obj.to_function( pD );
                }
                break;
            default:
                {
                }
                break;
            }
        }

        return ret;
    }

    int jsp_proc::to_array()
    {
        int ret = 0;

        data::LIST * pList = m_pJsp->item();
        bool bFirst = true;

        for( data::ITER iter = pList->begin(); iter != pList->end(); ++iter )
        {
            data * pD = *iter;
            if( !bFirst )
            {
                m_pJsp->push_text( ",", 1, JSP_TEXT );
            }
            else
            {
                bFirst = false;
            }
            switch( pD->m_eType )
            {
            case JSP_SUPPER_VARIABLE:
                {
                    jsp_supper_variable obj( m_pJsp );
                    obj.to_array( pD );
                }
                break;
            case JSP_CPLUSPLUS:
                {
                    jsp_cplusplus obj( m_pJsp );
                    obj.to_array( pD );
                }
                break;
            case JSP_VARIABLE:
                {
                    jsp_variable obj( m_pJsp );
                    obj.to_array( pD );
                }
                break;
            case JSP_HTML:
                {
                    jsp_html obj( m_pJsp );
                    obj.to_array( pD );
                }
                break;
            default:
                {
                }
                break;
            }
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int jsp_proc::add_function_head()
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszInfo = "\r\n\r\n\
void page_render( icat::i_task * pTask,\r\n\
    icat::i_request * pRequest,\r\n\
    icat::i_response * pResponse,\r\n\
    xos_common::i_property * pReqTag,\r\n\
    xos_common::i_property * pResTag )\r\n\
{\r\n\
    using namespace jsp;";

        m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );

        return ret;
    }

    int jsp_proc::add_function_tail()
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszInfo = "\r\n\
}";

        m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );

        return ret;
    }

} // jsp
