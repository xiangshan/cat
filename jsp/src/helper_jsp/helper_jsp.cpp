/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"

#include "jsp_parse_tag.h"
#include "jsp_cleaner.h"
#include "jsp_proc.h"
#include "helper_jsp.h"

#include "jsp_supper_variable.h"
#include "jsp_cplusplus.h"
#include "jsp_variable.h"
#include "jsp_declare.h"
#include "jsp_comment.h"
#include "jsp_html.h"
#include "jsp_cmd.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    helper_jsp::helper_jsp()
    {
        m_szDstFile[0] = 0;
        m_szPath[0] = 0;

        m_eType = JSP_HTML;
        m_pBigBuf = 0;
        m_pBuf = 0;
    }

    helper_jsp::~helper_jsp()
    {
        xos_stl::release_interface( m_pBigBuf );
        xos_stl::release_interface( m_pBuf );

        m_declare_list.put_back_to_pool( true );
        m_text_list.put_back_to_pool( true );
        m_item_list.put_back_to_pool( true );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int helper_jsp::proc( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( 0 == ret )
        {
            const char ** argv = ( const char ** )pMsg->get_void( 0, 0 );
            int argc = pMsg->get_int( 1, 0 );
            ret = load_jsp( argc, argv );
        }

        if( 0 == ret )
        {
            m_pBuf = mgr::xos()->buf();
        }

        // 扫描标签之内的东西
        while( ( 0 == ret ) && ( JSP_END != m_eType ) )
        {
            parse();
        }

        if( 0 == ret )
        {
            jsp_proc obj( this );
            obj.proc();
        }

        if( 0 == ret )
        {
            save();
        }

        msg::notify( pMsg, MSG_TYPE_FLUSH, false );
        xos_stl::release_interface( m_pBigBuf );
        xos_stl::release_interface( m_pBuf );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    // argv[1] : src jsp file
    int helper_jsp::load_jsp( int argc, const char * argv[] )
    {
        int ret = 0;

        xos::i_file * pFile = 0;

        if( ( 0 == ret ) && ( 2 != argc - 1 ) )
        {
            LOG4( "usage : jsp.exe index.jsp index.cpp" );
            ret = 1;
        }

        if( ( 0 == ret ) && ( 0 != mgr::xos()->common_misc()->end_with( argv[1], ".jsp" ) ) )
        {
            LOG4( "usage : jsp.exe index.jsp index.cpp" );
            ret = 1;
        }

        if( 0 == ret )
        {
            mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_FILE, ( void** )&pFile );
            ret = pFile->open( argv[1], xos::i_file::FILE_READ, xos::i_file::SHARE_READ, xos::i_file::FILE_OPEN );
        }

        if( 0 == ret )
        {
            mgr::xos()->common_misc()->split_full_file( argv[1], m_szPath, sizeof( m_szPath ), 0 );
            mgr::xos()->crt()->strcpy( m_szDstFile, sizeof( m_szDstFile ), argv[2] );
        }

        if( ( 0 == ret ) && !m_pBigBuf )
        {
            m_pBigBuf = mgr::xos()->big_buf();
        }

        if( 0 == ret )
        {
            char * lpszBuf = m_pBigBuf->get_data( 0, 0, 0 );
            int nSize = xos::i_big_buf::BUF_SIZE;
            int len = pFile->read( lpszBuf, nSize );
            if( len > 0 )
            {
                m_pBigBuf->set_len( len );
                lpszBuf[len] = 0;
            }
            else
            {
                ret = 1;
            }
        }

        xos_stl::release_interface( pFile );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    // 一个<>一个<>的读
    int helper_jsp::parse()
    {
        int ret = 0;

        // 保存html标签之外的字节
        if( 0 == ret )
        {
            jsp_cleaner obj( this );
            obj.proc( m_pBigBuf );
        }

        if( 0 == ret )
        {
            jsp_parse_tag obj( this );
            ret = obj.proc( m_pBigBuf, m_pBuf );
        }

        if( 0 != ret )
        {
            m_eType = JSP_END;
        }

        if( 0 == ret )
        {
            jsp_cmd obj( this );
            ret = obj.parse( m_pBigBuf, m_pBuf );
        }

        if( 0 == ret )
        {
            jsp_comment obj( this );
            ret = obj.parse( m_pBuf );
        }

        if( 0 == ret )
        {
            jsp_supper_variable obj( this );
            ret = obj.parse( m_pBuf );
        }

        if( 0 == ret )
        {
            jsp_variable obj( this );
            ret = obj.parse( m_pBuf );
        }

        if( 0 == ret )
        {
            jsp_declare obj( this );
            ret = obj.parse( m_pBuf );
        }

        if( 0 == ret )
        {
            jsp_cplusplus obj( this );
            ret = obj.parse( m_pBuf );
        }

        if( 0 == ret )
        {
            jsp_html obj( this );
            ret = obj.parse( m_pBuf );
        }

        m_pBuf->set_len( 0 );
        m_pBuf->set_pos( 0 );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int helper_jsp::push_data( data::LIST * pList, const char * lpszData, int nLen, enumJspType eType )
    {
        int ret = 0;

        int nFreeBufLen = 0;
        data * pD = 0;

        if( pList->size() > 0 )
        {
            pD = pList->back();
        }

        // base64之后，字串长度会变为原来的4/3，留出空间，确保m_item_list与m_text_list等长，这样索引就相同
        if( pD )
        {
            nFreeBufLen = xos::i_buf::BUF_SIZE - 1;
            if( JSP_HTML == eType )
            {
                nFreeBufLen = nFreeBufLen * 3 / 4 - pD->m_pBuf->get_len( 0 );
            }
            else
            {
                nFreeBufLen = nFreeBufLen - pD->m_pBuf->get_len( 0 );
            }
        }

        if( !pD || ( eType != JSP_HTML ) || ( eType != pD->m_eType ) || ( nFreeBufLen < nLen ) )
        {
            pD = data::get_item_from_pool( true );
            pD->init();
            pD->m_eType = eType;
            pList->push_back( pD );
        }

        {
            pD->m_pBuf->add_data( lpszData, nLen );
        }

        return ret;
    }

    data * helper_jsp::push_declare( const char * lpszData, int nLen, enumJspType eType )
    {
        data::LIST * pList = &m_declare_list;
        data * pRet = 0;
        push_data( pList, lpszData, nLen, eType );
        pRet = pList->back();
        return pRet;
    }

    data * helper_jsp::push_item( const char * lpszData, int nLen, enumJspType eType )
    {
        data::LIST * pList = &m_item_list;
        data * pRet = 0;
        push_data( pList, lpszData, nLen, eType );
        pRet = pList->back();
        return pRet;
    }

    data * helper_jsp::push_text( const char * lpszData, int nLen, enumJspType eType )
    {
        data::LIST * pList = &m_text_list;
        data * pRet = 0;
        push_data( pList, lpszData, nLen, eType );
        pRet = pList->back();
        return pRet;
    }

    data::LIST * helper_jsp::declare()
    {
        return &m_declare_list;
    }

    data::LIST * helper_jsp::item()
    {
        return &m_item_list;
    }

    data::LIST * helper_jsp::text()
    {
        return &m_text_list;
    }

    const char * helper_jsp::path()
    {
        return m_szPath;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int helper_jsp::save()
    {
        int ret = 0;

        xos::i_file * pFile = 0;

        if( 0 == ret )
        {
            xos::i_dir * pDir = 0;
            mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_DIR, ( void** )&pDir );
            pDir->delete_file( m_szDstFile );
            xos_stl::release_interface( pDir );
        }

        if( 0 == ret )
        {
            mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_FILE, ( void** )&pFile );
            ret = pFile->open( m_szDstFile, xos::i_file::FILE_WRITE, 0, xos::i_file::FILE_CREATE );
        }

        for( data::ITER iter = m_text_list.begin(); ( 0 == ret ) && ( iter != m_text_list.end() ); ++iter )
        {
            data * pD = *iter;
            xos::i_buf * pBuf = pD->m_pBuf;
            const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
            int len = pBuf->get_len( 0 );
            pFile->write( lpszBuf, len );
        }

        xos_stl::release_interface( pFile );

        return ret;
    }

} // jsp
