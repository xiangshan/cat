/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_JSP_HELPER_JSP_H__
#define __HELPER_JSP_HELPER_JSP_H__

#include "../data_struct/data.h"
#include "../msg/head.h"

namespace jsp
{

    class helper_jsp
    {
    public:
        helper_jsp();
        ~helper_jsp();

    protected:
        int push_data( data::LIST * pList, const char * lpszData, int nLen, enumJspType eType );
        int load_jsp( int argc, const char * argv[] );
        int parse();
        int save();

    public:
        data * push_declare( const char * lpszData, int nLen, enumJspType eType );
        data * push_item( const char * lpszData, int nLen, enumJspType eType );
        data * push_text( const char * lpszData, int nLen, enumJspType eType );
        int proc( xos::i_msg *& pMsg );

        data::LIST * declare();
        data::LIST * item();
        data::LIST * text();

        const char * path();

    protected:
        xos::i_big_buf * m_pBigBuf;
        xos::i_buf * m_pBuf;

        data::LIST m_declare_list;
        data::LIST m_text_list;
        data::LIST m_item_list;

        char m_szDstFile[1024];
        char m_szPath[1024];
        enumJspType m_eType;
    };

} // jsp

#endif // __HELPER_JSP_HELPER_JSP_H__
