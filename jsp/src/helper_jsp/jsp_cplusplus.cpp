/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_cplusplus.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_cplusplus::jsp_cplusplus( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_cplusplus::~jsp_cplusplus()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    // 
    // <% variable %>
    // 
    int jsp_cplusplus::parse( xos::i_buf * pBuf )
    {
        int result = 0;
        int ret = 0;

        const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
        const char * lpszBegin = 0;
        const char * lpszEnd = 0;
        int nLen = pBuf->get_len( 0 );
        xos::i_crt * pCrt = mgr::xos()->crt();
        char buf[40960];

        if( 0 == ret )
        {
            lpszBegin = pCrt->strstr( lpszBuf, "<%", true );
            if( !lpszBegin || ( lpszBegin != lpszBuf ) )
            {
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            lpszEnd = pCrt->strstr( lpszBegin, "%>", true );
            if( !lpszEnd )
            {
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            std::string str;
            int len = ( int )( lpszEnd - lpszBegin - 2 );
            buf[0] = 0;
            pCrt->strncpy( buf, sizeof( buf ), lpszBegin + 2, len );
        }

        if( 0 == ret )
        {
            data * pD = m_pJsp->push_item( lpszBuf, nLen, JSP_CPLUSPLUS );
            pD->m_str = buf;
        }

        if( 0 == ret )
        {
            result = 1;
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    int jsp_cplusplus::to_function( data * pData )
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszInfo_begin = "\r\n\
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ begin\r\n";
        const char * lpszInfo_end = "\r\n\
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ end";

        m_pJsp->push_text( lpszInfo_begin, pCrt->strlen( lpszInfo_begin ), JSP_TEXT );
        m_pJsp->push_text( pData->m_str.c_str(), ( int )pData->m_str.length(), JSP_TEXT );
        m_pJsp->push_text( lpszInfo_end, pCrt->strlen( lpszInfo_end ), JSP_TEXT );

        return ret;
    }

    int jsp_cplusplus::to_array( data * pData )
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszInfo = "\r\n\
0";
        m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );

        return ret;
    }

} // jsp
