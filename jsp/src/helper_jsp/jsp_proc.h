/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_JSP_JSP_PROC_H__
#define __HELPER_JSP_JSP_PROC_H__

#include "../data_struct/data.h"
#include "../msg/head.h"

namespace jsp
{
    class helper_jsp;

    class jsp_proc
    {
    public:
        jsp_proc( helper_jsp * pJsp );
        ~jsp_proc();

    protected:
        int add_head();

        int add_array();
        int add_declare();

        int to_function();
        int to_array();

        int add_function_head();
        int add_function_tail();

        int add_tail();

    public:
        int proc();

    protected:
        helper_jsp * m_pJsp;

    };

} // jsp

#endif // __HELPER_JSP_JSP_PROC_H__
