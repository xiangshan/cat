/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_cleaner.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_cleaner::jsp_cleaner( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_cleaner::~jsp_cleaner()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // clean out html tag space.
    // 
    int jsp_cleaner::proc( xos::i_big_buf * pBuf )
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();
        const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
        int nLen = pBuf->get_len( 0 );
        int nPos = pBuf->get_pos( 0 );
        const char * lpszEnd = 0;

        if( 0 == ret )
        {
            lpszEnd = pCrt->strstr( lpszBuf, "<", true );
            if( !lpszEnd )
            {
                lpszEnd = lpszBuf + nLen;
                ret = 1;
            }
        }

        if( ( 0 == ret ) && ( lpszBuf == lpszEnd ) )
        {
            ret = 2;
        }

        if( ( 0 == ret ) || ( 1 == ret ) )
        {
            int len = ( int )( lpszEnd - lpszBuf );
            m_pJsp->push_item( lpszBuf, len, JSP_HTML );
            nLen -= len;
            nPos += len;
            pBuf->set_pos( nPos );
            pBuf->set_len( nLen );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

} // jsp
