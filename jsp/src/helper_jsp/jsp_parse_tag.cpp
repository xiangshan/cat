/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_parse_tag.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_parse_tag::jsp_parse_tag( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_parse_tag::~jsp_parse_tag()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // clean out html tag space.
    // 
    int jsp_parse_tag::proc( xos::i_big_buf * pBigBuf, xos::i_buf * pBuf )
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();
        const char * lpszBuf = pBigBuf->get_data( 0, 0, 0 );
        int nLen = pBigBuf->get_len( 0 );
        int nPos = pBigBuf->get_pos( 0 );
        const char * lpszEnd = 0;
        bool bIsCode = false;

        if( ( 0 == ret ) && ( ( 0 == nLen ) || ( '<' != lpszBuf[0] ) ) )
        {
            ret = 1;
        }

        if( ( 0 == ret ) && ( '%' == lpszBuf[1] ) )
        {
            bIsCode = true;
        }

        if( ( 0 == ret ) && bIsCode )
        {
            lpszEnd = pCrt->strstr( lpszBuf, "%>", true );
            if( lpszEnd )
            {
                lpszEnd++;
            }
            else
            {
                ret = 1;
            }
        }

        if( ( 0 == ret ) && !bIsCode )
        {
            lpszEnd = pCrt->strstr( lpszBuf, ">", true );
            if( !lpszEnd )
            {
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            char * lpszB = pBuf->get_data( 0, 0, 0 );
            int len = ( int )( lpszEnd - lpszBuf + 1 );
            pBuf->set_data( lpszBuf, len );
            lpszB[len] = 0;
            nLen -= len;
            nPos += len;
            pBigBuf->set_pos( nPos );
            pBigBuf->set_len( nLen );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

} // jsp
