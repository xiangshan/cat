/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_JSP_JSP_CLEANER_H__
#define __HELPER_JSP_JSP_CLEANER_H__

namespace jsp
{

    class helper_jsp;
    class jsp_cleaner
    {
    public:
        jsp_cleaner( helper_jsp * pJsp );
        ~jsp_cleaner();

    public:
        int proc( xos::i_big_buf * pBuf );

    protected:

    protected:
        helper_jsp * m_pJsp;

    };

} // jsp

#endif // __HELPER_JSP_JSP_CLEANER_H__
