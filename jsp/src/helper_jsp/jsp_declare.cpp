/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_declare.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_declare::jsp_declare( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_declare::~jsp_declare()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    // 
    // <%= variable %>
    // 
    int jsp_declare::parse( xos::i_buf * pBuf )
    {
        int result = 0;
        int ret = 0;

        xos_common::i_misc * pMisc = mgr::xos()->common_misc();
        xos::i_crt * pCrt = mgr::xos()->crt();
        const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
        const char * lpszBegin = 0;
        const char * lpszEnd = 0;
        int nLen = pBuf->get_len( 0 );
        char buf[40960];

        if( ( 0 == ret ) && ( 0 != pMisc->begin_with( lpszBuf, "<%!" ) ) )
        {
            ret = 1;
        }

        if( ( 0 == ret ) && ( 0 != pMisc->end_with( lpszBuf, "%>" ) ) )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            lpszEnd = lpszBuf + nLen - 2;
            lpszBegin = lpszBuf + 3;
        }

        if( 0 == ret )
        {
            int len = ( int )( lpszEnd - lpszBegin );
            buf[0] = 0;
            pCrt->strncpy( buf, sizeof( buf ), lpszBegin, len );
        }

        if( 0 == ret )
        {
            data * pD = m_pJsp->push_declare( lpszBuf, nLen, JSP_DECLARE );
            pD->m_str = buf;
        }

        if( 0 == ret )
        {
            result = 1;
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    int jsp_declare::to_declare( data * pData )
    {
        int ret = 0;

        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszInfo_begin = "\r\n\
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ declare begin\r\n";
        const char * lpszInfo_end = "\r\n\
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ declare end";

        m_pJsp->push_text( lpszInfo_begin, pCrt->strlen( lpszInfo_begin ), JSP_TEXT );
        m_pJsp->push_text( pData->m_str.c_str(), ( int )pData->m_str.length(), JSP_TEXT );
        m_pJsp->push_text( lpszInfo_end, pCrt->strlen( lpszInfo_end ), JSP_TEXT );

        return ret;
    }

} // jsp
