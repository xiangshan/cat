/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_cmd.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_cmd::jsp_cmd( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_cmd::~jsp_cmd()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int jsp_cmd::load_file( char * lpszInfo, int nSize, const char * lpszFile )
    {
        int ret = 0;

        xos::i_file * pFile = 0;
        char file[1024] = { 0 };

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_FILE, ( void** )&pFile );
        }

        if( 0 == ret )
        {
            mgr::xos()->crt()->strcpy( file, sizeof( file ), m_pJsp->path() );
            mgr::xos()->misc()->path_append( file, lpszFile );
        }

        if( 0 == ret )
        {
            ret = pFile->open( file, xos::i_file::FILE_READ, xos::i_file::SHARE_READ, xos::i_file::FILE_OPEN );
        }

        if( 0 == ret )
        {
            int len = pFile->read( lpszInfo, nSize );
            if( len > 0 )
            {
                lpszInfo[len] = 0;
            }
        }

        xos_stl::release_interface( pFile );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    // 
    // <%@ include file="head.jsp" %>
    // 
    int jsp_cmd::parse( xos::i_big_buf * pBigBuf, xos::i_buf * pBuf )
    {
        int result = 0;
        int ret = 0;

        xos_common::i_misc * pMisc = mgr::xos()->common_misc();
        xos::i_crt * pCrt = mgr::xos()->crt();

        const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
        const char * lpszBegin = 0;
        const char * lpszEnd = 0;
        int nLen = pBuf->get_len( 0 );

        char * lpszContent = 0;
        char * lpszParam = 0;
        char * lpszCmd = 0;

        char jsp_file[40960];
        char buf[4096];

        if( ( 0 == ret ) && ( 0 != pMisc->begin_with( lpszBuf, "<%@" ) ) )
        {
            ret = 1;
        }

        if( ( 0 == ret ) && ( 0 != pMisc->end_with( lpszBuf, "%>" ) ) )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            lpszEnd = lpszBuf + nLen - 2;
            lpszBegin = lpszBuf + 3;
        }

        if( 0 == ret )
        {
            int len = ( int )( lpszEnd - lpszBegin );
            buf[0] = 0;
            pCrt->strncpy( buf, sizeof( buf ), lpszBegin, len );
            pMisc->trim( buf, " " );
        }

        // <%@ include file="head.jsp" %>
        // include file="head.jsp"
        if( 0 == ret )
        {
            lpszContent = pCrt->strstr( buf, "=", true );
            lpszParam = pCrt->strstr( buf, " ", true );
            lpszCmd = buf;
            if( !lpszContent || !lpszParam )
            {
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            *lpszContent++ = 0;
            *lpszParam++ = 0;
            pMisc->trim( lpszContent, "\"" );
        }

        // 现在只支持include一条指令
        if( ( 0 == ret ) && ( 0 != pCrt->strcmp( lpszCmd, "include" ) ) )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            ret = load_file( jsp_file, sizeof( jsp_file ), lpszContent );
        }

        if( 0 == ret )
        {
            char * lpszBigBuf = pBigBuf->get_data( 0, 0, 0 );
            int len = pBigBuf->get_len( 0 );
            char buf[40960];
            pCrt->memcpy( buf, lpszBigBuf, len );
            pBigBuf->set_str( jsp_file );
            pBigBuf->add_data( buf, len );
            lpszBigBuf = pBigBuf->get_data( 0, 0, 0 );
            len = pBigBuf->get_len( 0 );
            lpszBigBuf[len] = 0;
        }

        if( 0 == ret )
        {
            result = 1;
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

} // jsp
