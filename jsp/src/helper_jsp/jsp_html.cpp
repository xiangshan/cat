/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "jsp_html.h"
#include "helper_jsp.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp_html::jsp_html( helper_jsp * pJsp ) : m_pJsp( pJsp )
    {
    }

    jsp_html::~jsp_html()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    // 
    // 
    // 
    int jsp_html::parse( xos::i_buf * pBuf )
    {
        int result = 0;
        int ret = 0;

        const char * lpszBuf = pBuf->get_data( 0, 0, 0 );
        int nLen = pBuf->get_len( 0 );

        if( 0 == ret )
        {
            m_pJsp->push_item( lpszBuf, nLen, JSP_HTML );
        }

        if( 0 == ret )
        {
            result = 1;
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // parse

    int jsp_html::to_function( data * pData )
    {
        int ret = 0;

        const char * lpszInfo = "\r\n\
    html_to_html( pTask, g_pHtmlBuf[%d] );";

        char buf[4096];
        int len = mgr::xos()->crt()->sprintf( buf, sizeof( buf ), lpszInfo, pData->m_nIndex );
        m_pJsp->push_text( buf, len, JSP_TEXT );

        return ret;
    }

    int jsp_html::to_array( data * pData )
    {
        int ret = 0;

        xos_common::i_misc * pMisc = mgr::xos()->common_misc();
        xos::i_crt * pCrt = mgr::xos()->crt();
        xos::i_buf * pBuf = pData->m_pBuf;

        char * lpszData = pBuf->get_data( 0, 0, 0 );
        int len = pBuf->get_len( 0 ); 
        char buf[40960];

        {
            pMisc->base64_encode( lpszData, len, buf, sizeof( buf ) );
            len = pCrt->strlen( buf );
            lpszData = buf;
        }

        {
            const char * lpszInfo = "\r\n\"";
            m_pJsp->push_text( lpszInfo, pCrt->strlen( lpszInfo ), JSP_TEXT );
        }
        m_pJsp->push_text( lpszData, len, JSP_TEXT );
        m_pJsp->push_text( "\"", 1, JSP_TEXT );

        return ret;
    }

} // jsp
