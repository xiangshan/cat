/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/mgr.h"

#if defined( _DEBUG )
#if defined( XOS_WIN32 )
#include "../../../../xoskit/xos_lib/win32/vld/include/vld.h"
#elif defined( XOS_WIN64 )
#include "../../../../xoskit/xos_lib/x64/vld/include/vld.h"
#endif
#endif

int main( int argc, const char * argv[] )
{
    int ret = 0;

    if( 0 == ret )
    {
        jsp::mgr obj;
        ret = obj.run( argc, argv );
    }

    return ret;
}
