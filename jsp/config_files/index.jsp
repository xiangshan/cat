﻿<!DOCTYPE html>
<%--支持的jsp与java web开发中的jsp比较类似，只是不支持标签，觉得没用。因为服务器端的任务是产生数据，不是做ui，简单的支持嵌入c++，及方便提取数据就行了--%>
<%!
    const char * g_head = "head";
    const char * g_foot = "foot";
    const char * g_app = "mywebsite";
    const char * g_name = "ghh";
%>
<%!
    int get_result( int nData )
    {
        int ret = 0;
        ret = 2 * nData;
        return ret;
    }
%>
<%
    const char * http_url = pReqTag->str( "http_url" );
    int my_result = get_result( 2 );
%>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <title>test</title>
    </head>
    <%--系统支持几个固定名称变量：request,response,param,tag,application,http_url是http请求时，程序生成的标签名--%>
    <%--对于所有变量，也可以这样访问，这样更方便。框架会帮助查找这个名称的变量--%>
    <%--查找顺序为：-> 当前action或page -> action同步调用栈（按action调用栈出栈顺序） -> request.param -> request.tag -> application--%>
    <%--见代码task.cpp::helper_vt()--%>
    <body>
        <%@ include file="head.jsp" %>
        supper variable, url = <%$ http_url %>
	    my_aapp = <%= g_app %><%= g_name %>
	    my_name = <%= g_name %><br/>
	    my_uurl = <%= http_url %><br/>
	    resultv = <%= my_result %><br/>
        <%@ include file="foot.jsp" %>
    </body>
</html>
