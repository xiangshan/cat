/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __D836B1BB_6CC8_48D6_9B51_AB69FC39BA8E__
#define __D836B1BB_6CC8_48D6_9B51_AB69FC39BA8E__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_common/include/interface.h"
#include "../property/head.h"

namespace icat
{    
    class i_webapp : public i_property
    {
    public:
        virtual ~i_webapp(){}

    public:
    };
}

#endif // __D836B1BB_6CC8_48D6_9B51_AB69FC39BA8E__
