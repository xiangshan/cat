/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __B75615A8_AC52_4D64_922A_BC3C3F06AE68__
#define __B75615A8_AC52_4D64_922A_BC3C3F06AE68__

namespace icat
{
    const char * i_webapp_id = "CC144E79_7F49_4B7D_B7AF_EDB1F9822121";
    const char * i_webapp_name = "webapp";
}

#endif // __B75615A8_AC52_4D64_922A_BC3C3F06AE68__
