/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __AD673104_E1F2_4AF9_AECC_1D2F4AE7815E__
#define __AD673104_E1F2_4AF9_AECC_1D2F4AE7815E__

namespace icat
{
    const char * HTTP_QUIT_PROGRAM = "quit";

    const char * i_action_id = "A0EA1A08_039F_4797_97DA_4992A6D4B70F";
    const char * i_action_name = "action";
}

#endif // __AD673104_E1F2_4AF9_AECC_1D2F4AE7815E__
