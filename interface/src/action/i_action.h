/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __5B3E8052_3338_4859_9737_01E45815B07D__
#define __5B3E8052_3338_4859_9737_01E45815B07D__

#include "../property/head.h"
#include "../agged/head.h"

namespace icat
{    

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    class i_action : public i_agged, public i_property
    {
    public:
        virtual ~i_action(){}

    public:
    };

}

#endif // __5B3E8052_3338_4859_9737_01E45815B07D__
