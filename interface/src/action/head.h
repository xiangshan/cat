/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __3ADD362E_407A_411E_B654_34C2E2FB09AA__
#define __3ADD362E_407A_411E_B654_34C2E2FB09AA__

#include "guid_declare.h"
#include "i_action.h"

#endif // __3ADD362E_407A_411E_B654_34C2E2FB09AA__
