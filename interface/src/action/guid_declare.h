/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __1090E503_9528_4DD7_8B01_6E45E288EFC0__
#define __1090E503_9528_4DD7_8B01_6E45E288EFC0__

namespace icat
{
    extern const char * HTTP_QUIT_PROGRAM;

    extern const char * i_action_id;
    extern const char * i_action_name;
}

#endif // __1090E503_9528_4DD7_8B01_6E45E288EFC0__
