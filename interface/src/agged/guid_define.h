/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __732FC7BC_2485_4C9D_AC8E_4F8BE00ECD9C__
#define __732FC7BC_2485_4C9D_AC8E_4F8BE00ECD9C__

namespace icat
{
    const char * i_agged_id = "FB689E71_C9F3_4FC9_8D2B_3467EE067171";
    const char * i_agged_name = "agged";
}

#endif // __732FC7BC_2485_4C9D_AC8E_4F8BE00ECD9C__
