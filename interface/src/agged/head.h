/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __1C065F14_4324_4514_8943_E89291B23963__
#define __1C065F14_4324_4514_8943_E89291B23963__

#include "guid_declare.h"
#include "i_agged.h"

#endif // __1C065F14_4324_4514_8943_E89291B23963__
