/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __9F564E29_2A00_475B_801B_A04BD9E2709E__
#define __9F564E29_2A00_475B_801B_A04BD9E2709E__

#include "../plugin/head.h"

namespace icat
{    

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    class i_agged : public i_plugin
    {
    public:
        virtual ~i_agged(){}

    public:
        virtual int reset() = 0;
    };

}

#endif // __9F564E29_2A00_475B_801B_A04BD9E2709E__
