/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __45282A3C_B802_481E_BA01_F6AF4BE995B5__
#define __45282A3C_B802_481E_BA01_F6AF4BE995B5__

namespace icat
{
    extern const char * i_agged_id;
    extern const char * i_agged_name;
}

#endif // __45282A3C_B802_481E_BA01_F6AF4BE995B5__
