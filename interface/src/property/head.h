/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __A82DD34C_FD49_40BF_A7CB_4EF9C59BD93E__
#define __A82DD34C_FD49_40BF_A7CB_4EF9C59BD93E__

#include "guid_declare.h"
#include "i_property.h"

#endif // __A82DD34C_FD49_40BF_A7CB_4EF9C59BD93E__
