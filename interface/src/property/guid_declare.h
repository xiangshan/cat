/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __A75C3B77_BD3B_42E4_A658_48FC75150A45__
#define __A75C3B77_BD3B_42E4_A658_48FC75150A45__

namespace icat
{
    extern const char * i_property_id;
    extern const char * i_property_name;
}

#endif // __A75C3B77_BD3B_42E4_A658_48FC75150A45__
