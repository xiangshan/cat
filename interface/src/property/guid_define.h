/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __F77F2B47_443F_4AEE_9E6A_CA5CBDB2639F__
#define __F77F2B47_443F_4AEE_9E6A_CA5CBDB2639F__

namespace icat
{
    const char * i_property_id = "B7179D9B_44CC_4B8E_96A3_25597282C88C";
    const char * i_property_name = "property";
}

#endif // __F77F2B47_443F_4AEE_9E6A_CA5CBDB2639F__
