/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __69F3E322_4F33_4BAF_AF94_4E01E9162CE4__
#define __69F3E322_4F33_4BAF_AF94_4E01E9162CE4__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_common/include/interface.h"

namespace icat
{    
    class i_property : public xos::i_unknown
    {
    public:
        virtual ~i_property(){}

    public:
        virtual xos_common::i_property * prop() = 0;
    };
}

#endif // __69F3E322_4F33_4BAF_AF94_4E01E9162CE4__
