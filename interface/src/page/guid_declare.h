/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __06A264B8_20EF_4A91_9E71_6790B751E55F__
#define __06A264B8_20EF_4A91_9E71_6790B751E55F__

namespace icat
{
    extern const char * i_page_id;
    extern const char * i_page_name;
}

#endif // __06A264B8_20EF_4A91_9E71_6790B751E55F__
