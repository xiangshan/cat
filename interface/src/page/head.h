/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __2F8EC07D_1287_4C0F_AE1A_D24E04D37129__
#define __2F8EC07D_1287_4C0F_AE1A_D24E04D37129__

#include "guid_declare.h"
#include "i_page.h"

#endif // __2F8EC07D_1287_4C0F_AE1A_D24E04D37129__
