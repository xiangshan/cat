/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __23D37BFA_F7A5_4F10_ACED_B6541802DDE1__
#define __23D37BFA_F7A5_4F10_ACED_B6541802DDE1__

#include "../property/head.h"
#include "../action/head.h"

namespace icat
{    

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    class i_page : public i_action
    {
    public:
        virtual ~i_page(){}

    public:
    };

}

#endif // __23D37BFA_F7A5_4F10_ACED_B6541802DDE1__
