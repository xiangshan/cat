/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __D3430F04_8EE3_48C5_AB18_A72FD7A81858__
#define __D3430F04_8EE3_48C5_AB18_A72FD7A81858__

namespace icat
{
    extern const char * i_plugin_mgr_id;
    extern const char * i_plugin_mgr_name;

    extern const char * i_plugin_id;
    extern const char * i_plugin_name;
}

#endif // __D3430F04_8EE3_48C5_AB18_A72FD7A81858__
