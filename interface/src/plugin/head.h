/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __A16673AC_D78C_4130_A4AA_4880A473D6E9__
#define __A16673AC_D78C_4130_A4AA_4880A473D6E9__

#include "guid_declare.h"
#include "i_plugin_mgr.h"
#include "i_plugin.h"

#endif // __A16673AC_D78C_4130_A4AA_4880A473D6E9__
