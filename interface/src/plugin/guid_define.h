/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __E538EC63_CDC7_4148_A3F3_254F36CCBAC1__
#define __E538EC63_CDC7_4148_A3F3_254F36CCBAC1__

namespace icat
{
    const char * i_plugin_mgr_id = "32F3A875_9693_42E4_A714_324E606A483D";
    const char * i_plugin_mgr_name = "plugin_mgr";

    const char * i_plugin_id = "8C97370D_8F4E_458F_A316_092122E50C80";
    const char * i_plugin_name = "plugin";
}

#endif // __E538EC63_CDC7_4148_A3F3_254F36CCBAC1__
