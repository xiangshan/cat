/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __7B2B1B0A_CE48_4060_9545_954CEC138300__
#define __7B2B1B0A_CE48_4060_9545_954CEC138300__

#include "i_plugin.h"

namespace icat
{    

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    class i_plugin_mgr : public xos::i_unknown
    {
    public:
        virtual ~i_plugin_mgr(){}

    public:
        virtual int create( const char * pClsName, icat::i_plugin ** ppv ) = 0;
        virtual const char * query_creatable_class_name( int nIndex ) = 0;
        virtual int set_module( xos::i_dynamic * pModule ) = 0;
        virtual xos::i_dynamic * get_module() = 0;
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 导出函数定义
    // 
    typedef int ( * f_create )( xos_container::i_container * pContainer, xos::i_dynamic * pModule, 
        const char * lpszModule, 
        i_plugin_mgr ** ppv );
}

#endif // __7B2B1B0A_CE48_4060_9545_954CEC138300__
