/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __44B1EB03_33A3_44BA_9AEF_B057EC8B7EEC__
#define __44B1EB03_33A3_44BA_9AEF_B057EC8B7EEC__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_container/include/interface.h"

namespace icat
{    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    class i_task;
    class i_plugin : public xos::i_unknown
    {
    public:
        enum enumRetCode
        {
            RET_CODE_BREAK  = -9999999,
            RET_CODE_ASYNC  = -9999998,
            RET_CODE_QUIT   = -9999997,
            RET_CODE_DONE   =  9999999
        };

    public:
        virtual ~i_plugin(){}

    public:
        // 
        // i_plugin::RET_CODE_BREAK : not go on chain,break and return to caller
        // i_plugin::RET_CODE_ASYNC : async call
        // i_plugin::RET_CODE_QUIT  : quit server
        // i_plugin::RET_CODE_DONE  : finished
        // 
        virtual enumRetCode proc( i_task * pTask ) = 0;
    };

}

#endif // __44B1EB03_33A3_44BA_9AEF_B057EC8B7EEC__
