/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __1766A255_FA20_4994_9F8D_6DE769C57F0E__
#define __1766A255_FA20_4994_9F8D_6DE769C57F0E__

#include "guid_declare.h"
#include "i_request.h"

#endif // __1766A255_FA20_4994_9F8D_6DE769C57F0E__
