/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __3895FC7A_D670_42E3_9E10_8F9ED9304E96__
#define __3895FC7A_D670_42E3_9E10_8F9ED9304E96__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_common/include/interface.h"
#include "../property/head.h"

namespace icat
{    
    class i_request : public i_property
    {
    public:
        virtual ~i_request(){}

    public:
        virtual xos_common::i_property * get_param() = 0;
        virtual xos_common::i_property * get_path() = 0;
        virtual xos_common::i_property * get_tag() = 0;
        virtual xos::i_list * get_data() = 0;
        virtual i_task * get_task() = 0;
    };
}

#endif // __3895FC7A_D670_42E3_9E10_8F9ED9304E96__
