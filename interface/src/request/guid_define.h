/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __B498735F_7447_4D71_ABE8_7270006D5190__
#define __B498735F_7447_4D71_ABE8_7270006D5190__

namespace icat
{
    const char * i_request_id = "B3653958_79F6_4627_8D37_A78B1257C8F6";
    const char * i_request_name = "request";
}

#endif // __B498735F_7447_4D71_ABE8_7270006D5190__
