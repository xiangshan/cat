/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __6FAB66FA_0D5C_4CB9_9330_B686EF0D1582__
#define __6FAB66FA_0D5C_4CB9_9330_B686EF0D1582__

namespace icat
{

    const char * i_listener_id = "83953E1E_B74D_43FF_9814_CE4B780883A8";
    const char * i_listener_name = "listener";
}

#endif // __6FAB66FA_0D5C_4CB9_9330_B686EF0D1582__
