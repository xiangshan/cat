/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __1EB182A2_51C5_422D_B7F0_A0648F373A22__
#define __1EB182A2_51C5_422D_B7F0_A0648F373A22__

namespace icat
{
    namespace listener
    {
        namespace process
        {
            extern const char * type;

            extern const char * recv;
            extern const char * again;
            extern const char * done;
            extern const char * proc;

            extern const char * before;
            extern const char * after;
        }
    }
}

#endif // __1EB182A2_51C5_422D_B7F0_A0648F373A22__
