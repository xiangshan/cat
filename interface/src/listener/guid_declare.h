/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __A6015734_40CB_4A36_AE82_6407F1D65D63__
#define __A6015734_40CB_4A36_AE82_6407F1D65D63__

namespace icat
{
    extern const char * i_listener_id;
    extern const char * i_listener_name;
}

#endif // __A6015734_40CB_4A36_AE82_6407F1D65D63__
