/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __FE12C2F2_F53D_47E5_B507_D618EA637222__
#define __FE12C2F2_F53D_47E5_B507_D618EA637222__

namespace icat
{
    namespace listener
    {
        namespace process
        {
            const char * type = "process";

            const char * recv = "recv";
            const char * again = "again";
            const char * done = "done";
            const char * proc = "proc";

            const char * before = "before";
            const char * after = "after";
        }
    }
}

#endif // __FE12C2F2_F53D_47E5_B507_D618EA637222__
