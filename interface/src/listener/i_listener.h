/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __0D02EAB4_4B3C_42EE_A5E5_B3891820A869__
#define __0D02EAB4_4B3C_42EE_A5E5_B3891820A869__

#include "../agged/head.h"

namespace icat
{    
    class i_listener : public i_agged
    {
    public:
        virtual ~i_listener(){}

    public:
        virtual xos_common::i_property * listeners() = 0;
    };

}

#endif // __0D02EAB4_4B3C_42EE_A5E5_B3891820A869__
