/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __48137F78_BBAE_4805_8C78_EF6B034D3699__
#define __48137F78_BBAE_4805_8C78_EF6B034D3699__

#include "guid_declare.h"
#include "declare.h"
#include "i_listener.h"

#endif // __48137F78_BBAE_4805_8C78_EF6B034D3699__
