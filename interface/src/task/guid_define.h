/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __1E9A5035_A9A6_4D5D_B030_1AA87330F838__
#define __1E9A5035_A9A6_4D5D_B030_1AA87330F838__

namespace icat
{
    const char * i_task_id = "5A672181_34E0_466E_8E76_79F29A7B85BA";
    const char * i_task_name = "task";
}

#endif // __1E9A5035_A9A6_4D5D_B030_1AA87330F838__
