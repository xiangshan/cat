/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __428648DE_EF9E_4D81_9E9F_84D9C089596C__
#define __428648DE_EF9E_4D81_9E9F_84D9C089596C__

#include "guid_declare.h"
#include "i_task.h"

#endif // __428648DE_EF9E_4D81_9E9F_84D9C089596C__
