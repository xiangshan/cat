/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __5AB607DF_6896_4A37_B4D7_3CAF767AEF4F__
#define __5AB607DF_6896_4A37_B4D7_3CAF767AEF4F__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_common/include/interface.h"
#include "../property/head.h"

namespace icat
{    
    class i_webapp;
    class i_chain;
    class i_task : public i_property
    {
    public:
        virtual ~i_task(){}

    public:
        virtual int add_ret_data( const char * lpszData, int nLen ) = 0;
        virtual int add_ret_data( xos::i_buf *& pBuf ) = 0;
        virtual int add_ret_str( const char * lpszStr ) = 0;

        virtual int set_chain( i_chain * pChain ) = 0;
        virtual i_chain * get_chain() = 0;

        virtual i_webapp * app() = 0;

        virtual xos_common::i_variant * vt( const char * lpszVariable ) = 0;
        virtual xos::i_list * list( const char * lpszVariable ) = 0;
        virtual const char * str( const char * lpszVariable ) = 0;
        virtual double dbl( const char * lpszVariable ) = 0;
        virtual float flt( const char * lpszVariable ) = 0;
        virtual int it( const char * lpszVariable ) = 0;

        virtual i_response * get_response() = 0;
        virtual i_request * get_request() = 0;

        virtual const char * base_path() = 0;
    };
}

#endif // __5AB607DF_6896_4A37_B4D7_3CAF767AEF4F__
