/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __71C2E780_6DC5_4E16_BA1B_1EB9C77864B7__
#define __71C2E780_6DC5_4E16_BA1B_1EB9C77864B7__

namespace icat
{
    extern const char * i_task_id;
    extern const char * i_task_name;
}

#endif // __71C2E780_6DC5_4E16_BA1B_1EB9C77864B7__
