/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __131E2134_8856_4599_82B4_E319840DCAE2__
#define __131E2134_8856_4599_82B4_E319840DCAE2__

namespace icat
{
    extern const char * i_filter_id;
    extern const char * i_filter_name;
}

#endif // __131E2134_8856_4599_82B4_E319840DCAE2__
