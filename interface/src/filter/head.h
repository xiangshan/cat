/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __3CFD4BDC_49FA_4030_B6DA_29DF4BA79A48__
#define __3CFD4BDC_49FA_4030_B6DA_29DF4BA79A48__

#include "guid_declare.h"
#include "i_filter.h"

#endif // __3CFD4BDC_49FA_4030_B6DA_29DF4BA79A48__
