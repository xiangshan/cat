/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __24890AF9_DBAF_4EDF_84E7_A041BD285FB4__
#define __24890AF9_DBAF_4EDF_84E7_A041BD285FB4__

namespace icat
{
    const char * i_filter_id = "BECD4FB0_B870_498B_81DF_1BF1BC07E056";
    const char * i_filter_name = "filter";
}

#endif // __24890AF9_DBAF_4EDF_84E7_A041BD285FB4__
