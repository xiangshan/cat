/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __CCB6F2D4_6387_4E94_A2AA_48A85A4C14EF__
#define __CCB6F2D4_6387_4E94_A2AA_48A85A4C14EF__

#include "../agged/head.h"

namespace icat
{    
    class i_filter : public i_agged
    {
    public:
        virtual ~i_filter(){}

    public:
    };
}

#endif // __CCB6F2D4_6387_4E94_A2AA_48A85A4C14EF__
