/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __85D77380_D351_41AA_A28F_BEFB69BE48B5__
#define __85D77380_D351_41AA_A28F_BEFB69BE48B5__

namespace icat
{
    const char * i_response_id = "C1787C0C_85D6_431A_A49A_A4F0C1417A71";
    const char * i_response_name = "response";
}

#endif // __85D77380_D351_41AA_A28F_BEFB69BE48B5__
