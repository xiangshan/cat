/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __D07A17B1_E59A_417B_927F_8CFF91F3A1C9__
#define __D07A17B1_E59A_417B_927F_8CFF91F3A1C9__

#include "guid_declare.h"
#include "i_response.h"

#endif // __D07A17B1_E59A_417B_927F_8CFF91F3A1C9__
