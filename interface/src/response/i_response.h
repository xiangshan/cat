/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __43D76EB2_198F_4E52_B0E3_3E7478DB08F5__
#define __43D76EB2_198F_4E52_B0E3_3E7478DB08F5__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_common/include/interface.h"
#include "../property/head.h"

namespace icat
{    
    class i_response : public i_property
    {
    public:
        virtual ~i_response(){}

    public:
        virtual xos_common::i_property * get_tag() = 0;
        virtual xos::i_list * get_data() = 0;
        virtual i_task * get_task() = 0;
    };
}

#endif // __43D76EB2_198F_4E52_B0E3_3E7478DB08F5__
