/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __64275C71_5AC7_4A54_ACD0_9A1C13E3432E__
#define __64275C71_5AC7_4A54_ACD0_9A1C13E3432E__

namespace icat
{
    extern const char * i_response_id;
    extern const char * i_response_name;
}

#endif // __64275C71_5AC7_4A54_ACD0_9A1C13E3432E__
