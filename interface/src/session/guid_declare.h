/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __472343E5_D515_47B3_98C2_F4B530C880AB__
#define __472343E5_D515_47B3_98C2_F4B530C880AB__

namespace icat
{
    extern const char * i_session_id;
    extern const char * i_session_name;
}

#endif // __472343E5_D515_47B3_98C2_F4B530C880AB__
