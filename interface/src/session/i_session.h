/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __3368F744_38F3_4D2E_AF33_D80C6BA4212E__
#define __3368F744_38F3_4D2E_AF33_D80C6BA4212E__

#include "../../../../xoskit/xos_core/include/interface.h"
#include "../../../../xoskit/xos_common/include/interface.h"
#include "../property/head.h"

namespace icat
{    
    class i_session : public i_property
    {
    public:
        virtual ~i_session(){}

    public:
    };
}

#endif // __3368F744_38F3_4D2E_AF33_D80C6BA4212E__
