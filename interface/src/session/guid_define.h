/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __B771BEE2_BD9E_4500_BF1D_1BE68F2EFF67__
#define __B771BEE2_BD9E_4500_BF1D_1BE68F2EFF67__

namespace icat
{
    const char * i_session_id = "47D96006_58C1_4F75_BD9E_D1405BD93BD1";
    const char * i_session_name = "session";
}

#endif // __B771BEE2_BD9E_4500_BF1D_1BE68F2EFF67__
