/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __0D84A7A0_40F3_4E52_AA4F_2C8B9FDECCCE__
#define __0D84A7A0_40F3_4E52_AA4F_2C8B9FDECCCE__

#include "../plugin/head.h"

namespace icat
{    
    class i_chain : public xos::i_unknown
    {
    public:
        virtual ~i_chain(){}

    public:
        virtual icat::i_plugin::enumRetCode async_call( const char * lpszModule, const char * lpszAction, const char * lpszMethod, bool bSlowJob ) = 0;
        virtual icat::i_plugin::enumRetCode async_call( i_plugin * pPlugin, const char * lpszMethod, bool bTakeOwner, bool bSlowJob ) = 0;
        virtual icat::i_plugin::enumRetCode async_call_jsp( const char * lpszJspFile, bool bSlowJob ) = 0;
        virtual icat::i_plugin::enumRetCode async_call_self( const char * lpszMethod, bool bSlowJob ) = 0;

        virtual icat::i_plugin::enumRetCode call( const char * lpszModule, const char * lpszAction, const char * lpszMethod ) = 0;
        virtual icat::i_plugin::enumRetCode call( i_plugin * pPlugin, const char * lpszMethod, bool bTakeOwner ) = 0;
        virtual icat::i_plugin::enumRetCode call_jsp( const char * lpszJspFile ) = 0;
        virtual icat::i_plugin::enumRetCode call_self( const char * lpszMethod ) = 0;

        virtual int get_listen_event( const char *& lpszType, const char *& lpszEvent, const char *& lpszAdvise ) = 0;
        virtual int set_listen_event( const char * lpszType, const char * lpszEvent, const char * lpszAdvise ) = 0;

        virtual icat::i_plugin::enumRetCode run( icat::i_plugin::enumRetCode result ) = 0;

        virtual const char * action() = 0;
        virtual const char * method() = 0;

        virtual int set_go_back() = 0;
        virtual bool is_go_back() = 0;
    };
}

#endif // __0D84A7A0_40F3_4E52_AA4F_2C8B9FDECCCE__
