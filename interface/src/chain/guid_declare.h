/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __7924462B_F1AA_4440_B35C_64808EC57D92__
#define __7924462B_F1AA_4440_B35C_64808EC57D92__

namespace icat
{
    extern const char * i_chain_id;
    extern const char * i_chain_name;
}

#endif // __7924462B_F1AA_4440_B35C_64808EC57D92__
