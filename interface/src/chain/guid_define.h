/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DEB71FE4_F6AB_46F2_A390_52D4D26B9465__
#define __DEB71FE4_F6AB_46F2_A390_52D4D26B9465__

namespace icat
{
    const char * i_chain_id = "8A113F80_D581_454A_B6D1_AF940715AB33";
    const char * i_chain_name = "chain";
}

#endif // __DEB71FE4_F6AB_46F2_A390_52D4D26B9465__
