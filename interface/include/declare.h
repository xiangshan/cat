/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __50AE003F_AF1B_46AA_B203_144D64311668__
#define __50AE003F_AF1B_46AA_B203_144D64311668__

#include "../src/page/guid_declare.h"
#include "../src/webapp/guid_declare.h"
#include "../src/session/guid_declare.h"
#include "../src/listener/guid_declare.h"
#include "../src/listener/declare.h"
#include "../src/property/guid_declare.h"
#include "../src/filter/guid_declare.h"
#include "../src/agged/guid_declare.h"
#include "../src/action/guid_declare.h"
#include "../src/chain/guid_declare.h"
#include "../src/plugin/guid_declare.h"
#include "../src/request/guid_declare.h"
#include "../src/response/guid_declare.h"
#include "../src/task/guid_declare.h"

#endif // __50AE003F_AF1B_46AA_B203_144D64311668__
