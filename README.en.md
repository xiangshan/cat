﻿# cat

## 概述
-----------------
实现用c++做网站后端开发，和java一样容易。

### 原理 ###
   1. 用插件形式实现action,fileter,listener
   2. web server依靠目录结构寻找url请求的action
   3. 支持jsp文件，形式和java的jsp类似
   4. jsp文件中可以嵌入c++代码
   5. jsp文件可以被转成c++代码
   6. jsp转换的c++代码可以被编译成page插件
   7. page插件和action一样，可以被调用
   8. action和struts2一样，可以自动跳转到对应jsp生成的page插件,page插件中可以使用action的生成的数据变量，完成向前端发送生成的html
   9. 请求路由利用server端的目录结构，不需要配置

### 功能 ###
   1. action
   2. filter
   3. listener
   4. https
   5. jsp

### 流程 ###
![](doc/image/cat_flow.png)

### 例子 ###
![](doc/image/website.png)
<center>网站的目录结构</center>

   1. 目录结构说明
      1. webapps是所有网站的根目录
      2. www.cat.com_1111是一个网站目录，www.cat.com是域名，1111表示端口号
      3. action目录，所有action动态库都在这了。动态库名称即为模块名，比如action.xos，模块名为action。用户实现的action类在它里边。一个模块可以有多个action类。请求action的例子：http://www.cat.com:1111/action/echo!test.action, 会请求actin/action.xos中的echo类的test方法。
      4. filter目录，所有filter都在这了。每个动态库文件为一个模块，内会有多个filter实现类。
      5. html存放网站的html页面文件
      6. js也可以创建一个目录
      7. jsp目录，按照jsp源码目录结构，存放编译好的jsp动态库。可以直接请求jsp，和java web开发时一样，如：http://www.cat.com:1111/main/main.jsp ，就会请求到jsp/main/main.jsp文件。
      8. listener目录，现在还没太想好怎么实现。只是加了几个固定的侦听点，可以在配置文件中配置。
      9. upload是我测试上传文件的例子，和项目没关系。
      10. index.html是首页
      11. web.xml网站配置文件，只有一个配置项，就是配置welcome file list

   1. 请求url : http://www.cat.com:1111/action/echo!test.action
      1. action是模块名，和存放action的目录名没关系。只是碰巧我给例子action起了名也叫action。
      2. echo是action模块中的action实现类。
      3. test是echo类中的方法。c++没法用字串定位到方法，所以是在echo的入口函数中，通过判断方法名称字串来判断方法名称的。

   2. action处理过程
      1. 最简单的可以这样，只是设置一下返回码，test_success。prop()->set( "xoskit", "supper good from action.echo.test method" );是将action中的变量放入map中，供后边的jsp页面使用.
```
    void echo::proc_task( icat::i_task * pTask )
    {
        icat::i_chain * pChain = pTask->get_chain();
        const char * lpszMethod = pChain->method();

        xos::i_crt * pCrt = container()->crt();

        if( 0 == pCrt->strcmp( lpszMethod, "test" ) )
        {
            test( pTask );
        }
        else
        {
            default_handler( pTask );
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    
    void echo::default_handler( icat::i_task * pTask )
    {
        prop()->set( "xoskit", "supper good from action.echo.default method" );
        BASE::set_ret_code( "default_success" );
    }

    void echo::test( icat::i_task * pTask )
    {
        prop()->set( "xoskit", "supper good from action.echo.test method" );
        BASE::set_ret_code( "test_success" );
    }
```
   3. jsp过程
      1. jsp例子内容，这是test_success.jsp文件内容：
```
<!DOCTYPE html>
<%--支持的jsp与java web开发中的jsp比较类似，只是不支持标签，觉得没用。因为服务器端的任务是产生数据，不是做ui，简单的支持嵌入c++，及方便提取数据就行了--%>
<%!
	const char * g_poem[] = 
	{
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。"
	};
%>
<%
	const char * url = pReqTag->str( "http_url" );
%>
<html>
  <head>
  	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
  	<link type="text/css" rel="stylesheet" href="../../../html/css/web.css">
  	<title>test_success</title>
  </head>
   
  <body>
    <img src="../../../html/img/flower.jpg" border="0" width=100% height=213px>
    <a href="../../../html/baidu.htm">百度</a><br />
    <a href="../../../html/test.htm">ogre</a><br/>
    <%--loop demo--%>
    <% for( int i = 0; i < sizeof( g_poem ) / sizeof( g_poem[0] ); ++i ) { %>
        poem <%= i %> = <%= g_poem[i] %><br/>
    <% } %>
    <%--系统支持几个固定名称变量：request,response,param,tag,application,http_url是http请求时，程序生成的标签名--%>
    supper variable, url = <%$ task.request.tag.http_url %><br/>
    <%--对于所有变量，也可以这样访问，这样更方便。框架会帮助查找这个名称的变量--%>
    <%--查找顺序为：-> 当前action或page -> action同步调用栈（按action调用栈出栈顺序） -> request.param -> request.tag -> application--%>
    <%--见代码task.cpp::helper_vt()--%>
    supper variable, url = <%$ http_url %><br/>
    <%--variable from action--%>
    xoskit = <%$ xoskit %><br/>
    <p><%= url %></p>
  	<div id="container">
          <div id="banner">banner</div>
          <div id="link">link</div>
          <div id="content">content</div>
          <div id="footer">footer</div>
  	</div>
  </body>
</html>
```
      2. jsp编译成c++后的内容:
```
/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "jsp_functions.h"

const int JSP_BODY_NUM = 19;
int g_jsp_body_num = JSP_BODY_NUM;
xos::i_buf * g_pHtmlBuf[JSP_BODY_NUM] = { 0 };
const char * g_pszBase64Html[JSP_BODY_NUM] = 
{
"77u/PCFET0NUWVBFIGh0bWw+CgoK",
0,
"CjxodG1sPgogIDxoZWFkPgogIAk8bWV0YSBodHRwLWVxdWl2PSJDb250ZW50LXR5cGUiIGNvbnRlbnQ9InRleHQvaHRtbDsgY2hhcnNldD11dGYtOCI+CiAgCTxsaW5rIHR5cGU9InRleHQvY3NzIiByZWw9InN0eWxlc2hlZXQiIGhyZWY9Ii4uLy4uLy4uL2h0bWwvY3NzL3dlYi5jc3MiPgogIAk8dGl0bGU+dGVzdF9zdWNjZXNzPC90aXRsZT4KICA8L2hlYWQ+CiAgIAogIDxib2R5PgogICAgPGltZyBzcmM9Ii4uLy4uLy4uL2h0bWwvaW1nL2Zsb3dlci5qcGciIGJvcmRlcj0iMCIgd2lkdGg9MTAwJSBoZWlnaHQ9MjEzcHg+CiAgICA8YSBocmVmPSIuLi8uLi8uLi9odG1sL2JhaWR1Lmh0bSI+55m+5bqmPC9hPjxiciAvPgogICAgPGEgaHJlZj0iLi4vLi4vLi4vaHRtbC90ZXN0Lmh0bSI+b2dyZTwvYT48YnIvPgogICAgCiAgICA=",
0,
"CiAgICAgICAgcG9lbSA=",
0,
"ID0g",
0,
"PGJyLz4KICAgIA==",
0,
"CiAgICANCiAgICBzdXBwZXIgdmFyaWFibGUsIHVybCA9IA==",
0,
"PGJyLz4KICAgIAogICAgDQogICAgDQogICAgc3VwcGVyIHZhcmlhYmxlLCB1cmwgPSA=",
0,
"PGJyLz4KICAgIAogICAgeG9za2l0ID0g",
0,
"PGJyLz4KICAgIDxwPg==",
0,
"PC9wPgogIAk8ZGl2IGlkPSJjb250YWluZXIiPgogICAgICAgICAgPGRpdiBpZD0iYmFubmVyIj5iYW5uZXI8L2Rpdj4KICAgICAgICAgIDxkaXYgaWQ9ImxpbmsiPmxpbms8L2Rpdj4KICAgICAgICAgIDxkaXYgaWQ9ImNvbnRlbnQiPmNvbnRlbnQ8L2Rpdj4KICAgICAgICAgIDxkaXYgaWQ9ImZvb3RlciI+Zm9vdGVyPC9kaXY+CiAgCTwvZGl2PgogIDwvYm9keT4KPC9odG1sPgo="
};
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ declare begin

	const char * g_poem[] = 
	{
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。",
		"白日依山近，黄河入海流。",
		"欲穷千里目，更上一层楼。"
	};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ declare end

void page_render( icat::i_task * pTask,
    icat::i_request * pRequest,
    icat::i_response * pResponse,
    xos_common::i_property * pReqTag,
    xos_common::i_property * pResTag )
{
    using namespace jsp;
    html_to_html( pTask, g_pHtmlBuf[0] );
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ begin

	const char * url = pReqTag->str( "http_url" );

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ end
    html_to_html( pTask, g_pHtmlBuf[2] );
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ begin
 for( int i = 0; i < sizeof( g_poem ) / sizeof( g_poem[0] ); ++i ) { 
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ end
    html_to_html( pTask, g_pHtmlBuf[4] );
    variable_to_html( pTask, i );
    html_to_html( pTask, g_pHtmlBuf[6] );
    variable_to_html( pTask, g_poem[i] );
    html_to_html( pTask, g_pHtmlBuf[8] );
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ begin
 } 
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// - c++ end
    html_to_html( pTask, g_pHtmlBuf[10] );
    supper_variable_to_html( pTask, "task.request.tag.http_url" );
    html_to_html( pTask, g_pHtmlBuf[12] );
    supper_variable_to_html( pTask, "http_url" );
    html_to_html( pTask, g_pHtmlBuf[14] );
    supper_variable_to_html( pTask, "xoskit" );
    html_to_html( pTask, g_pHtmlBuf[16] );
    variable_to_html( pTask, url );
    html_to_html( pTask, g_pHtmlBuf[18] );
}

```
      2. 这个c++文件，会被放到编译jsp的工程中：cat\page,放到page下的objects目录下，变成render.cpp文件。编译page工程，会生成一个page.xos的动态库。page是跨平台的，通过在不同平台上编译，会生成对应平台下的动态库。
      3. 最终得到webapps\www.cat.com_1111\jsp\action\echo\test_success.xos文件。
      4. action类echo返回了字串test_success，表示它想让test_success.jsp页面处理它的数据，于是cat程序调用插件test_success.xos动态库。由它生成html返回给前端。
      5. 返回的html页面代码：
```
<!DOCTYPE html>



<html>
  <head>
  	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
  	<link type="text/css" rel="stylesheet" href="../../../html/css/web.css">
  	<title>test_success</title>
  </head>
   
  <body>
    <img src="../../../html/img/flower.jpg" border="0" width=100% height=213px>
    <a href="../../../html/baidu.htm">百度</a><br />
    <a href="../../../html/test.htm">ogre</a><br/>
    
    
        poem 0 = 白日依山近，黄河入海流。<br/>
    
        poem 1 = 欲穷千里目，更上一层楼。<br/>
    
        poem 2 = 白日依山近，黄河入海流。<br/>
    
        poem 3 = 欲穷千里目，更上一层楼。<br/>
    
        poem 4 = 白日依山近，黄河入海流。<br/>
    
        poem 5 = 欲穷千里目，更上一层楼。<br/>
    
        poem 6 = 白日依山近，黄河入海流。<br/>
    
        poem 7 = 欲穷千里目，更上一层楼。<br/>
    
    
    supper variable, url = /action/echo!test.action<br/>
    
    
    
    supper variable, url = /action/echo!test.action<br/>
    
    xoskit = supper good from action.echo.test method<br/>
    <p>/action/echo!test.action</p>
  	<div id="container">
          <div id="banner">banner</div>
          <div id="link">link</div>
          <div id="content">content</div>
          <div id="footer">footer</div>
  	</div>
  </body>
</html>
```
      5. 就是这么一个过程。编译jsp的过程是由makefile控制的，改完jsp后，直接到makejsp目录下，make os=win32 [G=-g]就行了。

### 编译 ###

   1. 因为cat依赖[xoskit](https://gitee.com/helloworldghh/xoskit)，所以要把xoskit的代码，下到与cat同级目录，这样：
      1. D:\vmware\c++\develop\cat
      2. D:\vmware\c++\develop\xoskit
   2. 编译xoskit，参见[xoskit](https://gitee.com/helloworldghh/xoskit)上的编译说明。其实cat的编译过程和它一样。
   3. 同样的方法，编译cat.只是进入的是cat\makefile目录下编译cat。

### 欢迎前来讨论 ###
欢迎前来讨论。希望c++开发web和java一样容易。

### 相关项目 ###
   1. 安涛也有一个c++实现的，用c++开发web的项目很不错，[drogon](https://gitee.com/an-tao/drogon)，推荐一下。
