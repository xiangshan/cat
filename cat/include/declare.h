/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __536262C5_4E04_4FA7_A3B4_F4ADCD91B14D__
#define __536262C5_4E04_4FA7_A3B4_F4ADCD91B14D__

#include "../src/interface/declare.h"

#endif // __536262C5_4E04_4FA7_A3B4_F4ADCD91B14D__
