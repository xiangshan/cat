/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_packet/head.h"
#include "../msg_proc/head.h"
#include "../msg_net/head.h"
#include "../msg_aio/head.h"
#include "../msg_main/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "helper_proc.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    helper_proc::helper_proc()
    {
    }

    helper_proc::~helper_proc()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int helper_proc::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsgType = msg::get_msg_type( pMsg );

        switch( nMsgType )
        {
        case MSG_TYPE_AIO:
            {
                msg_aio obj;
                obj.proc( pMsg );
            }
            break;
        case MSG_TYPE_PACKET:
            {
                msg_packet obj;
                obj.proc( pMsg );
            }
            break;
        case MSG_TYPE_PROC:
            {
                msg_proc obj;
                obj.proc( pMsg );
            }
            break;
        case MSG_TYPE_MAIN:
            {
                msg_main obj;
                obj.proc( pMsg );
            }
            break;
        case MSG_TYPE_NET:
            {
                msg_net obj;
                obj.proc( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        xos_stl::release_interface( pMsg );

        return ret;
    }

} // cat
