/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_PROC_HEAD_H__
#define __HELPER_PROC_HEAD_H__

#include "helper_proc.h"

#endif // __HELPER_PROC_HEAD_H__
