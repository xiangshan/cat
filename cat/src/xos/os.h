/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __XOS_XOS_H__
#define __XOS_XOS_H__

namespace cat
{

    class os
    {
    public:
        os();
        ~os();

    public:
        static xos_container::i_container * get();
		static int init();
        static int term();

    };
} // cat

#endif // __XOS_XOS_H__
