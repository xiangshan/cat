/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../../../../xoskit/xos_core/include/xos_helper.h"
#include "../macro/head.h"
#include "../impl/mgr.h"
#include "os.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static xos_container::i_container * container_ptr = 0;
    static void * xos_module = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    os::os()
    {
    }

    os::~os()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    xos_container::i_container * os::get()
    {
        return container_ptr;
    } 

    int os::init()
    {
        int ret = 0;

        xos_container::f_create fun = 0;

        if( 0 == ret )
        {
            ret = xos_helper::xos_load_and_get( ( void ** )&fun, 
                xos_module,
                0,
                "xos_container",
                "create" );
        }

        if( 0 == ret )
        {
            ret = fun( &container_ptr );
        }

        return ret;
    }

    int os::term()
    {
        int ret = 0;

        xos_stl::release_interface( container_ptr );
        xos_helper::xos_free( xos_module );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

} // cat
