/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __68B86A32_081F_4008_A5BC_44265B301EFA__
#define __68B86A32_081F_4008_A5BC_44265B301EFA__

namespace cat
{
    extern const char * cls_module_id;
    extern const char * cls_module_name;

    extern const char * cls_chain_id;
    extern const char * cls_chain_name;
}

#endif // __68B86A32_081F_4008_A5BC_44265B301EFA__
