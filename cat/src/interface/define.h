/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __ACCD139B_E26D_4AB3_B8C9_59F71DEBBF67__
#define __ACCD139B_E26D_4AB3_B8C9_59F71DEBBF67__

namespace cat
{
    const char * cls_module_id = "716B437E_BA25_410C_89A2_3C7801EB06B5";
    const char * cls_module_name = "cat";

    const char * cls_chain_id = "A7ED2006_4372_49C6_97DC_825A9F0904D5";
    const char * cls_chain_name = "chain";
}

#endif // __ACCD139B_E26D_4AB3_B8C9_59F71DEBBF67__
