/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_AIO_NET_AIO_H__
#define __MSG_AIO_NET_AIO_H__

namespace cat
{
    class connection;

    class aio_tcp
    {
    public:
        aio_tcp();
        ~aio_tcp();
        
    public:
        int post_init( connection * pConnect );
        int post_accept( connection * pConnect );
        int post_connect( connection * pConnect );
        int post_recv( connection * pConnect );
        int post_send( connection * pConnect, xos::i_list * pList );
        int post_send( connection * pConnect, xos::i_buf *& pBuf );
        int post_shut_down( connection * pConnect, int how );
        int post_close( connection * pConnect, int onoff, int linger );

    public:
        int on_init( xos::i_msg *& pMsg );
        int on_accept( xos::i_msg *& pMsg );
        int on_connect( xos::i_msg *& pMsg );
        int on_recv( xos::i_msg *& pMsg );
        int on_send( xos::i_msg *& pMsg );
        int on_close( xos::i_msg *& pMsg );
        int close_test( connection * pConnect );

    public:
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_AIO_NET_AIO_H__
