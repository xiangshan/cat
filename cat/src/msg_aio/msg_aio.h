/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_AIO_MSG_AIO_H__
#define __MSG_AIO_MSG_AIO_H__

namespace cat
{

    class msg_aio
    {
    public:
        msg_aio();
        ~msg_aio();

    public:
		int proc( xos::i_msg *& pMsg );

    public:
        static xos_aio::i_aio * get();
        static int init();
        static int term();
        
    };

} // cat

#endif // __MSG_AIO_MSG_AIO_H__
