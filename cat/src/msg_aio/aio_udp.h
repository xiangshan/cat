/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_AIO_AIO_UDP_H__
#define __MSG_AIO_AIO_UDP_H__

namespace cat
{

    class data;
    class udp;

    class aio_udp
    {
    public:
        aio_udp();
        ~aio_udp();

    public:
        int post_init( connection * pConnect );
        int post_recv( connection * pConnect );
        int post_send( connection * pConnect, xos::i_list * pList, const char * lpszIp, int nPort );
        int post_send( connection * pConnect, xos::i_buf *& pBuf, const char * lpszIp, int nPort );
        int post_shut_down( connection * pConnect, int how );
        int post_close( connection * pConnect, int onoff, int linger );
        int close_test( connection * pConnect );

    protected:
        int on_init( xos::i_msg *& pMsg );
        int on_recv( xos::i_msg *& pMsg );
        int on_send( xos::i_msg *& pMsg );
        int on_close( xos::i_msg *& pMsg );

    public:
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_AIO_AIO_UDP_H__
