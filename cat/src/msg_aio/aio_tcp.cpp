/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "msg_aio.h"
#include "aio_tcp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    aio_tcp::aio_tcp()
    {
    }

    aio_tcp::~aio_tcp()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // net ret msg proc

    int aio_tcp::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
            // 
            // tcp
            // 
        case xos_aio::i_aio::AIO_TCP_INIT_RET:
            {
                on_init( pMsg );
            }
            break;
        case xos_aio::i_aio::AIO_TCP_ACCPET_RET:
            {
                on_accept( pMsg );
            }
            break;
        case xos_aio::i_aio::AIO_TCP_CONNECT_RET:
            {
                on_connect( pMsg );
            }
            break;
        case xos_aio::i_aio::AIO_TCP_RECV_RET:
            {
                on_recv( pMsg );
            }
            break;
        case xos_aio::i_aio::AIO_TCP_SEND_RET:
            {
                on_send( pMsg );
            }
            break;
        case xos_aio::i_aio::AIO_TCP_CLOSE_RET:
            {
                on_close( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // tcp opt

    // 
    // m_bData[0] : true or false for result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : local port
    // m_szStr[1] : local ip
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_init( connection * pConnect )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = mgr::xos()->msg();
        bool bCannot = !pConnect || !pConnect->none() || !( pConnect->tcp_listen() || pConnect->http_listen() ) || !( state::running() || state::starting() );

        if( ( 0 == ret ) && bCannot )
        {
            pMsg->set_int( 0, NET_TCP_INIT );
            pMsg->set_bool( 0, false );
            pMsg->set_void( 0, pConnect );
            pMsg->set_str( 0, "test failed" );
            msg::notify_net( pMsg, false );
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_INIT );
            pMsg->set_str( 1, pTcp->m_local_ip.c_str() );
            pMsg->set_int( 1, pTcp->m_nLocalPort );
            pMsg->set_void( 0, pConnect );
        }

        if( 0 == ret )
        {
            pConnect->set_initing();
            ret = msg_aio::get()->post_request( pMsg );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : local port
    // m_szStr[1] : local ip
    // m_nData[2] : peer port
    // m_szStr[2] : peer ip
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // m_lpData[2] : ClientKey
    // 
    int aio_tcp::post_accept( connection * pConnect )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = 0;
        bool bCannot = !pConnect || !pConnect->running() || !( pConnect->tcp_listen() || pConnect->http_listen() ) || !( state::running() || state::starting() );

        if( ( 0 == ret ) && bCannot )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg = mgr::xos()->msg();
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_ACCPET );
            pMsg->set_void( 0, pConnect );
            pMsg->set_void( 1, pTcp->m_pAioKey );
        }

        if( 0 == ret )
        {
            pTcp->m_nPostAcceptNum++;
            ret = msg_aio::get()->post_request( pMsg );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : local port
    // m_szStr[1] : local ip
    // m_nData[2] : peer port
    // m_szStr[2] : peer ip
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_connect( connection * pConnect )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = mgr::xos()->msg();
        bool bCannot = !pConnect || !pConnect->none() || !( pConnect->tcp_data() || pConnect->http_data() ) || !state::running();

        if( ( 0 == ret ) && bCannot )
        {
            pMsg->set_int( 0, NET_TCP_CONNECT );
            pMsg->set_bool( 0, false );
            pMsg->set_void( 0, pConnect );
            pMsg->set_str( 0, "test failed" );
            msg::notify_net( pMsg, false );
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_CONNECT );
            pMsg->set_str( 1, "" );
            pMsg->set_int( 1, 0 );
            pMsg->set_str( 2, pTcp->m_peer_ip.c_str() );
            pMsg->set_int( 2, pTcp->m_nPeerPort );
            pMsg->set_void( 0, pConnect );
        }

        if( 0 == ret )
        {
            pConnect->set_connecting();
            ret = msg_aio::get()->post_request( pMsg );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : recv len
    // m_lpBuf[0] : recv buf
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_recv( connection * pConnect )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = 0;
        xos::i_buf * pBuf = 0;
        bool bCannot = !pConnect || !pConnect->running() || !( pConnect->tcp_data() || pConnect->http_data() ) || !( state::running() || state::starting() );

        if( ( 0 == ret ) && bCannot )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg = mgr::xos()->msg();
            pBuf = mgr::xos()->buf();
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_RECV );
            pMsg->set_buf( 0, pBuf );
            pMsg->set_void( 0, pConnect );
            pMsg->set_void( 1, pTcp->m_pAioKey );
            pBuf = 0;
        }

        if( 0 == ret )
        {
            pTcp->m_nPostRecvNum++;
            ret = msg_aio::get()->post_request( pMsg );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : send bytes
    // m_lpBuf[0] : send buf
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_send( connection * pConnect, xos::i_list * pList )
    {
        int ret = 0;

        xos::i_buf * pBuf = 0;

        while( ( pBuf = ( xos::i_buf * )pList->front( 0 ) ) )
        {
            post_send( pConnect, pBuf );
            pList->pop_front();
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : send bytes
    // m_lpBuf[0] : send buf
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_send( connection * pConnect, xos::i_buf *& pBuf )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = mgr::xos()->msg();
        bool bCannot = !pConnect || !pConnect->running() || !( pConnect->tcp_data() || pConnect->http_data() ) || !state::running();

        if( ( 0 == ret ) && bCannot )
        {
            pMsg->set_int( 0, NET_TCP_SEND );
            pMsg->set_bool( 0, false );
            pMsg->set_void( 0, pConnect );
            pMsg->set_buf( 0, pBuf );
            pMsg->set_str( 0, "test failed" );
            msg::notify_net( pMsg, false );
            pBuf = 0;
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_SEND );
            pMsg->set_void( 0, pConnect );
            pMsg->set_buf( 0, pBuf );
            pMsg->set_void( 1, pTcp->m_pAioKey );
            pBuf = 0;
        }

        if( 0 == ret )
        {
            pTcp->m_nPostSendNum++;
            ret = msg_aio::get()->post_request( pMsg );
        }

        return ret;
    }

    // 
    // m_nData[0] : cmd type
    // m_nData[1] : how
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_shut_down( connection * pConnect, int how )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = 0;
        bool bCannot = !pConnect || !pConnect->running() || !( pConnect->tcp_data() || pConnect->http_data() ) || !( state::running() || state::quitting() );

        if( ( 0 == ret ) && bCannot )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg = mgr::xos()->msg();
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_SHUT_DOWN );
            pMsg->set_void( 0, pConnect );
            pMsg->set_void( 1, pTcp->m_pAioKey );
            pMsg->set_int( 1, how );
        }

        if( 0 == ret )
        {
            pConnect->set_shutdown();
            ret = msg_aio::get()->post_request( pMsg );
        }

        if( 0 == ret )
        {
            LOG4( "shutdown = (%s:%d)(%s:%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // m_nData[1] : linger.l_onoff
    // m_nData[2] : linger.l_linger
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::post_close( connection * pConnect, int onoff, int linger )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;
        xos::i_msg * pMsg = 0;
        bool bCannot = !pConnect || pConnect->closing() || pConnect->closed() || !( state::running() || state::quitting() );

        if( ( 0 == ret ) && bCannot )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg = mgr::xos()->msg();
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, xos_aio::i_aio::AIO_TCP_CLOSE );
            pMsg->set_int( 1, onoff );
            pMsg->set_int( 2, linger );
            pMsg->set_void( 0, pConnect );
            pMsg->set_void( 1, pTcp->m_pAioKey );
        }

        if( 0 == ret )
        {
            pConnect->set_closing();
            ret = msg_aio::get()->post_request( pMsg );
        }

        if( 0 == ret )
        {
            LOG4( "close = (%s:%d)(%s:%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort );
        }

        return ret;
    }

    int aio_tcp::close_test( connection * pConnect )
    {
        int ret = 0;

        tcp * pTcp = pConnect->m_pTcp;

        bool bNeedClose = ( pConnect->running() || pConnect->shutdown() ) && ( 0 == pTcp->m_nPostRecvNum ) && ( 0 == pTcp->m_nPostSendNum ) && ( 0 == pTcp->m_nPostAcceptNum );
        if( bNeedClose )
        {
            if( pConnect->running() )
            {
                post_shut_down( pConnect, xos::i_socket::XOS_SD_SEND );
            }
            pConnect->set_need_close();
            xos::i_msg * pMsg = mgr::xos()->msg();
            pMsg->set_int( 0, NET_TCP_NEED_CLOSE );
            pMsg->set_void( 0, pConnect );
            msg::notify_net( pMsg, false );
            LOG4( "need close = (%s:%d)(%s:%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // tcp

    // 
    // m_bData[0] : true or false for result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : local port
    // m_szStr[1] : local ip
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::on_init( xos::i_msg *& pMsg )
    {
        int ret = 0;

        connection * pConnect = (connection *)pMsg->get_void( 0, 0 );
        void * pAioKey = pMsg->get_void( 1, 0 );
        bool bRet = pMsg->get_bool( 0, 0 );
        tcp * pTcp = pConnect->m_pTcp;
        int nPostAccept = 0;

        if( bRet )
        {
            pTcp->m_local_ip = pMsg->get_str( 1, 0, 0, 0 );
            pTcp->m_nLocalPort = pMsg->get_int( 1, 0 );
            LOG4( "%s server post_init(%s:%d)", pConnect->http_listen() ? "http" : "tcp", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort );
            pTcp->m_pAioKey = pAioKey;
            connection::list()->push_front( pConnect );
            pConnect->m_connection_iter = connection::list()->begin();
        }
        else
        {
            LOG4( "init tcp port %d failed", pTcp->m_nLocalPort );
        }
        if( bRet )
        {
            pConnect->set_running();
            pConnect->lock_server();
        }
        {
            pMsg->set_int( 0, NET_TCP_INIT );
            msg::notify_net( pMsg, false );
        }

        while( bRet && ( 0 == nPostAccept ) && ( pTcp->m_nPostAcceptNum < config::get()->tcp_max_post_accept ) )
        {
            nPostAccept = post_accept( pConnect );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : local port
    // m_szStr[1] : local ip
    // m_nData[2] : peer port
    // m_szStr[2] : peer ip
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // m_lpData[2] : ClientKey
    // 
    int aio_tcp::on_accept( xos::i_msg *& pMsg )
    {
        int ret = 0;

        connection * pConnect = (connection *)pMsg->get_void( 0, 0 );
        tcp * pServer = pConnect->m_pTcp;
        void * pAioKey = pMsg->get_void( 2, 0 );
        bool bRet = pMsg->get_bool( 0, 0 );
        connection * pAcceptConnection = 0;
        int nPostAccept = 0;
        int nPostRecv = 0;
        tcp * pTcp = 0;

        if( bRet )
        {
            connection * pC = 0;
            tcp::init( pTcp, pC );
            pTcp->m_local_ip = pMsg->get_str( 1, 0, 0, 0 );
            pTcp->m_nLocalPort = pMsg->get_int( 1, 0 );
            pTcp->m_peer_ip = pMsg->get_str( 2, 0, 0, 0 );
            pTcp->m_nPeerPort = pMsg->get_int( 2, 0 );
            pTcp->m_pListenTcp = pServer;
            pTcp->m_pAioKey = pAioKey;
            // 告诉helper_tcp.cpp，这是什么listener接收到的连接
            if( pConnect->http_listen() )
            {
                pC->set_http_data();
            }
            else
            {
                pC->set_tcp_data();
            }
            connection::list()->push_front( pC );
            pC->m_connection_iter = connection::list()->begin();
            pAcceptConnection = pC;
        }

        if( bRet )
        {
            pAcceptConnection->set_timeout_time_s( config::get()->data_timeout_s );
            pAcceptConnection->set_running();
            pAcceptConnection->lock_server();
        }

        pServer->m_nPostAcceptNum--;

        {
            pMsg->set_int( 0, NET_TCP_ACCEPT );
            if( bRet )
            {
                pMsg->set_void( 0, pAcceptConnection );
            }
            msg::notify_net( pMsg, false );
        }

        while( bRet && ( 0 == nPostAccept ) && ( pServer->m_nPostAcceptNum < config::get()->tcp_max_post_accept ) )
        {
            nPostAccept = post_accept( pConnect );
        }

        while( bRet && ( 0 == nPostRecv ) && ( pTcp->m_nPostRecvNum < config::get()->tcp_max_post_recv ) )
        {
            nPostRecv = post_recv( pAcceptConnection );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : local port
    // m_szStr[1] : local ip
    // m_nData[2] : peer port
    // m_szStr[2] : peer ip
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::on_connect( xos::i_msg *& pMsg )
    {
        int ret = 0;

        connection * pConnect = (connection *)pMsg->get_void( 0, 0 );
        tcp * pTcp = pConnect->m_pTcp;
        void * pAioKey = pMsg->get_void( 1, 0 );
        bool bRet = pMsg->get_bool( 0, 0 );
        int nPostRecv = 0;

        if( bRet )
        {
            pTcp->m_local_ip = pMsg->get_str( 1, 0, 0, 0 );
            pTcp->m_nLocalPort = pMsg->get_int( 1, 0 );
            pTcp->m_peer_ip = pMsg->get_str( 2, 0, 0, 0 );
            pTcp->m_nPeerPort = pMsg->get_int( 2, 0 );
            pTcp->m_pAioKey = pAioKey;
            connection::list()->push_front( pConnect );
            pConnect->m_connection_iter = connection::list()->begin();
        }
        if( bRet )
        {
            pConnect->set_running();
            pConnect->lock_server();
        }
        {
            pMsg->set_int( 0, NET_TCP_CONNECT );
            msg::notify_net( pMsg, false );
        }

        while( bRet && ( 0 == nPostRecv ) && ( pTcp->m_nPostRecvNum < config::get()->tcp_max_post_recv ) )
        {
            nPostRecv = post_recv( pConnect );
        }

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : recv len
    // m_lpBuf[0] : recv buf
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::on_recv( xos::i_msg *& pMsg )
    {
        int ret = 0;

        connection * pConnect = (connection *)pMsg->get_void( 0, 0 );
        xos::i_buf * pBuf = pMsg->get_buf( 0, 0 );
        tcp * pTcp = pConnect->m_pTcp;
        bool bRet = pMsg->get_bool( 0, 0 );
        int nPostRecv = 0;

        if( bRet && state::running() && ( pConnect->running() || pConnect->shutdown() ) && !pConnect->m_pQuitAfterSend && !pConnect->m_pCloseAfterSend )
        {
            if( !pConnect->ssl() || ( pConnect->ssl() && pConnect->m_bSslHandShaked ) )
            {
                pConnect->net_timeout_ms = 0;
            }
            pConnect->lock_server();
            pMsg->set_int( 0, NET_TCP_RECV );
            msg::notify_net( pMsg, false );
            pBuf = 0;
        }

        xos_stl::release_interface( pBuf );
        pTcp->m_nPostRecvNum--;

        while( bRet && ( 0 == nPostRecv ) && ( pTcp->m_nPostRecvNum < config::get()->tcp_max_post_recv ) )
        {
            nPostRecv = post_recv( pConnect );
        }

        close_test( pConnect );

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_nData[1] : send bytes
    // m_lpBuf[0] : send buf
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::on_send( xos::i_msg *& pMsg )
    {
        int ret = 0;

        connection * pConnect = (connection *)pMsg->get_void( 0, 0 );
        tcp * pTcp = pConnect->m_pTcp;
        
        pTcp->m_nPostSendNum--;

        {
            pMsg->set_int( 0, NET_TCP_SEND );
            msg::notify_net( pMsg, false );
        }

        close_test( pConnect );

        return ret;
    }

    // 
    // m_bData[0] : true or false for opt result
    // 
    // m_nData[0] : cmd type
    // 
    // m_lpData[0] : UserKey
    // m_lpData[1] : AioKey
    // 
    int aio_tcp::on_close( xos::i_msg *& pMsg )
    {
        int ret = 0;

        connection * pConnect = (connection *)pMsg->get_void( 0, 0 );
        tcp * pTcp = pConnect->m_pTcp;
        LOG4( "closed = (%s:%d)(%s:%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort );
        {
            pMsg->set_int( 0, NET_TCP_CLOSE );
            msg::notify_net( pMsg, false );
        }

        return ret;
    }

} // cat
