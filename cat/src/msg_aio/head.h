/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_AIO_HEAD_H__
#define __MSG_AIO_HEAD_H__

#include "msg_aio.h"
#include "aio_tcp.h"
#include "aio_udp.h"

#endif // __MSG_AIO_HEAD_H__
