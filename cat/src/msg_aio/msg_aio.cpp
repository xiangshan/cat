/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_thread_dispatcher/head.h"
#include "../data_struct/data.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "msg_aio.h"
#include "msg_aio.h"
#include "aio_tcp.h"
#include "aio_udp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static xos_aio::i_aio * aio_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    msg_aio::msg_aio()
    {
    }

    msg_aio::~msg_aio()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int msg_aio::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            helper_thread_dispatcher obj;
            mgr::xos()->aio()->set_callback_thread( obj.net_thread() );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->aio()->create( &aio_ptr );
        }

        if( 0 == ret )
        {
            aio_ptr->set_callback( msg::on_aio );
        }

        return ret;
    }

    int msg_aio::term()
    {
        int ret = 0;
        xos_stl::release_interface( aio_ptr );
        return ret;
    }

    xos_aio::i_aio * msg_aio::get()
    {
        return aio_ptr;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // net ret msg proc

	int msg_aio::proc( xos::i_msg *& pMsg )
	{
        int ret = 0;

        if( 0 == ret )
        {
            aio_tcp obj;
            ret = obj.proc( pMsg );
        }

        if( 0 == ret )
        {
            aio_udp obj;
            ret = obj.proc( pMsg );
        }

		return ret;
	}

} // cat
