/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_PASS_THROUGH_HELPER_PASS_THROUGH_H__
#define __HELPER_PASS_THROUGH_HELPER_PASS_THROUGH_H__

namespace cat
{

    class helper_pass_through
    {
    public:
        helper_pass_through();
        ~helper_pass_through();

    public:
        static int add_thread( xos::i_callback * pThread );
        static int init();
        static int term();

    protected:
        int pass_through_impl( xos::i_msg *& pMsg );
        int pop( xos::i_msg *& pMsg );

    public:
        int proc( xos::i_msg *& pMsg );
        int push( xos::i_msg *& pMsg );

    };

} // cat

#endif // __HELPER_PASS_THROUGH_HELPER_PASS_THROUGH_H__
