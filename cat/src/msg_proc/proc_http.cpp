/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/container_impl.h"
#include "../helper_webapp/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "proc_plugin.h"
#include "proc_http.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	proc_http::proc_http()
	{
	}

	proc_http::~proc_http()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

	int proc_http::proc( xos::i_msg *& pMsg )
	{
		int ret = 0;

        task * pTask = ( task* )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        response * pResponse = pTask->m_pResponse;
        request * pRequest = pTask->m_pRequest;
        xos_common::i_property * pResTag = pResponse->m_pTagProp;
        xos_common::i_property * pReqTag = pRequest->m_pTagProp;
        const char * lpszHost = pReqTag->str( xos_http::HTTP_HOST );
        webapp * pApp = 0;

        if( 0 == ret )
        {
            pResTag->set( xos_http::HTTP_REQUEST_RET_CODE, xos_http::HTTP_RET_CODE_200 );
            pResTag->set( xos_http::HTTP_CONNECTION, "keep-alive" );
        }

        if( 0 == ret )
        {
            tcp * pTcp = pConnect->m_pTcp;
            LOG4( "http recv (%s:%d)(%s:%d)(%d) = %s", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum,
                pReqTag->str( xos_http::HTTP_REQUEST_URL ) );
        }

        if( ( 0 == ret ) && ( pApp = helper_webapp::find( lpszHost ) ) )
        {
            pTask->m_pWebApp = pApp;
        }

        if( ( 0 == ret ) && pApp )
        {
            proc_plugin obj;
            obj.call( pMsg );
            pApp = 0;
            ret = 1;
        }

        if( 0 == ret )
        {
            pResTag->set( xos_http::HTTP_REQUEST_RET_CODE, xos_http::HTTP_RET_CODE_404 );
            char buf[4096];
            mgr::xos()->http()->fmt_simple_html( buf, sizeof( buf ), 0, "not find", "not find host processor" );
            container_impl::get()->add_ret_str( pTask, buf );
            http_ret( pMsg );
        }

		return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int proc_http::http_quit( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task* )pMsg->get_void( 0, 0 );

        if( 0 == ret )
        {
            LOG4( "recv http quit cmd, quitting..." );
        }

        if( 0 == ret )
        {
            pTask->m_bQuitAfterSend = true;
        }

        if( 0 == ret )
        {
            ret = http_ret( pMsg );
        }

        return ret;
    }

    int proc_http::http_ret( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task* )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        response * pResponse = pTask->m_pResponse;
        request * pRequest = pTask->m_pRequest;
        xos_common::i_property * pResTag = pResponse->get_tag();
        xos_common::i_property * pReqTag = pRequest->get_tag();

        // 判断是否对方要求处理后关闭连接
        if( ( 0 == ret ) && ( 0 == mgr::xos()->crt()->strcmp( pReqTag->str( xos_http::HTTP_CONNECTION ), "close" ) ) )
        {
            pTask->m_bCloseAfterSend = true;
        }

        // 加入Date
        if( 0 == ret )
        {
            char buf[256];
            mgr::xos()->tm()->get_time();
            mgr::xos()->tm()->get_time_string_http_format( buf, sizeof( buf ), 0 );
            pResTag->set( xos_http::HTTP_DATE, buf );
        }

        // 加入Server,show off一下
        if( 0 == ret )
        {
            pResTag->set( xos_http::HTTP_SERVER, "xoskit http server cat" );
        }

        if( 0 == ret )
        {
            tcp * pTcp = pConnect->m_pTcp;
            LOG4( "http ret  (%s:%d)(%s:%d)(%d) = %s", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum,
                pReqTag->str( xos_http::HTTP_REQUEST_URL ) );
        }

        if( 0 == ret )
        {
            msg::notify_package( pMsg, PACKET_HTTP_COMPRESS, false );
        }

        return ret;
    }

} // cat
