/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "proc_tcp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	proc_tcp::proc_tcp()
	{
	}

	proc_tcp::~proc_tcp()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int proc_tcp::proc( xos::i_msg *& pMsg )
	{
        int ret = 0;

        task * pTask = ( task* )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        tcp * pTcp = pConnect->m_pTcp;
        response * pResponse = pTask->m_pResponse;
        request * pRequest = pTask->m_pRequest;
        xos::i_buf * pBuf = 0;

        while( ( 0 == ret ) && ( pBuf = ( xos::i_buf * )pRequest->m_pUnCompressList->front( 0 ) ) )
        {
            if( pTcp->m_pListenTcp )
            {
                LOG4( "server recv = %s", pBuf->get_data( 0, 0, 0 ) );
            }
            else
            {
                LOG4( "client recv = %s", pBuf->get_data( 0, 0, 0 ) );
            }
            pRequest->m_pUnCompressList->pop_front();
            pResponse->m_pRawDataList->push_back( pBuf );
        }

        if( pTcp->m_pListenTcp )
        {
            msg::notify( pMsg, THREAD_TYPE_PACKET, MSG_TYPE_PACKET, PACKET_TCP_COMPRESS, false, false );
        }
        else
        {
            aio_tcp obj;
            obj.post_close( pConnect, 0, 0 );
        }

        return ret;
	}

} // cat
