/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_PROC_PROC_HTTP_H__
#define __MSG_PROC_PROC_HTTP_H__

namespace cat
{
    class task;
    class proc_http
    {
    public:
        proc_http();
        ~proc_http();

    public:
        int http_quit( xos::i_msg *& pMsg );
        int http_ret( xos::i_msg *& pMsg );
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_PROC_PROC_HTTP_H__
