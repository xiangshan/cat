/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_webapp/head.h"
#include "../data_struct/head.h"
#include "../macro/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "proc_plugin.h"
#include "proc_http.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	proc_plugin::proc_plugin()
	{
	}

	proc_plugin::~proc_plugin()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

	int proc_plugin::proc( xos::i_msg *& pMsg )
	{
        int ret = 0;
        ret = call( pMsg );
        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

	int proc_plugin::call( xos::i_msg *& pMsg )
	{
        int ret = 0;

        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        webapp * pApp = pTask->m_pWebApp;

        if( 0 == ret )
        {
            app_plugin obj( pApp );
            result = obj.proc( pTask );
        }

        switch( result )
        {
        case icat::i_plugin::RET_CODE_DONE:
            {
                proc_http obj;
                ret = obj.http_ret( pMsg );
            }
            break;
        case icat::i_plugin::RET_CODE_QUIT:
            {
                proc_http obj;
                ret = obj.http_quit( pMsg );
            }
            break;
            // RET_CODE_BREAK说明已经异步调用，不用处理
            // RET_CODE_BREAK不会到这里。
        case icat::i_plugin::RET_CODE_BREAK:
        case icat::i_plugin::RET_CODE_ASYNC:
            {
                ret = 1;
            }
            break;
        }

        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

} // cat
