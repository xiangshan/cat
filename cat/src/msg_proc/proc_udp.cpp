/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "proc_udp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	proc_udp::proc_udp()
	{
	}

	proc_udp::~proc_udp()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int proc_udp::proc( xos::i_msg *& pMsg )
	{
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        response * pResponse = pTask->m_pResponse;
        request * pRequest = pTask->m_pRequest;
        xos::i_buf * pBuf = 0;

        while( ( 0 == ret ) && ( pBuf = ( xos::i_buf * )pRequest->m_pUnCompressList->front( 0 ) ) )
        {
            LOG4( "udp recv = %s", pBuf->get_data( 0, 0, 0 ) );
            pRequest->m_pUnCompressList->pop_front();
            pResponse->m_pRawDataList->push_back( pBuf );
        }

        {
            msg::notify( pMsg, THREAD_TYPE_PACKET, MSG_TYPE_PACKET, PACKET_UDP_COMPRESS, false, false );
        }

        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

} // cat
