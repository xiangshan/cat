/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "proc_plugin.h"
#include "proc_http.h"
#include "proc_tcp.h"
#include "proc_udp.h"
#include "msg_proc.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    msg_proc::msg_proc()
    {
    }

    msg_proc::~msg_proc()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int msg_proc::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PROC_PLUGIN:
            {   
                proc_plugin obj;
                obj.proc( pMsg );
            }
            break;
        case PROC_HTTP:
            {   
                proc_http obj;
                obj.proc( pMsg );
            }
            break;
        case PROC_TCP:
            {
                proc_tcp obj;
                obj.proc( pMsg );
            }
            break;
        case PROC_UDP:
            {
                proc_udp obj;
                obj.proc( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  

} // cat
