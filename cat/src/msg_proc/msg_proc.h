/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_PROC_MSG_PROC_H__
#define __MSG_PROC_MSG_PROC_H__

namespace cat
{

    class msg_proc
    {
    public:
        msg_proc();
        ~msg_proc();

    public:
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_PROC_MSG_PROC_H__
