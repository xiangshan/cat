/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __STEP_STRUCT_STEP_H__
#define __STEP_STRUCT_STEP_H__

#include "../tools/head.h"

namespace cat
{
    class plugin;
    class chain;
    class step : public xos_stl::mem_item< step, thread_lock >
    {
    public:
        typedef step T;

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        step();
        ~step();

    public:
        std::string m_jsp_file;
        std::string m_action;
        std::string m_method;
        step::ITER m_iter;

        plugin * m_pPlugin;
        chain * m_pChain;

    protected:
        int init_data();
        
    public:
        int release();
        int init();
        int term();
        
    };

} // cat

#endif // __STEP_STRUCT_STEP_H__
