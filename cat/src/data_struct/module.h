/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MODULE_STRUCT_MODULE_H__
#define __MODULE_STRUCT_MODULE_H__

#include "../tools/head.h"

namespace cat
{
    class module : public xos_stl::mem_item< module, thread_lock >
    {
    public:
        typedef module T;

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        module();
        ~module();

    public:
        icat::i_plugin_mgr * m_pPluginMgr;

    protected:
        int init_data();
        
    public:
        bool operator < ( const module & right ) const;
        int release();
        int init();
        int term();
        
    };

} // cat

#endif // __MODULE_STRUCT_MODULE_H__
