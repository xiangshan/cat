/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_WEBAPP_H__
#define __DATA_STRUCT_WEBAPP_H__

#include "../tools/head.h"
#include "module.h"
#include "plugin.h"

namespace cat
{
    class step;
    class webapp : public xos_stl::mem_item< xos::com_object_no_ref< webapp >, thread_lock >,
        public icat::i_webapp
    {
    public:
        typedef std::list< icat::i_listener * > LISTENER_LIST;
        typedef std::map< std::string, LISTENER_LIST* > LISTENER_MAP;

        typedef xos_stl::xos_map< std::string, module, thread_lock > MODULE_MAP;
        typedef MODULE_MAP::iterator MODULE_ITER;

        typedef xos::com_object_no_ref< webapp > T;

    public:
        XOS_BEGIN_COM_MAP( webapp, icat::i_webapp )
            XOS_COM_INTERFACE_ENTRY( icat::i_webapp )
        XOS_END_COM_MAP()

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        webapp();
        ~webapp();

    public:
        xos_common::i_property * m_pConfigProperty;
        xos_common::i_property * m_pProperty;
        std::string m_webapp_path;
        std::string m_host;
        int m_nPort;

    public:
        plugin::LIST m_listener_plugin_list;
        plugin::LIST m_filter_plugin_list;

        module::LIST m_listener_mgr_list;
        module::LIST m_filter_mgr_list;
        module::LIST m_action_mgr_list;

        MODULE_MAP * m_pActionMgrMap;
        MODULE_MAP * m_pJspMgrMap;
        LISTENER_MAP * m_pListenerMap;

    protected:
        int init_data();

    public:
        xos_common::i_property * prop();
        
    public:
        module * find_action_module( const char * lpszName );
        module * find_jsp_module( const char * lpszName );
        int init();
        int term();
        
    };

} // cat

#endif // __DATA_STRUCT_WEBAPP_H__
