/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_webapp/app_plugin.h"
#include "../impl/container_impl.h"
#include "webapp.h"
#include "module.h"
#include "plugin.h"
#include "chain.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static chain::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    chain::chain()
    {
        init_data();
    }

    chain::~chain()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int chain::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int chain::init_data()
    {
        int ret = 0;

        m_step_iter = m_step_list.end();
        m_pCurStep = 0;
        m_pImpl = 0;
        m_pTask = 0;

        m_pszAdvise = 0;
        m_pszEvent = 0;
        m_pszType = 0;

        m_bIsGoBack = false;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int chain::init()
    {
        int ret = 0;
        return ret;
    }

    int chain::term()
    {
        int ret = 0;

        m_step_list.put_back_to_pool( true );
        // 栈没有所有权
        m_call_stack.clear();

        init_data();

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode chain::async_call( const char * lpszModule, const char * lpszAction, const char * lpszMethod, bool bSlowJob )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_ASYNC;
        module * pM = m_pImpl->find_action_module( lpszModule );
        icat::i_plugin * pPlugin = 0;
        int ret = 0;

        if( ( 0 == ret ) && !pM )
        {
            char buf[256] = { 0 };
            container_impl::get()->xos()->crt()->sprintf( buf, sizeof( buf ), "not find module = %s", lpszModule );
            container_impl::get()->add_ret_str( m_pTask, buf );
            result = icat::i_plugin::RET_CODE_DONE;
            ret = 1;
        }

        if( 0 == ret )
        {
            ret = pM->m_pPluginMgr->create( lpszAction, &pPlugin );
        }

        if( 0 == ret )
        {
            result = async_call( pPlugin, lpszMethod, true, bSlowJob );
        }

        return result;
    }

    icat::i_plugin::enumRetCode chain::async_call( icat::i_plugin * pPlugin, const char * lpszMethod, bool bTakeOwner, bool bSlowJob )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_ASYNC;

        xos::i_unknown * pUnk = 0;
        pPlugin->query_interface( xos::i_unknown_id, ( void** )&pUnk );

        plugin * pP = plugin::get_item_from_pool( true );
        pP->init();
        pP->m_module = pUnk->module_name();
        pP->m_bRelease = bTakeOwner;
        pP->m_pPlugin = pPlugin;

        step * pS = step::get_item_from_pool( true );
        pS->init();
        pS->m_method = lpszMethod ? lpszMethod : "";
        pS->m_action = pUnk->class_name();
        pS->m_pChain = this;
        pS->m_pPlugin = pP;
        pP = 0;

        // 加到最深的栈里
        // 这样pop栈时，会剩下最后这个异步的step
        m_call_stack.push_back( pS );
        pS = 0;

        container_impl::get()->notify( m_pTask, bSlowJob );

        return result;
    }

    icat::i_plugin::enumRetCode chain::async_call_jsp( const char * lpszJspFile, bool bSlowJob )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_ASYNC;
        container_impl::get()->add_ret_str( m_pTask, "async_call_jsp not implement" );
        return result;
    }

    icat::i_plugin::enumRetCode chain::async_call_self( const char * lpszMethod, bool bSlowJob )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_ASYNC;
        result = async_call( m_pCurStep->m_pPlugin->m_pPlugin, lpszMethod, false, bSlowJob );
        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode chain::call( const char * lpszModule, const char * lpszAction, const char * lpszMethod )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_BREAK;
        module * pM = m_pImpl->find_action_module( lpszModule );
        icat::i_plugin * pPlugin = 0;
        int ret = 0;

        if( ( 0 == ret ) && !pM )
        {
            char buf[256] = { 0 };
            container_impl::get()->xos()->crt()->sprintf( buf, sizeof( buf ), "not find module = %s", lpszModule );
            container_impl::get()->add_ret_str( m_pTask, buf );
            result = icat::i_plugin::RET_CODE_DONE;
            ret = 1;
        }

        if( 0 == ret )
        {
            ret = pM->m_pPluginMgr->create( lpszAction, &pPlugin );
        }

        if( 0 == ret )
        {
            result = call( pPlugin, lpszMethod, true );
        }

        return result;
    }

    icat::i_plugin::enumRetCode chain::call( icat::i_plugin * pPlugin, const char * lpszMethod, bool bTakeOwner )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_BREAK;

        xos::i_unknown * pUnk = 0;
        pPlugin->query_interface( xos::i_unknown_id, ( void** )&pUnk );

        plugin * pP = plugin::get_item_from_pool( true );
        pP->init();
        pP->m_module = pUnk->module_name();
        pP->m_bRelease = bTakeOwner;
        pP->m_pPlugin = pPlugin;

        step * pS = step::get_item_from_pool( true );
        pS->init();
        pS->m_method = lpszMethod ? lpszMethod : "";
        pS->m_action = pUnk->class_name();
        pS->m_pChain = this;
        pS->m_pPlugin = pP;
        pP = 0;

        // 入栈
        m_call_stack.push_front( m_pCurStep );

        {
            app_plugin obj( m_pImpl );
            result = obj.call( m_pTask, pS );
        }

        // 出栈
        m_pCurStep = m_call_stack.front();
        m_call_stack.pop_front();

        xos_stl::release_interface( pS );

        return result;
    }

    icat::i_plugin::enumRetCode chain::call_jsp( const char * lpszJspFile )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_BREAK;
        xos::i_misc * pMisc = container_impl::get()->xos()->misc();

        step * pS = step::get_item_from_pool( true );
        pS->init();
        pS->m_pChain = this;

        {
            char file[4096];
            file[0] = 0;
            pMisc->path_append( file, m_pCurStep->m_pPlugin->m_module.c_str() );
            pMisc->path_append( file, m_pCurStep->m_action.c_str() );
            pMisc->path_append( file, lpszJspFile );
            pS->m_jsp_file = file;
        }

        // 入栈
        m_call_stack.push_front( m_pCurStep );

        {
            app_plugin obj( m_pImpl );
            result = obj.call( m_pTask, pS );
        }

        // 出栈
        m_pCurStep = m_call_stack.front();
        m_call_stack.pop_front();

        xos_stl::release_interface( pS );

        return result;
    }

    icat::i_plugin::enumRetCode chain::call_self( const char * lpszMethod )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_BREAK;
        result = call( m_pCurStep->m_pPlugin->m_pPlugin, lpszMethod, false );
        return result;
    }

    int chain::get_listen_event( const char *& lpszType, const char *& lpszEvent, const char *& lpszAdvise )
    {
        int ret = 0;

        lpszAdvise = m_pszAdvise;
        lpszEvent = m_pszEvent;
        lpszType = m_pszType;

        return ret;
    }

    int chain::set_listen_event( const char * lpszType, const char * lpszEvent, const char * lpszAdvise )
    {
        int ret = 0;

        m_pszAdvise = lpszAdvise;
        m_pszEvent = lpszEvent;
        m_pszType = lpszType;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode chain::run( icat::i_plugin::enumRetCode result )
    {
        using namespace icat;

        i_task * pTask = m_pTask;
        app_plugin obj( m_pImpl );
        int ret = 0;

        // 如果已经完成反向，没有内容了，就是完成了
        if( ( 0 == ret ) && ( m_step_iter == m_step_list.begin() ) && ( 0 == m_call_stack.size() ) && is_go_back() )
        {
            ret = 1;
        }

        // 如果是本轮异步的后续，则直接无视，等下一轮才处理异步
        if( ( 0 == ret ) && ( icat::i_plugin::RET_CODE_ASYNC == result ) )
        {
            ret = 1;
        }

        // 非第一轮运行的quit被无视，因为那太没有逻辑了。
        // 第一轮的quit，只是给自己留下一个退出后门
        if( ( 0 == ret ) && ( icat::i_plugin::RET_CODE_QUIT == result ) )
        {
            result = icat::i_plugin::RET_CODE_DONE;
        }

        // 异步处理只是自陷，完成后会恢复后续执行，不会造成break
        // 异步处理时，m_call_stack中只会有一个节点
        // 因为启动异步时，会清掉栈中所有其它节点
        // 就是消灭原来的栈空间，不会原路返回
        if( ( 0 == ret ) && ( m_call_stack.size() > 0 ) )
        {
            step * pStep = m_call_stack.front();
            m_call_stack.pop_front();
            result = obj.call( pTask, pStep );
            xos_stl::release_interface( pStep );
        }

        // 正向处理
        while( ( 0 == ret ) && ( i_plugin::RET_CODE_DONE == result ) && ( m_step_iter != m_step_list.end() ) && !is_go_back() )
        {
            step * pStep = *m_step_iter++;
            result = obj.call( pTask, pStep );
        }

        // 正向完成时反转
        if( ( 0 == ret ) && ( i_plugin::RET_CODE_DONE == result ) && ( m_step_iter == m_step_list.end() ) && !is_go_back() )
        {
            set_go_back();
        }

        // 正向中途break的话就反转，丢掉正向后续节点
        // 反向时break无效，set_go_back有状态检查
        if( ( 0 == ret ) && ( i_plugin::RET_CODE_BREAK == result ) )
        {
            result = i_plugin::RET_CODE_DONE;
            set_go_back();
        }

        // 反向处理。此时break、quit均无效。但可以异步自陷。
        while( ( 0 == ret ) && ( i_plugin::RET_CODE_ASYNC != result ) && ( m_step_iter != m_step_list.begin() ) && is_go_back() )
        {
            step * pStep = *(--m_step_iter);
            result = obj.call( pTask, pStep );
        }

        // break状态处理完后，变成done状态，因为已经完成了
        // 总之，只要没有异步，并执行完了，那就是完成了
        if( i_plugin::RET_CODE_BREAK == result )
        {
            result = i_plugin::RET_CODE_DONE;
        }

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    const char * chain::action()
    {
        return m_pCurStep->m_action.c_str();
    }

    const char * chain::method()
    {
        return m_pCurStep->m_method.c_str();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int chain::set_go_back()
    {
        int ret = 0;

        if( !m_bIsGoBack )
        {
            if( m_step_iter != m_step_list.begin() )
            {
                m_step_iter--;
            }
            m_bIsGoBack = true;
        }

        return ret;
    }

    bool chain::is_go_back()
    {
        return m_bIsGoBack;
    }

} // cat
