/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "head.h"
#include "pool.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    pool::pool()
    {
    }

    pool::~pool()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int pool::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = chain::init_all();
        }

        if( 0 == ret )
        {
            ret = connection::init_all();
        }

        if( 0 == ret )
        {
            ret = cookie::init_all();
        }

        if( 0 == ret )
        {
            ret = data::init_all();
        }

        if( 0 == ret )
        {
            ret = module::init_all();
        }

        if( 0 == ret )
        {
            ret = plugin::init_all();
        }

        if( 0 == ret )
        {
            ret = request::init_all();
        }

        if( 0 == ret )
        {
            ret = response::init_all();
        }

        if( 0 == ret )
        {
            ret = session::init_all();
        }

        if( 0 == ret )
        {
            ret = step::init_all();
        }

        if( 0 == ret )
        {
            ret = task::init_all();
        }

        if( 0 == ret )
        {
            ret = tcp::init_all();
        }

        if( 0 == ret )
        {
            ret = udp::init_all();
        }

        if( 0 == ret )
        {
            ret = webapp::init_all();
        }

        return ret;
    }

    int pool::term()
    {
        int ret = 0;

        chain::term_all();
        connection::term_all();
        cookie::term_all();
        data::term_all();
        module::term_all();
        plugin::term_all();
        request::term_all();
        response::term_all();
        session::term_all();
        step::term_all();
        task::term_all();
        tcp::term_all();
        udp::term_all();
        webapp::term_all();

        return ret;
    }

} // cat
