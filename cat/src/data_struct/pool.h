/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_POOL_H__
#define __DATA_STRUCT_POOL_H__

#include "../tools/thread_lock.h"

//////////////////////////////////////////////////////////////////////////////////
// 

namespace cat
{

	class pool
    {
    public:
        pool();
        ~pool();

    public:
        static int init();
		static int term();

    };
} // cat

#endif // __DATA_STRUCT_POOL_H__
