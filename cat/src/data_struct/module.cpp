/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "module.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static module::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    module::module()
    {
        init_data();
    }

    module::~module()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int module::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int module::init_data()
    {
        int ret = 0;

        m_pPluginMgr = 0;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    bool module::operator < ( const module & right ) const
    {
        return m_pPluginMgr->get_priority( 0 ) < right.m_pPluginMgr->get_priority( 0 );
    }

    // 
    // �ͷŽӿ�
    // 
    int module::release()
    {
        int ret = 0;

        term();
		put_back_to_pool( this, true );

        return ret;
    }

    int module::init()
    {
        int ret = 0;
        return ret;
    }

    int module::term()
    {
        int ret = 0;
        if( m_pPluginMgr )
        {
            xos::i_dynamic * pDll = m_pPluginMgr->get_module();
            xos_stl::release_interface( m_pPluginMgr );
            xos_stl::release_interface( pDll );
        }
        init_data();
        return ret;
    }


} // cat
