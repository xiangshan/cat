/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_TASK_H__
#define __DATA_STRUCT_TASK_H__

#include "../tools/head.h"

namespace cat
{
    class connection;
    class response;
    class request;
    class webapp;

    class task : public xos_stl::mem_item< xos::com_object_no_ref< task >, thread_lock >, 
        public icat::i_task
    {
    public:
        typedef xos::com_object_no_ref< task > T;

    public:
        XOS_BEGIN_COM_MAP( task, icat::i_task )
            XOS_COM_INTERFACE_ENTRY( icat::i_task )
        XOS_END_COM_MAP()

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        task();
        ~task();

    public:
        icat::i_chain * m_pChain;
        webapp * m_pWebApp;

        connection * m_pConnection;
        response * m_pResponse;
        request * m_pRequest;

        bool m_bCloseAfterSend;
        bool m_bQuitAfterSend;
        bool m_bRecving;

        task::ITER m_task_iter;

        std::string m_peer_ip;
        int m_nPeerPort;

    protected:
        xos_common::i_property * m_pProperty;
        bool m_bServerLocked;
        
    public:
        static task * init_task( connection * pConnect, bool bAddToList );
        static int term_task( task *& pTask );
        int swap_http_request( task * pTask );
        int un_lock_server_for_request_data();
        int lock_server_for_request_data();
        int quit_close_test();
        int init();
        int term();

    protected:
        xos_common::i_variant * helper_vt( const char * lpszVariable );
        int init_data();

        // 
        // icat::i_task
        // 
    public:
        int add_ret_data( const char * lpszData, int nLen );
        int add_ret_data( xos::i_buf *& pBuf );
        int add_ret_str( const char * lpszStr );

        int set_chain( icat::i_chain * pChain );
        icat::i_chain * get_chain();

        icat::i_webapp * app();

        xos_common::i_variant * vt( const char * lpszVariable );
        xos::i_list * list( const char * lpszVariable );
        const char * str( const char * lpszVariable );
        double dbl( const char * lpszVariable );
        float flt( const char * lpszVariable );
        int it( const char * lpszVariable );

        icat::i_response * get_response();
        icat::i_request * get_request();

        const char * base_path();

        xos_common::i_property * prop();
        
    };

} // cat

#endif // __DATA_STRUCT_TASK_H__
