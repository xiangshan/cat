/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_TCP_H__
#define __DATA_STRUCT_TCP_H__

#include "../tools/thread_lock.h"

namespace cat
{
    class connection;

	class tcp : public xos_stl::mem_item< tcp, thread_lock >
    {
    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        typedef tcp T;

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        tcp();
        ~tcp();

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );
        static int init( tcp *& pTcp, connection *& pConnect );
        static int term( tcp *& pTcp );

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        xos_http::i_parse_request * m_pParseRequest;
        xos_http::i_compose_return * m_pComposeRet;

        connection * m_pConnection;
        tcp * m_pListenTcp;
        void * m_pAioKey;

        int m_nPostAcceptNum;
        int m_nPostRecvNum;
        int m_nPostSendNum;

		std::string m_local_ip;
        int m_nLocalPort;

        std::string m_peer_ip;
        int m_nPeerPort;

    protected:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        int init_data();

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
		int release();
        int init();
        int term();
    };
} // cat

#endif // __DATA_STRUCT_TCP_H__
