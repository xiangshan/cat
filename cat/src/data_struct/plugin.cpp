/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "plugin.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static plugin::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    plugin::plugin()
    {
        init_data();
    }

    plugin::~plugin()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int plugin::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int plugin::init_data()
    {
        int ret = 0;

        m_module = "";
        m_class = "";
        m_module = "";
        m_nPriority = 0;
        m_bRelease = true;
        m_pPlugin = 0;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    const plugin & plugin::operator=( const plugin & right )
    {
        if( this == &right )
        {
            return *this;
        }

        m_pPlugin = right.m_pPlugin;
        m_module = right.m_module;
        m_class = right.m_class;
        m_nPriority = right.m_nPriority;

        return *this;
    }

    bool plugin::operator < ( const plugin & right ) const
    {
        return m_nPriority < right.m_nPriority;
    }

    const char * plugin::name()
    {
        return m_class.c_str();
    }

    int plugin::priority()
    {
        return m_nPriority;
    }

    int plugin::init()
    {
        int ret = 0;
        return ret;
    }

    int plugin::term()
    {
        int ret = 0;

        if( !m_bRelease )
        {
            m_pPlugin = 0;
        }

        xos_stl::release_interface( m_pPlugin );

        init_data();

        return ret;
    }

    // 
    // �ͷŽӿ�
    // 
    int plugin::release()
    {
        int ret = 0;

        term();
		put_back_to_pool( this, true );

        return ret;
    }


} // cat
