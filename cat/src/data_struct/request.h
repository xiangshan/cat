/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_REQUEST_H__
#define __DATA_STRUCT_REQUEST_H__

#include "../tools/head.h"

namespace cat
{
    class task;

    class request : public xos_stl::mem_item< xos::com_object_no_ref< request >, thread_lock >, 
        public icat::i_request
    {
    public:
        typedef xos::com_object_no_ref< request > T;

    public:
        XOS_BEGIN_COM_MAP( request, icat::i_request )
            XOS_COM_INTERFACE_ENTRY( icat::i_request )
        XOS_END_COM_MAP()

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        request();
        ~request();

    public:
        xos_common::i_property * m_pParamProp;
        xos_common::i_property * m_pPathProp;
        xos_common::i_property * m_pTagProp;
        xos::i_list * m_pUnCompressList;
        xos::i_list * m_pUnPackageList;
        xos::i_list * m_pUnEnctyptList;
        xos::i_list * m_pRecvList;

        task * m_pTask;

    protected:
        xos_common::i_property * m_pProperty;

    protected:
        int init_data();
        
    public:
        int swap_http_request( request * pRequest );
        int init();
        int term();

        // 
        // icat::i_request methods
        // 
    public:
        xos_common::i_property * get_param();
        xos_common::i_property * get_path();
        xos_common::i_property * get_tag();
        icat::i_task * get_task();
        xos::i_list * get_data();

    public:
        xos_common::i_property * prop();
        
    };

} // cat

#endif // __DATA_STRUCT_REQUEST_H__
