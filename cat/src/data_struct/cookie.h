/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_COOKIE_H__
#define __DATA_STRUCT_COOKIE_H__

#include "../tools/head.h"

namespace cat
{
    class cookie : public xos_stl::mem_item< cookie, thread_lock >
    {
    public:
        typedef cookie T;

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        cookie();
        ~cookie();

    public:

    protected:
        int init_data();
        
    public:
        int release();
        int init();
        int term();
        
    };

} // cat

#endif // __DATA_STRUCT_COOKIE_H__
