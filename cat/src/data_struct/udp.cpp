/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "connection.h"
#include "udp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    static udp::POOL * pool_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    udp::udp()
    {
        init_data();
    }

    udp::~udp()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int udp::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    int udp::init( udp *& pUdp, connection *& pConnect )
    {
        int ret = 0;

        if( 0 == ret )
        {
            pConnect = connection::get_item_from_pool( true );
            pConnect->init();
            pUdp = udp::get_item_from_pool( true );
            pUdp->init();
        }

        if( 0 == ret )
        {
            pUdp->m_pConnection = pConnect;
            pConnect->m_pUdp = pUdp;
        }

        if( 0 == ret )
        {
            pConnect->set_udp();
        }

        return ret;
    }

    int udp::term( udp *& pUdp )
    {
        int ret = 0;
        connection * pConnect = pUdp->m_pConnection;
        xos_stl::release_interface( pUdp );
        xos_stl::release_interface( pConnect );
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int udp::init_data()
    {
        int ret = 0;

        m_nPostRecvNum = 0;
        m_nPostSendNum = 0;
        m_nPort = 0;
        m_pConnection = 0;
        m_pAioKey = 0;
        m_ip = "";

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	int udp::init()
    {
        int ret = 0;
        return ret;
    }

    int udp::term()
    {
        int ret = 0;
        init_data();
        return ret;
    }

    int udp::release()
    {
        int ret = 0;
		term();
		put_back_to_pool( this, true );
        return ret;
    }

} // cat
