/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "connection.h"
#include "tcp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	static tcp::POOL * pool_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    tcp::tcp()
    {
        init_data();
    }

    tcp::~tcp()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int tcp::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    int tcp::init( tcp *& pTcp, connection *& pConnect )
    {
        int ret = 0;

        if( 0 == ret )
        {
            pConnect = connection::get_item_from_pool( true );
            pConnect->init();
            pTcp = tcp::get_item_from_pool( true );
            pTcp->init();
        }

        if( 0 == ret )
        {
            pTcp->m_pConnection = pConnect;
            pConnect->m_pTcp = pTcp;
        }

        return ret;
    }

    int tcp::term( tcp *& pTcp )
    {
        int ret = 0;
        connection * pConnect = pTcp->m_pConnection;
        xos_stl::release_interface( pTcp );
        xos_stl::release_interface( pConnect );
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int tcp::init_data()
    {
        int ret = 0;

        m_pParseRequest = 0;
        m_pComposeRet = 0;

        m_nPostAcceptNum = 0;
        m_nPostRecvNum = 0;
        m_nPostSendNum = 0;

        m_pListenTcp = 0;
        m_pConnection = 0;
        m_pAioKey = 0;

        m_nLocalPort = 0;
        m_local_ip = "";

        m_nPeerPort = 0;
        m_peer_ip = "";

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	int tcp::init()
    {
        int ret = 0;
        return ret;
    }

    int tcp::term()
    {
        int ret = 0;

        xos_stl::release_interface( m_pParseRequest );
        xos_stl::release_interface( m_pComposeRet );

        init_data();

        return ret;
    }

    int tcp::release()
    {
        int ret = 0;
		term();
		put_back_to_pool( this, true );
        return ret;
    }

} // cat
