/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "plugin.h"
#include "step.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static step::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    step::step()
    {
        init_data();
    }

    step::~step()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int step::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int step::init_data()
    {
        int ret = 0;

        m_jsp_file = "";
        m_action = "";
        m_method = "";
        m_pPlugin = 0;
        m_pChain = 0;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int step::init()
    {
        int ret = 0;
        return ret;
    }

    int step::term()
    {
        int ret = 0;
        xos_stl::release_interface( m_pPlugin );
        init_data();
        return ret;
    }

    // 
    // �ͷŽӿ�
    // 
    int step::release()
    {
        int ret = 0;
        term();
		put_back_to_pool( this, true );
        return ret;
    }


} // cat
