/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __CHAIN_STRUCT_CHAIN_H__
#define __CHAIN_STRUCT_CHAIN_H__

#include "../tools/head.h"
#include "step.h"

namespace cat
{
    class webapp;
    class chain : public xos_stl::mem_item< xos::com_object_no_ref< chain >, thread_lock >,
        public icat::i_chain
    {
    public:
        typedef xos::com_object_no_ref< chain > T;

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        XOS_BEGIN_COM_MAP( chain, cls_chain )
            XOS_COM_INTERFACE_ENTRY( icat::i_chain )
        XOS_END_COM_MAP()

    public:
        chain();
        ~chain();

    public:
        icat::i_task * m_pTask;
        step * m_pCurStep;
        webapp * m_pImpl;

        const char * m_pszAdvise;
        const char * m_pszEvent;
        const char * m_pszType;

        step::LIST m_call_stack;
        step::LIST m_step_list;
        step::ITER m_step_iter;

        bool m_bIsGoBack;

    protected:
        int init_data();
        
    public:
        int init();
        int term();

        // 
        // icat::i_chain methods
        // 
    public:
        icat::i_plugin::enumRetCode async_call( const char * lpszModule, const char * lpszAction, const char * lpszMethod, bool bSlowJob );
        icat::i_plugin::enumRetCode async_call( icat::i_plugin * pPlugin, const char * lpszMethod, bool bTakeOwner, bool bSlowJob );
        icat::i_plugin::enumRetCode async_call_jsp( const char * lpszJspFile, bool bSlowJob );
        icat::i_plugin::enumRetCode async_call_self( const char * lpszMethod, bool bSlowJob );

        icat::i_plugin::enumRetCode call( const char * lpszModule, const char * lpszAction, const char * lpszMethod );
        icat::i_plugin::enumRetCode call( icat::i_plugin * pPlugin, const char * lpszMethod, bool bTakeOwner );
        icat::i_plugin::enumRetCode call_jsp( const char * lpszJspFile );
        icat::i_plugin::enumRetCode call_self( const char * lpszMethod );

        int get_listen_event( const char *& lpszType, const char *& lpszEvent, const char *& lpszAdvise );
        int set_listen_event( const char * lpszType, const char * lpszEvent, const char * lpszAdvise );

        icat::i_plugin::enumRetCode run( icat::i_plugin::enumRetCode result );

        const char * action();
        const char * method();
        
        int set_go_back();
        bool is_go_back();
        
    };

} // cat

#endif // __CHAIN_STRUCT_CHAIN_H__
