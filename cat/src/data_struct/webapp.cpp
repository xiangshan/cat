/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/container_impl.h"
#include "../impl/mgr.h"
#include "webapp.h"
#include "chain.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static webapp::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    webapp::webapp()
    {
        init_data();
    }

    webapp::~webapp()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int webapp::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int webapp::init_data()
    {
        int ret = 0;

        m_pActionMgrMap = 0;
        m_pJspMgrMap = 0;
        m_pListenerMap = 0;

        m_pConfigProperty = 0;
        m_pProperty = 0;

        m_webapp_path = "";
        m_host = "";
        m_nPort = 0;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    xos_common::i_property * webapp::prop()
    {
        return m_pProperty;
    }

    module * webapp::find_action_module( const char * lpszName )
    {
        module * pRet = 0;

        webapp::MODULE_ITER iter = m_pActionMgrMap->find( lpszName );

        if( iter != m_pActionMgrMap->end() )
        {
            pRet = iter->second;
        }

        return pRet;
    }

    module * webapp::find_jsp_module( const char * lpszName )
    {
        module * pRet = 0;

        webapp::MODULE_ITER iter = m_pJspMgrMap->find( lpszName );

        if( iter != m_pJspMgrMap->end() )
        {
            pRet = iter->second;
        }

        return pRet;
    }

    int webapp::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void** )&m_pProperty );
        }

        if( 0 == ret )
        {
            xos_stl::init_map( m_pActionMgrMap );
            xos_stl::init_map( m_pJspMgrMap );
            xos_stl::init_obj( m_pListenerMap );
        }

        return ret;
    }

    int webapp::term()
    {
        int ret = 0;

        if( m_pListenerMap )
        {
            for( LISTENER_MAP::iterator it = m_pListenerMap->begin(); it != m_pListenerMap->end(); ++it )
            {
                LISTENER_LIST * pList = it->second;
                xos_stl::term_obj( pList );
            }
            m_pListenerMap->clear();
            xos_stl::term_obj( m_pListenerMap );
        }

        if( m_pActionMgrMap )
        {
            m_pActionMgrMap->clear();
            xos_stl::term_map( m_pActionMgrMap, true );
        }

        if( m_pJspMgrMap )
        {
            xos_stl::term_map( m_pJspMgrMap, true );
        }

        m_listener_plugin_list.put_back_to_pool( true );
        m_filter_plugin_list.put_back_to_pool( true );

        m_listener_mgr_list.put_back_to_pool( true );
        m_filter_mgr_list.put_back_to_pool( true );
        m_action_mgr_list.put_back_to_pool( true );

        xos_stl::release_interface( m_pConfigProperty );
        xos_stl::release_interface( m_pProperty );

        init_data();

        return ret;
    }


} // cat
