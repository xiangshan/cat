/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_CONNECTION_H__
#define __DATA_STRUCT_CONNECTION_H__

#include "../tools/thread_lock.h"
#include "task.h"

namespace cat
{
    class data;
    class tcp;
    class udp;

	class connection : public xos_stl::mem_item< connection, thread_lock >
    {
    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        typedef connection T;
        enum enumState
        {
            STATE_NONE          = 0x0000,

            STATE_CONNECTING    = 0x0010,
            STATE_INITING       = 0x0020,
            STATE_RUNNING       = 0x0040,
            STATE_SHUTDOWN      = 0x0080,
            STATE_CLOSING       = 0x0100,
            STATE_CLOSED        = 0x0200,

            STATE_NEED_CLOSE    = 0x0400,
            STATE_TIME_OUT      = 0x0800,
            STATE_DATA_ERR      = 0x1000
        };
        enum enumType
        {
            TYPE_NONE,
            TYPE_HTTP_LISTEN,
            TYPE_HTTP_DATA,
            TYPE_TCP_LISTEN,
            TYPE_TCP_DATA,
            TYPE_UDP
        };

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        connection();
        ~connection();

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );
        static int heart();

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        xos::xos_i64 m_tLastHeartTimestampS;
        int net_timeout_ms;

        enumType m_eType;
        int m_nState;

        xos::i_buf * m_pCloseAfterSend;
        xos::i_buf * m_pQuitAfterSend;

        xos_encrypt::i_encrypt * m_pEncrypt;
        bool m_bSslHandShaked;
        std::string m_str_key;

        connection::ITER m_connection_iter;
        tcp * m_pTcp;
        udp * m_pUdp;

        task::LIST m_task_list;
        task::T * m_pTask;

    public:
        // 保证有数据在处理时，不应该释放
        thread_lock m_server_lock;
        int m_nLockNum;

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        int set_http_listen();
        int set_http_data();
        int set_tcp_listen();
        int set_tcp_data();
        int set_udp();

        bool http_listen();
        bool http_data();
        bool tcp_listen();
        bool tcp_data();
        bool is_udp();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        int set_none();
        int set_connecting();
        int set_initing();
        int set_running();
        int set_shutdown();
        int set_need_close();
        int set_timeout();
        int set_data_err();
        int set_closing();
        int set_closed();

        bool none();
        bool connecting();
        bool initing();
        bool running();
        bool shutdown();
        bool need_close();
        bool timeout();
        bool data_err();
        bool closing();
        bool closed();

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        int post_close( int onoff, int linger );
        int post_shut_down( int how );
        bool ssl();

    protected:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        int timeout_check();
        int init_data();

    public:
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        int set_timeout_time_s( int nSecond );
        int helper_release();
        int un_lock_server();
        int lock_server();
		int release();
        int init();
        int term();
    };
} // cat

#endif // __DATA_STRUCT_CONNECTION_H__
