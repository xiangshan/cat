/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "request.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static request::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    request::request()
    {
        init_data();
    }

    request::~request()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int request::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int request::init_data()
    {
        int ret = 0;

        m_pUnCompressList = 0;
        m_pUnPackageList = 0;
        m_pUnEnctyptList = 0;
        m_pRecvList = 0;
        m_pParamProp = 0;
        m_pPathProp = 0;
        m_pTagProp = 0;
        m_pTask = 0;
        m_pProperty = 0;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int request::swap_http_request( request * pRequest )
    {
        int ret = 0;

        xos::i_list * pUnPackageList = pRequest->m_pUnPackageList;
        xos_common::i_property * pParam = pRequest->m_pParamProp;
        xos_common::i_property * pPath = pRequest->m_pPathProp;
        xos_common::i_property * pTag = pRequest->m_pTagProp;

        pRequest->m_pUnPackageList = m_pUnPackageList;
        pRequest->m_pParamProp = m_pParamProp;
        pRequest->m_pPathProp = m_pPathProp;
        pRequest->m_pTagProp = m_pTagProp;

        m_pUnPackageList = pUnPackageList;
        m_pParamProp = pParam;
        m_pPathProp = pPath;
        m_pTagProp = pTag;

        m_pProperty->set( "param", m_pParamProp, false );
        m_pProperty->set( "tag", m_pTagProp, false );

        return ret;
    }

    int request::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pUnCompressList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pUnPackageList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pUnEnctyptList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pRecvList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void ** )&m_pParamProp );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void ** )&m_pPathProp );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void ** )&m_pTagProp );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void ** )&m_pProperty );
        }

        if( 0 == ret )
        {
            m_pProperty->set( "param", m_pParamProp, false );
            m_pProperty->set( "tag", m_pTagProp, false );
        }

        return ret;
    }

    int request::term()
    {
        int ret = 0;

        xos_stl::release_interface( m_pUnCompressList );
        xos_stl::release_interface( m_pUnPackageList );
        xos_stl::release_interface( m_pUnEnctyptList );
        xos_stl::release_interface( m_pRecvList );
        xos_stl::release_interface( m_pParamProp );
        xos_stl::release_interface( m_pPathProp );
        xos_stl::release_interface( m_pTagProp );
        xos_stl::release_interface( m_pProperty );

        init_data();

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    // 
    // icat::i_request methods
    // 

    xos_common::i_property * request::get_param()
    {
        return m_pParamProp;
    }

    xos_common::i_property * request::get_path()
    {
        return m_pPathProp;
    }

    xos_common::i_property * request::get_tag()
    {
        return m_pTagProp;
    }

    icat::i_task * request::get_task()
    {
        return ( icat::i_task * )m_pTask;
    }

    xos::i_list * request::get_data()
    {
        return m_pUnCompressList;
    }

    xos_common::i_property * request::prop()
    {
        return m_pProperty;
    }

} // cat
