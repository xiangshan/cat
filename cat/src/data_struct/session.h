/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_SESSION_H__
#define __DATA_STRUCT_SESSION_H__

#include "../tools/head.h"

namespace cat
{
    class session : public xos_stl::mem_item< xos::com_object_no_ref< session >, thread_lock >, 
        public icat::i_session
    {
    public:
        typedef xos::com_object_no_ref< session > T;

    public:
        XOS_BEGIN_COM_MAP( session, icat::i_session )
            XOS_COM_INTERFACE_ENTRY( icat::i_session )
        XOS_END_COM_MAP()

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        session();
        ~session();

    protected:
        xos_common::i_property * m_pProperty;

    public:
        xos_common::i_property * prop();

    protected:
        int init_data();
        
    public:
        int init();
        int term();
        
    };

} // cat

#endif // __DATA_STRUCT_SESSION_H__
