/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "response.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static response::POOL * pool_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    response::response()
    {
        init_data();
    }

    response::~response()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // public method

    int response::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = 0;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int response::init_data()
    {
        int ret = 0;

        m_pCompressList = 0;
        m_pPackageList = 0;
        m_pEnctyptList = 0;
        m_pRawDataList = 0;
        m_pTagProp = 0;
        m_pTask = 0;
        m_pProperty = 0;

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int response::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pCompressList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pPackageList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pEnctyptList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void ** )&m_pRawDataList );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void ** )&m_pTagProp );
        }

        if( 0 == ret )
        {
            ret = mgr::xos()->common()->create( xos_common::OBJ_PROPERTY, ( void ** )&m_pProperty );
        }

        if( 0 == ret )
        {
            m_pProperty->set( "tag", m_pTagProp, false );
        }

        return ret;
    }

    int response::term()
    {
        int ret = 0;

        xos_stl::release_interface( m_pCompressList );
        xos_stl::release_interface( m_pPackageList );
        xos_stl::release_interface( m_pEnctyptList );
        xos_stl::release_interface( m_pRawDataList );
        xos_stl::release_interface( m_pTagProp );
        xos_stl::release_interface( m_pProperty );

        init_data();

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    // 
    // icat::i_response methods
    // 

    xos_common::i_property * response::get_tag()
    {
        return m_pTagProp;
    }

    icat::i_task * response::get_task()
    {
        return ( icat::i_task * )m_pTask;
    }

    xos::i_list * response::get_data()
    {
        return m_pRawDataList;
    }

    xos_common::i_property * response::prop()
    {
        return m_pProperty;
    }

} // cat
