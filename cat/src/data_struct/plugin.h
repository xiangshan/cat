/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_PLUGIN_H__
#define __DATA_STRUCT_PLUGIN_H__

#include "../tools/head.h"

namespace cat
{
    class module;
    class plugin : public xos_stl::mem_item< plugin, thread_lock >
    {
    public:
        typedef plugin T;

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        plugin();
        ~plugin();

    public:
        icat::i_plugin * m_pPlugin;
        std::string m_module;
        std::string m_class;
        int m_nPriority;
        bool m_bRelease;

    protected:
        int init_data();
        
    public:
        const plugin & operator=( const plugin & right );
        bool operator < ( const plugin & right ) const;
        const char * name();
        int priority();
        int release();
        int init();
        int term();
        
    };

} // cat

#endif // __DATA_STRUCT_PLUGIN_H__
