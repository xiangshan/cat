/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../config/config.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "../macro/head.h"
#include "../msg/head.h"
#include "connection.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	static connection::POOL * pool_ptr = 0;
    static connection::LIST * list_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    connection::connection()
    {
        init_data();
    }

    connection::~connection()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int connection::get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields )
    {
        int ret = 0;
        if( pppPool )
        {
            *pppPool = &pool_ptr;
        }
        if( pppGroup )
        {
            *pppGroup = 0;
        }
        if( pppList )
        {
            *pppList = &list_ptr;
        }
        if( pppObj )
        {
            *pppObj = 0;
        }
        if( pppFields )
        {
            *pppFields = 0;
        }
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int connection::heart()
    {
        int ret = 0;

        connection::LIST & s_list = *connection::list();

        for( connection::ITER i = s_list.begin(); ( 0 == ret ) && ( i != s_list.end() ); ++i )
        {
            connection * pConnect = *i;
            if( !pConnect->http_data() && !pConnect->tcp_data() && !pConnect->is_udp() )
            {
                continue;
            }
            if( !pConnect->running() && !pConnect->shutdown() )
            {
                continue;
            }
            if( pConnect->timeout() )
            {
                continue;
            }
            pConnect->timeout_check();
        }

        return ret;
    }

    int connection::timeout_check()
    {
        int ret = 0;

        if( 0 == ret )
        {
            net_timeout_ms += config::get()->heart_check_interval_ms;
        }

        if( ( 0 == ret ) && ( net_timeout_ms < config::get()->net_timeout_ms ) )
        {
            ret = 1;
        }

        // ��ʱ��ֱ��reset
        if( 0 == ret )
        {
            if( m_pTcp )
            {
                LOG4( "close timeout socket(%s:%d)", m_pTcp->m_peer_ip.c_str(), m_pTcp->m_nPeerPort );
            }
            if( m_pQuitAfterSend )
            {
                msg::notify_main_main( MAIN_QUITING, false );
            }
            else
            {
                post_close( 0, 0 );
            }
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int connection::init_data()
    {
        int ret = 0;

        m_tLastHeartTimestampS = 0;
        net_timeout_ms = 0;

        m_nState = STATE_NONE;
        m_eType = TYPE_NONE;

        m_pCloseAfterSend = 0;
        m_pQuitAfterSend = 0;

        m_str_key = DEFAULT_DES_PASSWORD;
        m_bSslHandShaked = false;
        m_pEncrypt = 0;

        m_connection_iter = connection::list()->end();
        m_pTcp = 0;
        m_pUdp = 0;

        m_pTask = 0;

        m_nLockNum = 0;

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int connection::set_timeout_time_s( int nSecond )
    {
        int ret = 0;
        net_timeout_ms = config::get()->net_timeout_ms - nSecond * config::get()->heart_check_interval_ms;
        return ret;
    }

    int connection::helper_release()
    {
        int ret = 0;

        if( m_pTcp )
        {
            LOG4( "released tcp = (%s:%d)(%s:%d) = %x", m_pTcp->m_local_ip.c_str(), m_pTcp->m_nLocalPort, m_pTcp->m_peer_ip.c_str(), m_pTcp->m_nPeerPort, this );
        }
        else
        {
            LOG4( "released udp = (%s:%d)", m_pUdp->m_ip.c_str(), m_pUdp->m_nPort );
        }

        {
            connection::list()->erase( m_connection_iter );
        }

        if( state::quitting() && ( 0 == connection::list()->size() ) )
        {
            msg::notify_main_main( MAIN_FLUSH, false );
        }
        else if( !state::quitting() && m_pQuitAfterSend )
        {
            msg::notify_main_main( MAIN_QUITING, false );
        }

        if( m_pTcp )
        {
            tcp::term( m_pTcp );
        }
        else
        {
            udp::term( m_pUdp );
        }

        return ret;
    }

    int connection::un_lock_server()
    {
        int ret = 0;
        xos_stl::auto_lock< thread_lock > lock( &m_server_lock );
        m_nLockNum--;
        if( 0 == m_nLockNum )
        {
            xos::i_msg * pMsg = mgr::xos()->msg();
            pMsg->set_void( 0, this );
            pMsg->set_int( 0, m_pTcp ? NET_TCP_NEED_RELEASE : NET_UDP_NEED_RELEASE );
            LOG4( "post connection release msg for through, = %x", this );
            msg::notify_net( pMsg, true );
        }
        return ret;
    }

    int connection::lock_server()
    {
        int ret = 0;
        xos_stl::auto_lock< thread_lock > lock( &m_server_lock );
        m_nLockNum++;
        return ret;
    }

	int connection::init()
    {
        int ret = 0;

        if( ( 0 == ret ) && ssl() )
        {
            ret = mgr::xos()->encrypt()->create( true, &m_pEncrypt );
        }

        if( 0 == ret )
        {
            m_pTask = task::get_item_from_pool( true );
            m_pTask->init();
            m_pTask->m_task_iter = m_task_list.end();
            m_pTask->m_pConnection = this;
        }

        return ret;
    }

    int connection::term()
    {
        int ret = 0;

        xos_stl::release_interface( m_pEncrypt );
        xos_stl::release_interface( m_pTask );
        m_task_list.put_back_to_pool( true );

        init_data();

        return ret;
    }

    int connection::release()
    {
        int ret = 0;

		term();
		put_back_to_pool( this, true );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int connection::set_http_listen()
    {
        int ret = 0;
        m_eType = TYPE_HTTP_LISTEN;
        return ret;
    }

    int connection::set_http_data()
    {
        int ret = 0;
        m_eType = TYPE_HTTP_DATA;
        return ret;
    }

    int connection::set_tcp_listen()
    {
        int ret = 0;
        m_eType = TYPE_TCP_LISTEN;
        return ret;
    }

    int connection::set_tcp_data()
    {
        int ret = 0;
        m_eType = TYPE_TCP_DATA;
        return ret;
    }

    int connection::set_udp()
    {
        int ret = 0;
        m_eType = TYPE_UDP;
        return ret;
    }

    bool connection::http_listen()
    {
        return TYPE_HTTP_LISTEN == m_eType;
    }

    bool connection::http_data()
    {
        return TYPE_HTTP_DATA == m_eType;
    }

    bool connection::tcp_listen()
    {
        return TYPE_TCP_LISTEN == m_eType;
    }

    bool connection::tcp_data()
    {
        return TYPE_TCP_DATA == m_eType;
    }

    bool connection::is_udp()
    {
        return TYPE_UDP == m_eType;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int connection::set_none()
    {
        int ret = 0;
        m_nState = STATE_NONE;
        return ret;
    }

    int connection::set_connecting()
    {
        int ret = 0;
        m_nState = STATE_CONNECTING;
        return ret;
    }

    int connection::set_initing()
    {
        int ret = 0;
        m_nState = STATE_INITING;
        return ret;
    }

    int connection::set_running()
    {
        int ret = 0;
        m_nState = STATE_RUNNING;
        return ret;
    }

    int connection::set_shutdown()
    {
        int ret = 0;
        m_nState = STATE_SHUTDOWN;
        return ret;
    }

    int connection::set_need_close()
    {
        int ret = 0;
        m_nState = STATE_NEED_CLOSE;
        return ret;
    }

    int connection::set_timeout()
    {
        int ret = 0;
        m_nState = STATE_TIME_OUT;
        return ret;
    }

    int connection::set_closing()
    {
        int ret = 0;
        m_nState = STATE_CLOSING;
        return ret;
    }

    int connection::set_data_err()
    {
        int ret = 0;
        m_nState = STATE_DATA_ERR;
        return ret;
    }

    int connection::set_closed()
    {
        int ret = 0;
        m_nState = STATE_CLOSED;
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    bool connection::none()
    {
        return STATE_NONE == m_nState;
    }

    bool connection::connecting()
    {
        return STATE_CONNECTING == m_nState;
    }

    bool connection::initing()
    {
        return STATE_INITING == m_nState;
    }

    bool connection::running()
    {
        return STATE_RUNNING == m_nState;
    }

    bool connection::shutdown()
    {
        return STATE_SHUTDOWN == m_nState;
    }

    bool connection::need_close()
    {
        return STATE_NEED_CLOSE == m_nState;
    }

    bool connection::timeout()
    {
        return STATE_TIME_OUT == m_nState;
    }

    bool connection::data_err()
    {
        return STATE_DATA_ERR == m_nState;
    }

    bool connection::closing()
    {
        return STATE_CLOSING == ( m_nState & STATE_CLOSING );
    }

    bool connection::closed()
    {
        return STATE_CLOSED == m_nState;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	int connection::post_close( int onoff, int linger )
	{
		int ret = 0;

		if( ( 0 == ret ) && m_pTcp )
		{
            aio_tcp obj;
			obj.post_close( this, onoff, linger );
            ret = 1;
		}

		if( ( 0 == ret ) && m_pUdp )
		{
            aio_udp obj;
            obj.post_close( this, onoff, linger );
            ret = 1;
		}

		return ret;
	}

    int connection::post_shut_down( int how )
    {
        int ret = 0;

        if( ( 0 == ret ) && m_pTcp )
        {
            aio_tcp obj;
            obj.post_shut_down( this, how );
            ret = 1;
        }

        if( ( 0 == ret ) && m_pUdp )
        {
            aio_udp obj;
            obj.post_shut_down( this, how );
            ret = 1;
        }

        return ret;
    }

    bool connection::ssl()
    {
        return 0 != config::get()->use_ssl;
    }

} // cat
