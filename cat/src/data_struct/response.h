/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DATA_STRUCT_RESPONSE_H__
#define __DATA_STRUCT_RESPONSE_H__

#include "../tools/head.h"

namespace cat
{
    class task;

    class response : public xos_stl::mem_item< xos::com_object_no_ref< response >, thread_lock >, 
        public icat::i_response
    {
    public:
        typedef xos::com_object_no_ref< response > T;

    public:
        XOS_BEGIN_COM_MAP( response, icat::i_response )
            XOS_COM_INTERFACE_ENTRY( icat::i_response )
        XOS_END_COM_MAP()

    public:
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, xos_odbc::i_fields *** pppFields );

    public:
        response();
        ~response();

    public:
        xos_common::i_property * m_pTagProp;
        xos::i_list * m_pCompressList;
        xos::i_list * m_pPackageList;
        xos::i_list * m_pEnctyptList;
        xos::i_list * m_pRawDataList;

        task * m_pTask;

    protected:
        xos_common::i_property * m_pProperty;

    protected:
        int init_data();
        
    public:
        int init();
        int term();

        // 
        // icat::i_response methods
        // 
    public:
        xos_common::i_property * get_tag();
        icat::i_task * get_task();
        xos::i_list * get_data();

    public:
        xos_common::i_property * prop();
        
    };

} // cat

#endif // __DATA_STRUCT_RESPONSE_H__
