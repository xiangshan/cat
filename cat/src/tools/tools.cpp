/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "../settings/head.h"
#include "tools.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

	namespace tools
	{
		int full_config_path_file( char * lpszFullFile, int nBufSize, const char * lpszFile )
		{
			int ret = 0;
			mgr::xos()->crt()->strcpy( lpszFullFile, nBufSize, mgr::xos()->exe_path() );
			mgr::xos()->misc()->path_append( lpszFullFile, settings::get()->config_path.c_str() );
			mgr::xos()->misc()->path_append( lpszFullFile, lpszFile );
			return ret;
		}

		int full_path( char * lpszFullFile, int nBufSize, const char * lpszFile )
		{
			int ret = 0;
			mgr::xos()->crt()->strcpy( lpszFullFile, nBufSize, mgr::xos()->exe_path() );
			mgr::xos()->misc()->path_append( lpszFullFile, lpszFile );
			return ret;
		}
	}

} // cat
