/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_MSG_H__
#define __MSG_MSG_H__

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 线程类型定义
    // 
    enum enumThreadType
    {
        THREAD_TYPE_PACKET,
        THREAD_TYPE_WORKER,
        THREAD_TYPE_MAIN,
        THREAD_TYPE_NET
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 消息类型定义
    // 
    enum enumMsgType
    {
        MSG_TYPE_BEGIN,

        // 程序产生的消息类型
        MSG_TYPE_PACKET,
        MSG_TYPE_PROC,
        MSG_TYPE_MAIN,
        MSG_TYPE_NET,

        // 外部模块消息类型
        MSG_TYPE_AIO,

        MSG_TYPE_END
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 消息定义 : MSG_TYPE_PACKET
    // 
    enum enumPacketMsg
    {
        PACKET_BEGIN,

        // http
        PACKET_HTTP_UN_COMPRESS_FLUSH,
        PACKET_HTTP_UN_COMPRESS,
        PACKET_HTTP_COMPRESS,

        PACKET_HTTP_UN_PACKAGE_FLUSH,
        PACKET_HTTP_UN_PACKAGE,
        PACKET_HTTP_PACKAGE,

        PACKET_HTTP_UN_ENCRYPT_FLUSH,
        PACKET_HTTP_UN_ENCRYPT,
        PACKET_HTTP_ENCRYPT,

        // tcp
        PACKET_TCP_UN_COMPRESS_FLUSH,
        PACKET_TCP_UN_COMPRESS,
        PACKET_TCP_COMPRESS,

        PACKET_TCP_UN_PACKAGE_FLUSH,
        PACKET_TCP_UN_PACKAGE,
        PACKET_TCP_PACKAGE,

        PACKET_TCP_UN_ENCRYPT_FLUSH,
        PACKET_TCP_UN_ENCRYPT,
        PACKET_TCP_ENCRYPT,

        // udp
        PACKET_UDP_UN_COMPRESS_FLUSH,
        PACKET_UDP_UN_COMPRESS,
        PACKET_UDP_COMPRESS,

        PACKET_UDP_UN_PACKAGE_FLUSH,
        PACKET_UDP_UN_PACKAGE,
        PACKET_UDP_PACKAGE,

        PACKET_UDP_UN_ENCRYPT_FLUSH,
        PACKET_UDP_UN_ENCRYPT,
        PACKET_UDP_ENCRYPT,

        PACKET_END
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 消息定义 : MSG_TYPE_PROC
    // 
    enum enumProcMsg
    {
        PROC_BEGIN,

        PROC_PLUGIN,
        PROC_HTTP,
        PROC_TCP,
        PROC_UDP,

        PROC_END
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 消息定义 : MSG_TYPE_MAIN
    // 
    enum enumMainMsg
    {
        MAIN_BEGIN,

        MAIN_START_HTTP_SERVER,
        MAIN_PASS_THROUGH,

        MAIN_STARTING,
        MAIN_STARTED,
        MAIN_QUITING,
        MAIN_QUITED,
        MAIN_FLUSH,

        MAIN_HEART,

        MAIN_END
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    // 
    // 消息定义 : MSG_TYPE_NET
    // 
    enum enumNetMsg
    {
        NET_BEGIN,

        NET_TCP_NEED_RELEASE,
        NET_TCP_NEED_CLOSE,
        NET_TCP_DATA_ERR,
        NET_TCP_INIT,
        NET_TCP_ACCEPT,
        NET_TCP_CONNECT,
        NET_TCP_RECV,
        NET_TCP_SEND,
        NET_TCP_SHUTDOWN,
        NET_TCP_CLOSE,

        NET_UDP_NEED_RELEASE,
        NET_UDP_NEED_CLOSE,
        NET_UDP_DATA_ERR,
        NET_UDP_INIT,
        NET_UDP_RECV,
        NET_UDP_SEND,
        NET_UDP_SHUTDOWN,
        NET_UDP_CLOSE,

        NET_QUITTING,
        NET_HEART,

        NET_END
    };
}

#endif // __MSG_MSG_H__
