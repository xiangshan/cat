/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_WEBAPP_APP_LOAD_PLUGIN__
#define __HELPER_WEBAPP_APP_LOAD_PLUGIN__

#include "../data_struct/module.h"
#include "../data_struct/plugin.h"
#include "../tools/head.h"

namespace cat
{
    class webapp;
    class step;
    class app_plugin
    {
    public:
        app_plugin( webapp * pWebApp );
        ~app_plugin();

    protected:
        int make_chain( icat::i_task * pTask, icat::i_plugin * pProcess, const char * lpszModule, const char * lpszAction, const char * lpszMethod );

        icat::i_plugin::enumRetCode download_proc( icat::i_task * pTask );
        icat::i_plugin::enumRetCode jsp_proc( icat::i_task * pTask );

        icat::i_plugin::enumRetCode normal_proc( icat::i_task * pTask );
        icat::i_plugin::enumRetCode index_proc( icat::i_task * pTask );

        icat::i_plugin::enumRetCode first_proc( icat::i_task * pTask );

        int try_split_action( char * lpszFullAction, const char *& lpszAction, const char *& lpszMethod );

        int create_plugin( module::LIST & mgr_list, plugin::LIST & plugin_list );
        int helper_load( const char * lpszDir, module::LIST & mlist );

        int make_full_file( char * lpszFullFile, int nLen, icat::i_task * pTask );
        int index_file( char * lpszFullFile );
        int download_file( const char * lpszFullFile, icat::i_task * pTask );
        int allow_download_test( icat::i_task * pTask );

        module * get_jsp_module( icat::i_task * pTask );
        int make_full_jsp_file( icat::i_task * pTask );

    public:
        icat::i_plugin::enumRetCode call( icat::i_task * pTask, step * pStep );
        icat::i_plugin::enumRetCode proc( icat::i_task * pTask );
        int load( const char * lpszPath );

    private:
        webapp * m_pWebApp;
        
    };

} // cat

#endif // __HELPER_WEBAPP_APP_LOAD_PLUGIN__
