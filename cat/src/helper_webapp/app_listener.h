/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_WEBAPP_APP_LISTENER__
#define __HELPER_WEBAPP_APP_LISTENER__

#include "../tools/head.h"

namespace cat
{
    class webapp;
    class app_listener
    {
    public:
        app_listener( webapp * pWebApp );
        ~app_listener();

    public:
        int load_event( const char * lpszListenType, const char * lpszEventType, xos_common::i_property * pEvent, icat::i_listener * pListener );
        int load_listen( const char * lpszListenType, xos_common::i_property * pListen, icat::i_listener * pListener );
        int load_listeners( xos_common::i_property * pListeners, icat::i_listener * pListener );

    public:
        icat::i_plugin::enumRetCode call_listeners( icat::i_plugin::enumRetCode result, 
            icat::i_task * pTask, 
            const char * lpszType, 
            const char * lpszEvent, 
            const char * lpszAdvise );

    private:
        webapp * m_pWebApp;
                
    };

} // cat

#endif // __HELPER_WEBAPP_APP_LISTENER__
