/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_WEBAPP_HELPER_WEBAPP_H__
#define __HELPER_WEBAPP_HELPER_WEBAPP_H__

namespace cat
{
    class webapp;

    class helper_webapp
    {
    public:
        helper_webapp();
        ~helper_webapp();

    protected:
        static int scan_webapp_path();
        static int load_webapp_xml();
        static int create_entrance();
        static int scan_webapps();

    public:
        static int get_ports( int * pPortArray, int nNum, int * pnRetNum );
        static webapp * find( const char * lpszHost );
        static int init();
        static int term();

    };

} // cat

#endif // __HELPER_WEBAPP_HELPER_WEBAPP_H__
