/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/container_impl.h"
#include "../data_struct/webapp.h"
#include "../data_struct/chain.h"
#include "app_listener.h"
#include "app_plugin.h"

namespace cat
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    app_plugin::app_plugin( webapp * pWebApp ) : m_pWebApp( pWebApp )
    {
    }

    app_plugin::~app_plugin()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int app_plugin::make_chain( icat::i_task * pTask, icat::i_plugin * pProcess, const char * lpszModule, const char * lpszAction, const char * lpszMethod )
    {
        int ret = 0;

        chain * pChain = 0;

        if( 0 == ret )
        {
            pChain = chain::get_item_from_pool( true );
            pChain->init();
        }

        for( plugin::ITER iter = m_pWebApp->m_filter_plugin_list.begin(); ( 0 == ret ) && ( iter != m_pWebApp->m_filter_plugin_list.end() ); ++iter )
        {
            plugin * pP = plugin::get_item_from_pool( true );
            pP->init();
            plugin * p = *iter;
            *pP = *p;
            pP->m_bRelease = false;
            xos::i_unknown * pUnk = 0;
            pP->m_pPlugin->query_interface( xos::i_unknown_id, ( void** )&pUnk );
            step * pS = step::get_item_from_pool( true );
            pS->init();
            pS->m_action = pUnk->class_name();
            pS->m_pChain = pChain;
            pS->m_pPlugin = pP;
            pChain->m_step_list.push_back( pS );
            pS->m_iter = pChain->m_step_list.end();
            pS->m_iter--;
        }

        if( ( 0 == ret ) && pProcess )
        {
            plugin * pP = plugin::get_item_from_pool( true );
            pP->init();
            pP->m_module = lpszModule;
            pP->m_pPlugin = pProcess;
            pP->m_bRelease = true;
            step * pS = step::get_item_from_pool( true );
            pS->init();
            pS->m_action = lpszAction ? lpszAction : "";
            pS->m_method = lpszMethod ? lpszMethod : "";
            pS->m_pChain = pChain;
            pS->m_pPlugin = pP;
            pChain->m_step_list.push_back( pS );
            pS->m_iter = pChain->m_step_list.end();
            pS->m_iter--;
        }

        if( ( 0 == ret ) && !pProcess )
        {
            step * pS = step::get_item_from_pool( true );
            pS->init();
            pS->m_pChain = pChain;
            pChain->m_step_list.push_back( pS );
            pS->m_iter = pChain->m_step_list.end();
            pS->m_iter--;
        }

        if( 0 == ret )
        {
            pChain->m_step_iter = pChain->m_step_list.begin();
            pChain->m_pImpl = m_pWebApp;
            pTask->set_chain( pChain );
            pChain->m_pTask = pTask;
            pChain = 0;
        }

        return ret;
    }

    icat::i_plugin::enumRetCode app_plugin::first_proc( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        int ret = 0;

        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pPath = pRequest->get_path();
        xos::i_crt * pCrt = container_impl::get()->xos()->crt();
        const char * lpszModule = pPath->str( 0 );
        const char * lpszAction = 0;
        const char * lpszMethod = 0;
        char full_action[512];
        icat::i_plugin * pProcess = 0;
        module * pModule = 0;

        if( 0 == ret )
        {
            int nPathNum = 0;
            pPath->size( 0, &nPathNum );
            if( 2 != nPathNum )
            {
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            const char * pAction = pPath->str( 1 );
            pCrt->strcpy( full_action, sizeof( full_action ), pAction );
        }

        if( 0 == ret )
        {
            ret = try_split_action( full_action, lpszAction, lpszMethod );
        }

        if( 0 == ret )
        {
            pModule = m_pWebApp->find_action_module( lpszModule );
            if( !pModule )
            {
                ret = 1;
            }
        }

        if( pModule )
        {
            pModule->m_pPluginMgr->create( lpszAction, &pProcess );
        }

        {
            make_chain( pTask, pProcess, lpszModule, lpszAction, lpszMethod );
        }
        {
            using namespace icat::listener::process;
            app_listener obj( m_pWebApp );
            result = obj.call_listeners( result, pTask, type, recv, after );
        }
        {
            result = pTask->get_chain()->run( result );
            pProcess = 0;
        }

        xos_stl::release_interface( pProcess );

        return result;
    }

    int app_plugin::try_split_action( char * lpszFullAction, const char *& lpszAction, const char *& lpszMethod )
    {
        int ret = 0;

        xos::i_crt * pCrt = container_impl::get()->xos()->crt();

        // 以.action结尾才是action
        if( ( 0 == ret ) && (0 != container_impl::get()->xos()->common_misc()->end_with( lpszFullAction, ".action" ) ) )
        {
            ret = 1;
        }

        // http://www.cat.com/module/action!method.action
        // 拆分action和action方法
        if( 0 == ret )
        {
            int len = pCrt->strlen( lpszFullAction );
            lpszFullAction[len-7] = 0;
            char * pMethod = pCrt->strchr( lpszFullAction, '!' );
            if( pMethod )
            {
                *pMethod++ = 0;
            }
            lpszAction = lpszFullAction;
            lpszMethod = pMethod;
        }

        return ret;
    }

    icat::i_plugin::enumRetCode app_plugin::call( icat::i_task * pTask, step * pStep )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        chain * pChain = ( chain* )pTask->get_chain();

        pChain->m_pCurStep = pStep;

        if( pStep->m_pPlugin )
        {
            result = pStep->m_pPlugin->m_pPlugin->proc( pTask );
        }
        else
        {
            result = normal_proc( pTask );
        }

        pChain->m_pCurStep = 0;

        return result;
    }

    icat::i_plugin::enumRetCode app_plugin::normal_proc( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        int ret = 0;

        chain * pChain = ( chain* )pTask->get_chain();
        step * pS = pChain->m_pCurStep;
        icat::i_request * pR = pTask->get_request();
        xos_common::i_property * pPath = pR->get_path();
        int nPathNum = 0;

        if( ( 0 == ret ) && ( pS->m_jsp_file.length() > 0 ) )
        {
            result = jsp_proc( pTask );
            ret = 1;
        }

        if( 0 == ret )
        {
            ret = pPath->size( 0, &nPathNum );
        }

        if( ( 0 == ret ) && ( 0 == nPathNum ) )
        {
            result = index_proc( pTask );
            ret = 1;
        }

        if( ( 0 == ret ) && ( 0 == container_impl::get()->xos()->common_misc()->end_with( pPath->str( nPathNum - 1 ), ".jsp" ) ) )
        {
            result = jsp_proc( pTask );
            ret = 1;
        }

        if( 0 == ret )
        {
            result = download_proc( pTask );
            ret = 1;
        }

        return result;
    }

    icat::i_plugin::enumRetCode app_plugin::download_proc( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;

        char file[4096];
        int ret = 0;

        if( 0 == ret )
        {
            ret = make_full_file( file, sizeof( file ), pTask );
        }

        if( 0 == ret )
        {
            download_file( file, pTask );
        }

        return result;
    }

    int app_plugin::make_full_jsp_file( icat::i_task * pTask )
    {
        xos::i_misc * pMisc = container_impl::get()->xos()->misc();
        xos::i_crt * pCrt = container_impl::get()->xos()->crt();
        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pPath = pRequest->get_path();
        chain * pChain = ( chain* )pTask->get_chain();
        step * pS = pChain->m_pCurStep;
        const char * lpszPath = 0;
        char file[4096];
        int nPathIndex = 0;
        int result = 0;
        int ret = 0;

        if( 0 == ret )
        {
            file[0] = 0;
        }

        if( ( 0 == ret ) && ( pS->m_jsp_file.length() > 0 ) )
        {
            ret = 1;
        }

        while( ( 0 == ret ) && ( lpszPath = pPath->str( nPathIndex++ ) ) )
        {
            pMisc->path_append( file, lpszPath );
        }

        if( 0 == ret )
        {
            pS->m_jsp_file = file;
            ret = 1;
        }

        pCrt->strcpy( file, sizeof( file ), pTask->base_path() );
        pMisc->path_append( file, "jsp" );
        pMisc->path_append( file, pS->m_jsp_file.c_str() );
        pS->m_jsp_file = file;

        return result;
    }

    icat::i_plugin::enumRetCode app_plugin::jsp_proc( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        icat::i_response * pResponse = pTask->get_response();
        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pResTag = pResponse->get_tag();
        xos_common::i_property * pReqTag = pRequest->get_tag();
        chain * pChain = ( chain* )pTask->get_chain();
        step * pS = pChain->m_pCurStep;
        icat::i_plugin * pPlugin = 0;
        module * pModule = 0;
        int ret = 0;

        if( 0 == ret )
        {
            ret = make_full_jsp_file( pTask );
        }

        if( 0 == ret )
        {
            pModule = get_jsp_module( pTask );
            if( !pModule )
            {
                ret = 1;
            }
        }

        if( 0 != ret )
        {
            pResTag->set( xos_http::HTTP_REQUEST_RET_CODE, xos_http::HTTP_RET_CODE_404 );
            char info[4096];
            container_impl::get()->xos()->crt()->sprintf( info, sizeof( info ), "not find file, url = %s", pReqTag->str( xos_http::HTTP_REQUEST_URI ) );
            char buf[4096];
            container_impl::get()->xos()->http()->fmt_simple_html( buf, sizeof( buf ), 0, "not find", info );
            container_impl::get()->add_ret_str( pTask, buf );
        }

        if( 0 == ret )
        {
            ret = pModule->m_pPluginMgr->create( "jsp", &pPlugin );
        }

        if( 0 == ret )
        {
            plugin * pP = plugin::get_item_from_pool( true );
            pP->init();
            pP->m_pPlugin = pPlugin;
            pP->m_bRelease = true;
            pS->m_action = "jsp";
            pS->m_pPlugin = pP;
        }

        if( 0 == ret )
        {
            result = pPlugin->proc( pTask );
        }

        return result;
    }

    icat::i_plugin::enumRetCode app_plugin::index_proc( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;

        char file[4096];
        int ret = 0;

        if( 0 == ret )
        {
            file[0] = 0;
            ret = make_full_file( file, sizeof( file ), pTask );
        }

        if( 0 == ret )
        {
            download_file( file, pTask );
        }

        return result;
    }

    int app_plugin::make_full_file( char * lpszFullFile, int nLen, icat::i_task * pTask )
    {
        int result = 0;
        int ret = 0;

        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pPath = pRequest->get_path();
        xos::i_misc * pMisc = container_impl::get()->xos()->misc();
        xos::i_crt * pCrt = container_impl::get()->xos()->crt();
        const char * lpszPath = 0;
        int nPathIndex = 0;
        int nPathNum = 0;

        if( 0 == ret )
        {
            pCrt->strcpy( lpszFullFile, nLen, m_pWebApp->m_webapp_path.c_str() );
        }

        if( ( 0 == ret ) && ( 0 == pPath->size( 0, &nPathNum ) ) && ( 0 == nPathNum ) )
        {
            result = index_file( lpszFullFile );
            ret = 1;
        }

        while( ( 0 == ret ) && ( lpszPath = pPath->str( nPathIndex++ ) ) )
        {
            pMisc->path_append( lpszFullFile, lpszPath );
        }

        return result;
    }

    int app_plugin::index_file( char * lpszFullFile )
    {
        int ret = 0;

        xos::i_list * pList = m_pWebApp->m_pConfigProperty->list( "welcome" );
        xos::i_misc * pMisc = container_impl::get()->xos()->misc();
        xos::i_crt * pCrt = container_impl::get()->xos()->crt();
        xos_common::i_variant * pVT = 0;
        xos::i_dir * pDir = 0;
        char file[4096];

        if( 0 == ret )
        {
            ret = container_impl::get()->xos()->xos()->create( xos::i_xos::XOS_OBJ_DIR, ( void** )&pDir );
        }

        if( 0 == ret )
        {
            if( pList )
            {
                pList->reset();
            }
            else
            {
                ret = 1;
            }
        }

        while( ( 0 == ret ) && ( pVT = ( xos_common::i_variant* )pList->data() ) )
        {
            const char * lpszFile = pVT->str();
            pCrt->strcpy( file, sizeof( file ), lpszFullFile );
            pMisc->path_append( file, lpszFile );
            if( 0 == pDir->is_file_exist( file ) )
            {
                pMisc->path_append( lpszFullFile, lpszFile );
                break;
            }
            pList->next();
        }

        xos_stl::release_interface( pDir );

        return ret;
    }

    int app_plugin::download_file( const char * lpszFullFile, icat::i_task * pTask )
    {
        int ret = 0;

        icat::i_response * pResponse = pTask->get_response();
        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pResTag = pResponse->get_tag();
        xos_common::i_property * pReqTag = pRequest->get_tag();
        xos::i_file * pFile = 0;
        bool bFindGzip = false;
        const char * lpszRetFile = lpszFullFile;
        char full_name[4096];
        int nReadLen = 1;

        if( ( 0 == ret ) && ( 0 != allow_download_test( pTask ) ) )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            const char * lpszEncoding = pReqTag->str( xos_http::HTTP_ACCEPT_ENCODING );
            if( lpszEncoding && container_impl::get()->xos()->crt()->strstr( lpszEncoding, xos_http::HTTP_ZIP_GZIP, true ) )
            {
                int r = container_impl::get()->xos()->http()->get_gz_file_if_awailable( full_name, sizeof( full_name ), lpszFullFile );
                if( 0 == r )
                {
                    lpszRetFile = full_name;
                    bFindGzip = true;
                }
            }
        }

        if( 0 == ret )
        {
            xos::i_dir * pDir = 0;
            container_impl::get()->xos()->xos()->create( xos::i_xos::XOS_OBJ_DIR, ( void** )&pDir );
            ret = pDir->is_file_exist( lpszFullFile );
            xos_stl::release_interface( pDir );
        }

        if( 0 == ret )
        {
            container_impl::get()->xos()->xos()->create( xos::i_xos::XOS_OBJ_FILE, ( void** )&pFile );
            ret = pFile->open( lpszRetFile, xos::i_file::FILE_READ, xos::i_file::SHARE_READ, xos::i_file::FILE_OPEN );
        }

        while( ( 0 == ret ) && ( nReadLen > 0 ) )
        {
            xos::i_buf * pBuf = container_impl::get()->xos()->buf();
            char * lpszBuf = pBuf->get_data( 0, 0, 0 );
            int nSize = xos::i_buf::BUF_SIZE;
            nReadLen = pFile->read( lpszBuf, nSize );
            if( nReadLen > 0 )
            {
                pBuf->set_len( nReadLen );
                container_impl::get()->add_ret_data( pTask, pBuf );
            }
            xos_stl::release_interface( pBuf );
        }

        if( 0 != ret )
        {
            pResTag->set( xos_http::HTTP_REQUEST_RET_CODE, xos_http::HTTP_RET_CODE_404 );
            char info[4096];
            container_impl::get()->xos()->crt()->sprintf( info, sizeof( info ), "not find file, url = %s", pReqTag->str( xos_http::HTTP_REQUEST_URI ) );
            char buf[4096];
            container_impl::get()->xos()->http()->fmt_simple_html( buf, sizeof( buf ), 0, "not find", info );
            container_impl::get()->add_ret_str( pTask, buf );
        }

        if( 0 == ret )
        {
            const char * lpszType = container_impl::get()->xos()->http()->content_type( lpszFullFile );
            if( lpszType )
            {
                pResTag->set( xos_http::HTTP_CONTENT_TYPE, lpszType );
            }
        }

        if( ( 0 == ret ) && bFindGzip )
        {
            pResTag->set( xos_http::HTTP_CONTENT_ENCODING, xos_http::HTTP_ZIP_GZIP );
        }

        xos_stl::release_interface( pFile );

        return ret;
    }

    int app_plugin::allow_download_test( icat::i_task * pTask )
    {
        int result = 1;
        int ret = 0;

        xos::i_list * pAllowDownloadList = m_pWebApp->m_pConfigProperty->list( "allow_download_dirs" );
        xos::i_list * pWelcomeList = m_pWebApp->m_pConfigProperty->list( "welcome" );
        xos::i_misc * pMisc = container_impl::get()->xos()->misc();
        xos::i_crt * pCrt = container_impl::get()->xos()->crt();
        xos_common::i_variant * pVT = 0;

        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pPath = pRequest->get_path();
        const char * lpszPath = 0;
        int nIndex = 0;

        if( ( 0 == ret ) && !( lpszPath = pPath->str( ( int )0 ) ) )
        {
            result = 0;
            ret = 1;
        }

        for( pAllowDownloadList->reset(); ( 0 == ret ) && ( pVT = ( xos_common::i_variant* )pAllowDownloadList->data() ); pAllowDownloadList->next() )
        {
            const char * lpszStr = pVT->str();
            if( 0 == pCrt->strcmp( lpszPath, lpszStr ) )
            {
                result = 0;
                ret = 1;
            }
        }

        for( pWelcomeList->reset(); ( 0 == ret ) && ( pVT = ( xos_common::i_variant* )pWelcomeList->data() ); pWelcomeList->next() )
        {
            const char * lpszStr = pVT->str();
            if( 0 == pCrt->strcmp( lpszPath, lpszStr ) )
            {
                result = 0;
                ret = 1;
            }
        }

        return result;
    }

    module * app_plugin::get_jsp_module( icat::i_task * pTask )
    {
        int ret = 0;

        xos_common::i_misc * pCmnMisc = container_impl::get()->xos()->common_misc();
        xos::i_crt * pCrt = container_impl::get()->xos()->crt();
        chain * pChain = ( chain* )pTask->get_chain();
        step * pS = pChain->m_pCurStep;
        char jsp_path[4096];
        char jsp_file[1024];
        icat::i_plugin_mgr * pPluginMgr = 0;
        module * pModule = 0;

        if( 0 == ret )
        {
            jsp_file[0] = 0;
            jsp_path[0] = 0;
            const char * lpszFile = pCmnMisc->split_full_file( pS->m_jsp_file.c_str(), jsp_path, sizeof( jsp_path ), 0 );
            pCrt->strcpy( jsp_file, sizeof( jsp_file ), lpszFile );
            int len = pCrt->strlen( jsp_file );
            jsp_file[len-4] = 0;
        }

        // lock
        m_pWebApp->m_pJspMgrMap->lock();

        if( 0 == ret )
        {
            pModule = m_pWebApp->find_jsp_module( pS->m_jsp_file.c_str() );
        }

        if( ( 0 == ret ) && pModule )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            ret = container_impl::get()->load_plugin( jsp_path, 0, jsp_file, jsp_file, &pPluginMgr );
        }

        if( 0 == ret )
        {
            pModule = module::get_item_from_pool( true );
            pModule->init();
            pModule->m_pPluginMgr = pPluginMgr;
            ( *m_pWebApp->m_pJspMgrMap )[pS->m_jsp_file.c_str()] = pModule;
            pPluginMgr = 0;
        }

        // un lock
        m_pWebApp->m_pJspMgrMap->un_lock();

        return pModule;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode app_plugin::proc( icat::i_task * pTask )
    {
        using namespace icat;

        i_plugin::enumRetCode result = i_plugin::RET_CODE_DONE;
        i_chain * pChain = pTask->get_chain();
        app_listener obj( m_pWebApp );

        if( pChain )
        {
            using namespace listener::process;
            result = obj.call_listeners( result, pTask, type, again, before );
            result = pChain->run( result );
        }
        else
        {
            result = first_proc( pTask );
        }

        if( i_plugin::RET_CODE_ASYNC != result )
        {
            using namespace listener::process;
            obj.call_listeners( result, pTask, type, done, after );
            pChain = pTask->get_chain();
            xos_stl::release_interface( pChain );
            pTask->set_chain( 0 );
        }

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int app_plugin::load( const char * lpszPath )
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = helper_load( "listener", m_pWebApp->m_listener_mgr_list );
        }

        if( 0 == ret )
        {
            ret = helper_load( "filter", m_pWebApp->m_filter_mgr_list );
        }

        if( 0 == ret )
        {
            ret = helper_load( "action", m_pWebApp->m_action_mgr_list );
        }

        for( module::ITER iter = m_pWebApp->m_action_mgr_list.begin(); ( 0 == ret ) && ( iter != m_pWebApp->m_action_mgr_list.end() ); ++iter )
        {
            module * pM = *iter;
            const char * lpszName = pM->m_pPluginMgr->module_name();
            ( *m_pWebApp->m_pActionMgrMap )[lpszName] = pM;
        }

        if( 0 == ret )
        {
            ret = create_plugin( m_pWebApp->m_listener_mgr_list, m_pWebApp->m_listener_plugin_list );
        }

        if( 0 == ret )
        {
            ret = create_plugin( m_pWebApp->m_filter_mgr_list, m_pWebApp->m_filter_plugin_list );
        }

        if( 0 == ret )
        {
            m_pWebApp->m_listener_plugin_list.sort( xos_stl::p_less< plugin > );
            m_pWebApp->m_filter_plugin_list.sort( xos_stl::p_less< plugin > );
        }

        for( plugin::ITER iter = m_pWebApp->m_listener_plugin_list.begin(); ( 0 == ret ) && ( iter != m_pWebApp->m_listener_plugin_list.end() ); ++iter )
        {
            plugin * pP = *iter;
            icat::i_listener * pL = 0;
            pP->m_pPlugin->query_interface( icat::i_listener_id, ( void** )&pL );
            xos_common::i_property * pListeners = pL->listeners();
            app_listener obj( m_pWebApp );
            obj.load_listeners( pListeners, pL );
        }

        return ret;
    }

    int app_plugin::create_plugin( module::LIST & mgr_list, plugin::LIST & plugin_list )
    {
        int ret = 0;

        for( module::ITER iter = mgr_list.begin(); iter != mgr_list.end(); ++iter )
        {
            module * pM = *iter;
            icat::i_plugin_mgr * pMgr = pM->m_pPluginMgr;
            icat::i_plugin * pTempPlugin = 0;
            // 让创建类函数运行至少一次，这样就可以收集到所有可创建类的名称
            // 这步并不真的创建类，所以弄了个guid做类名来创建类
            pMgr->create( "B590FD67_E1D1_4193_91DB_AC2111CF4B79", &pTempPlugin );
            int nIndex = 0;
            while( 1 )
            {
                const char * lpszClassName = pMgr->query_creatable_class_name( nIndex++ );
                if( !lpszClassName )
                {
                    break;
                }
                icat::i_plugin * pPlugin = 0;
                int r = pMgr->create( lpszClassName, &pPlugin );
                if( 0 != r )
                {
                    continue;
                }
                xos::i_unknown * pUnk = 0;
                pPlugin->query_interface( xos::i_unknown_id, ( void** )&pUnk );
                plugin * pP = plugin::get_item_from_pool( true );
                pP->init();
                pP->m_nPriority = pUnk->get_priority( 0 );
                pP->m_module = pMgr->module_name();
                pP->m_class = pUnk->class_name();
                pP->m_pPlugin = pPlugin;
                plugin_list.push_back( pP );
            }
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int app_plugin::helper_load( const char * lpszDir, module::LIST & mlist )
    {
        int ret = 0;

        icat::i_plugin_mgr * pMgr = 0;
        xos::i_list * pList = 0;
        char path[4096];

        if( 0 == ret )
        {
            ret = container_impl::get()->xos()->xos()->create( xos::i_xos::XOS_OBJ_LIST, ( void** )&pList );
        }

        if( 0 == ret )
        {
            container_impl::get()->xos()->crt()->strcpy( path, sizeof( path ), m_pWebApp->m_webapp_path.c_str() );
            container_impl::get()->xos()->misc()->path_append( path, lpszDir );
        }

        if( 0 == ret )
        {
            ret = container_impl::get()->scan_file_for_all_module( path, pList );
        }

        while( ( 0 == ret ) && ( pMgr = ( icat::i_plugin_mgr* )pList->front( 0 ) ) )
        {
            module * pM = module::get_item_from_pool( true );
            pM->init();
            pM->m_pPluginMgr = pMgr;
            mlist.push_back( pM );
            pList->pop_front();
        }

        xos_stl::release_interface( pList );

        return ret;
    }

} // cat
