/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "net_heart.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    net_heart::net_heart()
    {
    }

    net_heart::~net_heart()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    int net_heart::proc( xos::i_msg *& pMsg )
    {
        int nMsg = pMsg->get_int( 0, 0 );

        if( NET_HEART != nMsg )
        {
            return 0;
        }

        if( state::can_send_heart() )
        {
            connection::heart();
        }

        return 1;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

} // cat
