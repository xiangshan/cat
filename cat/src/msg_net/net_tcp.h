/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_NET_NET_TCP_H__
#define __MSG_NET_NET_TCP_H__

namespace cat
{

    class connection;

    class net_tcp
    {
    public:
        net_tcp();
        ~net_tcp();

    protected:
        int on_need_release( xos::i_msg *& pMsg );
        int on_need_close( xos::i_msg *& pMsg );
        int on_data_err( xos::i_msg *& pMsg );
        int on_init( xos::i_msg *& pMsg );
        int on_accept( xos::i_msg *& pMsg );
        int on_recv( xos::i_msg *& pMsg );
        int on_send( xos::i_msg *& pMsg );
        int on_connect( xos::i_msg *& pMsg );
        int on_shutdown( xos::i_msg *& pMsg );
        int on_close( xos::i_msg *& pMsg );

    public:
        static int set_server_num( int nNum );
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_NET_NET_TCP_H__
