/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../config/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "net_heart.h"
#include "net_tcp.h"
#include "net_udp.h"
#include "msg_net.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	msg_net::msg_net()
	{
	}

	msg_net::~msg_net()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int msg_net::init()
    {
        int ret = 0;
        return ret;
    }

    int msg_net::term()
    {
        int ret = 0;
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int msg_net::proc( xos::i_msg *& pMsg )
	{
		int ret = 0;

        if( 0 == ret )
        {
            ret = on_quitting( pMsg );
        }

        if( 0 == ret )
        {
            net_heart obj;
            ret = obj.proc( pMsg );
        }

        if( 0 == ret )
        {
            net_tcp obj;
            ret = obj.proc( pMsg );
        }

        if( 0 == ret )
        {
            net_udp obj;
            ret = obj.proc( pMsg );
        }

		return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int msg_net::on_quitting( xos::i_msg *& pMsg )
    {
        if( NET_QUITTING != pMsg->get_int( 0, 0 ) )
        {
            return 0;
        }

        connection::LIST & s_list = *connection::list();
       
        // �����ر�
        for( connection::ITER iter = s_list.begin(); iter != s_list.end(); ++iter )
        {
            connection * pC = *iter;
            if( ( pC->http_data() || pC->tcp_data() ) && pC->running() )
            {
                pC->set_timeout_time_s( config::get()->quit_wait_time_s );
                pC->post_shut_down( xos::i_socket::XOS_SD_SEND );
                pC->m_pQuitAfterSend = 0;
            }
            else
            {
                pC->post_close( 0, 0 );
            }
        }

        if( 0 == s_list.size() )
        {
            msg::notify_main_main( pMsg, MAIN_FLUSH, false );
        }

        return 1;
    }

} // cat
