/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_NET_MSG_NET_H__
#define __MSG_NET_MSG_NET_H__

namespace cat
{

    class msg_net
    {
    public:
        msg_net();
        ~msg_net();

    protected:
        int on_quitting( xos::i_msg *& pMsg );

    public:
		int proc( xos::i_msg *& pMsg );

    public:
        static int init();
        static int term();

    };

} // cat

#endif // __MSG_NET_MSG_NET_H__
