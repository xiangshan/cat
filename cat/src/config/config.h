/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __CONFIG_CONFIG_H__
#define __CONFIG_CONFIG_H__


namespace cat
{

    class config
    {
    public:
        typedef std::list< std::string > STR_LIST;
        typedef STR_LIST::iterator STR_ITER;

    public:
        config();
        ~config();

    protected:
        int route_path( xos_xml::i_xml_node * pNode );
        int get_value( xos_xml::i_xml_node * pNode );

    protected:
        int get_thread( const char * lpszKey, const char * lpszValue );
        int get_heart( const char * lpszKey, const char * lpszValue );
        int get_net( const char * lpszKey, const char * lpszValue );
        int get_timeout( const char * lpszKey, const char * lpszValue );
        int get_server( const char * lpszKey, const char * lpszValue );

    protected:
        int load( const char * lpszFile );

    public:
        static config * get();
        static int init();
        static int term();

    public:
        // thread
        int max_fast_job_thread_num;
        int max_slow_job_thread_num;

        // heart
        int heart_check_interval_ms;
        int sys_heart_interval_ms;
        int show_info_interval_ms;

        // net
        int tcp_max_post_accept;
        int tcp_max_post_recv;
        int udp_max_post_recv;
        int compress_size;

        // timeout
        int close_wait_time_s;
        int quit_wait_time_s;
        int net_timeout_ms;
        int http_timeout_s;
        int data_timeout_s;

        // server
        std::string webapps;
        int use_ssl;

    };

} // cat

#endif // __CONFIG_CONFIG_H__
