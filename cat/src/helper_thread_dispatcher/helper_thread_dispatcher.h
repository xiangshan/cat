/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __HELPER_THREAD_DISPATCHER_HELPER_THREAD_DISPATCHER_H__
#define __HELPER_THREAD_DISPATCHER_HELPER_THREAD_DISPATCHER_H__

namespace cat
{

    class helper_thread_dispatcher
    {
    public:
        helper_thread_dispatcher();
        ~helper_thread_dispatcher();

    protected:
        xos::i_callback * get_thread( bool bSlowJob );

    public:
        int dispatch( xos::i_msg *& pMsg );
        xos::i_callback * packet_thread();
        xos::i_callback * net_thread();

    public:
        static int init();
        static int term();

    };

} // cat

#endif // __HELPER_THREAD_DISPATCHER_HELPER_THREAD_DISPATCHER_H__
