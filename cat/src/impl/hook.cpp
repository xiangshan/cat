/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_thread_dispatcher/head.h"
#include "../msg_packet/helper_compress.h"
#include "../msg_packet/helper_encrypt.h"
#include "../msg_packet/helper_package.h"
#include "../helper_pass_through/head.h"
#include "../helper_webapp/head.h"
#include "../data_struct/pool.h"
#include "../msg_aio/head.h"
#include "../msg_net/head.h"
#include "../settings/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../xos/head.h"
#include "container_impl.h"
#include "hook.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    hook::hook()
    {
    }

    hook::~hook()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int hook::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = settings::init();
        }

        if( 0 == ret )
        {
            ret =os::init();
        }

        if( 0 == ret )
        {
            ret = config::init();
        }

        if( 0 == ret )
        {
            mgr::xos()->heart()->add_acceptor( msg::on_heart, config::get()->heart_check_interval_ms );
            mgr::xos()->heart()->set_interval( config::get()->sys_heart_interval_ms );
        }

        if( 0 == ret )
        {
            mgr::xos()->log()->add_log_module( LOG_NAME, 1 );
            mgr::xos()->log()->add_log_module( LOG_NAME, 2 );
            mgr::xos()->log()->add_log_module( LOG_NAME, 3 );
            mgr::xos()->log()->add_log_module( LOG_NAME, 4 );
        }

        if( 0 == ret )
        {
            ret = pool::init();
        }

        if( 0 == ret )
        {
            ret = helper_pass_through::init();
        }

        if( 0 == ret )
        {
            ret = helper_thread_dispatcher::init();
        }

        if( 0 == ret )
        {
            ret = msg_net::init();
        }

        if( 0 == ret )
        {
            ret = helper_compress::init();
        }

        if( 0 == ret )
        {
            ret = helper_encrypt::init();
        }

        if( 0 == ret )
        {
            ret = helper_package::init();
        }

        if( 0 == ret )
        {
            ret = msg_aio::init();
        }

        if( 0 == ret )
        {
            ret = container_impl::static_init();
        }

        if( 0 == ret )
        {
            ret = helper_webapp::init();
        }

        return ret;
    }

    int hook::term()
    {
        int ret = 0;

        helper_webapp::term();
        container_impl::static_term();
        msg_aio::term();
        helper_compress::term();
        helper_encrypt::term();
        helper_package::term();
        msg_net::term();
        helper_thread_dispatcher::term();
        helper_pass_through::term();
        pool::term();
        config::term();
        os::term();
        settings::term();

        return ret;
    }

} // cat
