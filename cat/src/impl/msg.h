/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_MSG_H__
#define __IMPL_MSG_H__

namespace cat
{
    namespace msg
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息定义
        // 
        // int[0]   : msg
        // int[1]   : thread index when pass through
        // int[-2]  : thread type
        // int[-1]  : msg type
        // bool[-1] : bSlowJob
        // 

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息发送函数
        // 
        int notify( xos::i_msg *& pMsg, int nThreadType, int nMsgType, int nMsg, bool bSlowJob, bool bPassThrough );
        int notify( xos::i_msg *& pMsg, int nThreadType, int nMsgType, bool bSlowJob, bool bPassThrough );
        int fill( xos::i_msg *& pMsg, int nThreadType, int nMsgType, int nMsg, bool bSlowJob );

        int notify_main( xos::i_msg *& pMsg, int nMsgType, int nMsg, bool bPassThrough );
        int notify_sub( xos::i_msg *& pMsg, int nMsgType, int nMsg, bool bSlowJob, bool bPassThrough );

        int notify_main_type( xos::i_msg *& pMsg, int nMsgType, bool bPassThrough );
        int notify_sub_type( xos::i_msg *& pMsg, int nMsgType, bool bSlowJob, bool bPassThrough );

        int notify_main_msg( xos::i_msg *& pMsg, int nMsg, bool bPassThrough );
        int notify_sub_msg( xos::i_msg *& pMsg, int nMsg, bool bSlowJob, bool bPassThrough );

        int notify_main( xos::i_msg *& pMsg, bool bPassThrough );
        int notify_sub( xos::i_msg *& pMsg, bool bSlowJob, bool bPassThrough );

        int notify_main_main( xos::i_msg *& pMsg, int nMsg, bool bPassThrough );
        int notify_main_main( int nMsg, bool bPassThrough );

        int notify_package( xos::i_msg *& pMsg, int nMsg, bool bPassThrough );
        int notify_net( xos::i_msg *& pMsg, bool bPassThrough );

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息修改函数
        // 
        int set_thread_type( xos::i_msg * pMsg, int nThreadType );
        int get_thread_type( xos::i_msg * pMsg );

        int set_msg_type( xos::i_msg * pMsg, int nMsgType );
        int get_msg_type( xos::i_msg * pMsg );

        int set_slow( xos::i_msg * pMsg, bool bSlowJob );
        bool get_slow( xos::i_msg * pMsg );

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息响应函数
        // 
        int on_aio( xos::i_msg *& pMsg );
        int on_msg( xos::i_msg *& pMsg );
        int on_heart();

    } // msg
} // cat

#endif // __IMPL_MSG_H__
