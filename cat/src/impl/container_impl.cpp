/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "../xos/head.h"
#include "../msg/head.h"
#include "container_impl.h"
#include "mgr.h"
#include "msg.h"

namespace cat
{

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    // 

    static container_impl * container_ptr = 0;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    // 

    container_impl::container_impl()
    {
        init_data();
    }

    container_impl::~container_impl()
    {
        term();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    // 
    // 插件文件就是目录下的basepath/pluginpath/main.xos
    // 
    int container_impl::load_plugin( const char * lpszBasePath, const char * lpszPluginPath, const char * lpszModule, const char * lpszModuleName, icat::i_plugin_mgr ** ppv )
    {
        int ret = 0;

        xos::i_dynamic * pDll = 0;
        icat::i_plugin_mgr * pPlugin = 0;
        icat::f_create fun = 0;

        char path[4096] = { 0 };

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_DYNAMIC, ( void** )&pDll );
        }

        if( 0 == ret )
        {
            mgr::xos()->crt()->strcpy( path, sizeof( path ), lpszBasePath );
        }

        if( ( 0 == ret ) && lpszPluginPath )
        {
            mgr::xos()->misc()->path_append( path, lpszPluginPath );
        }

        if( 0 == ret )
        {
            ret = pDll->load_and_get( ( void ** )&fun, path, lpszModule, "create" );
        }

        if( 0 == ret )
        {
            ret = fun( mgr::xos(), pDll, lpszModuleName, &pPlugin );
        }

        if( 0 == ret )
        {
            *ppv = pPlugin;
            pDll = 0;
        }

        xos_stl::release_interface( pDll );

        return ret;
    }

    int container_impl::scan_dir_for_fixed_name_module( const char * lpszBasePath, const char * lpszModule, xos::i_list * pList )
    {
        int ret = 0;

        xos::i_dir * pDir = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_DIR, ( void** )&pDir );
        }

        if( 0 == ret )
        {
            ret = pDir->begin_find_file( lpszBasePath );
        }

        while( 0 == ret )
        {
            xos::i_file * pFile = 0;
            int r = pDir->get_find_result( &pFile );
            if( 0 != r )
            {
                break;
            }
            if( xos::i_file::FILE_TYPE_DIR != pFile->get_type() )
            {
                continue;
            }
            const char * lpszPath = pFile->get_name( 0, 0, 0 );
            icat::i_plugin_mgr * pPlugin = 0;
            r = load_plugin( lpszBasePath, lpszPath, lpszModule, lpszPath, &pPlugin );
            if( 0 != r )
            {
                continue;
            }
            pList->push_back( pPlugin );
        }

        xos_stl::release_interface( pDir );

        return ret;
    }

    int container_impl::scan_file_for_all_module( const char * lpszBasePath, xos::i_list * pList )
    {
        int ret = 0;

        xos::i_dir * pDir = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->xos()->create( xos::i_xos::XOS_OBJ_DIR, ( void** )&pDir );
        }

        if( 0 == ret )
        {
            ret = pDir->begin_find_file( lpszBasePath );
        }

        while( 0 == ret )
        {
            xos::i_file * pFile = 0;
            int r = pDir->get_find_result( &pFile );
            if( 0 != r )
            {
                break;
            }
            if( xos::i_file::FILE_TYPE_FILE != pFile->get_type() )
            {
                continue;
            }
            const char * lpszFile = pFile->get_name( 0, 0, 0 );
            if( 0 != mgr::xos()->common_misc()->end_with( lpszFile, ".xos" ) )
            {
                continue;
            }
            icat::i_plugin_mgr * pPlugin = 0;
            char name[1024];
            {
                mgr::xos()->crt()->strcpy( name, sizeof( name ), lpszFile );
                int len = mgr::xos()->crt()->strlen( name );
                name[len-4] = 0;
            }
            r = load_plugin( lpszBasePath, 0, name, name, &pPlugin );
            if( 0 != r )
            {
                continue;
            }
            pList->push_back( pPlugin );
        }

        xos_stl::release_interface( pDir );

        return ret;
    }

    int container_impl::add_ret_data( icat::i_task * pTask, const char * lpszData, int nLen )
    {
        int ret = 0;

        icat::i_response * pR = pTask->get_response();
        xos::i_list * pRetData = pR->get_data();
        const char * lpszDataPos = lpszData;
        int nDataLen = nLen;

        while( nDataLen > 0 )
        {
            xos::i_buf * pBuf = ( xos::i_buf * )pRetData->back( 0 );
            if( !pBuf )
            {
                break;
            }
            int nBufLen = pBuf->get_len( 0 );
            int nBufPos = pBuf->get_pos( 0 );
            if( 0 != nBufPos )
            {
                pBuf->move_data_to_begin();
            }
            int nBufLeft = xos::i_buf::BUF_SIZE - nBufLen;
            int len = nDataLen > nBufLeft ? nBufLeft : nDataLen;
            if( 0 == len )
            {
                break;
            }
            pBuf->add_data( lpszDataPos, len );
            lpszDataPos += len;
            nDataLen -= len;
            break;
        }

        while( nDataLen > 0 )
        {
            int len = nDataLen > xos::i_buf::BUF_SIZE ? xos::i_buf::BUF_SIZE : nDataLen;
            xos::i_buf * pBuf = mgr::xos()->buf();
            pBuf->add_data( lpszDataPos, len );
            pRetData->push_back( pBuf );
            lpszDataPos += len;
            nDataLen -= len;
            pBuf = 0;
        }

        return ret;
    }

    int container_impl::add_ret_data( icat::i_task * pTask, xos::i_buf *& pBuf )
    {
        int ret = 0;

        icat::i_response * pR = pTask->get_response();
        xos::i_list * pRetData = pR->get_data();
        pRetData->push_back( pBuf );
        pBuf = 0;

        return ret;
    }

    int container_impl::add_ret_str( icat::i_task * pTask, const char * lpszData )
    {
        int ret = 0;

        int len = mgr::xos()->crt()->strlen( lpszData );
        ret = add_ret_data( pTask, lpszData, len );

        return ret;
    }

    const char * container_impl::framework()
    {
        return config::get()->webapps.c_str();
    }

    const char * container_impl::webapps()
    {
        return config::get()->webapps.c_str();
    }

    const char * container_impl::exe_path()
    {
        return mgr::xos()->exe_path();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int container_impl::notify( icat::i_task * pTask, bool bSlowJob )
    {
        xos::i_msg * pMsg = mgr::xos()->msg();
        msg::fill( pMsg, THREAD_TYPE_WORKER, MSG_TYPE_PROC, PROC_PLUGIN, bSlowJob );
        pMsg->set_void( 0, pTask );
        pTask = 0;
        return mgr::notify( pMsg, false );
    }

    xos_container::i_container * container_impl::xos()
    {
        return os::get();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int container_impl::put_back_to_pool( container_impl * pT, bool bLock )
    {
        int ret = 0;
        delete pT;
        return ret;
    }

    int container_impl::init()
    {
        int ret = 0;
        return ret;
    }

    int container_impl::term()
    {
        int ret = 0;
        init_data();
        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int container_impl::static_init()
    {
        int ret = 0;
        if( 0 == ret )
        {
            ret = xos_stl::init_item( container_ptr );
        }
        return ret;
    }

    int container_impl::static_term()
    {
        int ret = 0;
        xos_stl::term_item( container_ptr );
        return ret;
    }

    container_impl * container_impl::get()
    {
        return container_ptr;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int container_impl::init_data()
    {
        int ret = 0;
        return ret;
    }

} // cat
