/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_CONTAINER_IMPL_H__
#define __IMPL_CONTAINER_IMPL_H__

#include "../tools/head.h"

namespace cat
{
    class container_impl
    {
    public:
        container_impl();
        ~container_impl();

        // 
        // icat::i_container methods
        // 
    public:
        // 
        // helper functions. 插件文件就是目录下的basepath/pluginpath/main.xos
        // 
        int load_plugin( const char * lpszBasePath, const char * lpszPluginPath, const char * lpszModule, const char * lpszModuleName, icat::i_plugin_mgr ** ppv );
        int scan_dir_for_fixed_name_module( const char * lpszBasePath, const char * lpszModule, xos::i_list * pList );
        int scan_file_for_all_module( const char * lpszBasePath, xos::i_list * pList );
        int add_ret_data( icat::i_task * pTask, const char * lpszData, int nLen );
        int add_ret_data( icat::i_task * pTask, xos::i_buf *& pBuf );
        int add_ret_str( icat::i_task * pTask, const char * lpszData );
        const char * framework();
        const char * webapps();
        const char * exe_path();
        int notify( icat::i_task * pTask, bool bSlowJob );
        xos_container::i_container * xos();

    public:
        int put_back_to_pool( container_impl * pT, bool bLock );
        int init();
        int term();

    public:
        static container_impl * get();
        static int static_init();
        static int static_term();

    protected:
        int init_data();

    };
} // cat

#endif // __IMPL_CONTAINER_IMPL_H__
