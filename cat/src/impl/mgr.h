/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_IMPL_H__
#define __IMPL_IMPL_H__

//////////////////////////////////////////////////////////////////////////////////
// 

namespace cat
{
    class mgr
    {
    public:
        mgr();
        ~mgr();

    public:
        static int notify( xos::i_msg *& pMsg, bool bPassThrough );
        static xos_container::i_container * xos();

    public:
        int run( int argc, const char  * argv[] );

    private:
        bool proc();
        int init();
        int term();

    };
} // cat

#endif // __IMPL_IMPL_H__
