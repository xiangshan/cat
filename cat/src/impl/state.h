/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_STATE_H__
#define __IMPL_STATE_H__

//////////////////////////////////////////////////////////////////////////////////
// 

namespace cat
{
    namespace state
    {
         int set_initting();
         int set_running();
         int set_starting();
         int set_quitting();
         int set_flushing();
         int set_stopped();

         bool can_send_heart();
         bool initting();
         bool running();
         bool starting();
         bool quitting();
         bool flushing();
         bool stopped();
    }
} // cat

#endif // __IMPL_STATE_H__
