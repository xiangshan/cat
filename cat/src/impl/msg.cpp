/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_thread_dispatcher/head.h"
#include "../helper_proc/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "state.h"
#include "msg.h"
#include "mgr.h"

namespace cat
{
    namespace msg
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息发送函数
        // 

        int notify( xos::i_msg *& pMsg, int nThreadType, int nMsgType, int nMsg, bool bSlowJob, bool bPassThrough )
        {
            set_thread_type( pMsg, nThreadType );
            set_msg_type( pMsg, nMsgType );
            set_slow( pMsg, bSlowJob );
            pMsg->set_int( 0, nMsg );
            return mgr::notify( pMsg, bPassThrough );
        }

        int notify( xos::i_msg *& pMsg, int nThreadType, int nMsgType, bool bSlowJob, bool bPassThrough )
        {
            set_thread_type( pMsg, nThreadType );
            set_msg_type( pMsg, nMsgType );
            set_slow( pMsg, bSlowJob );
            return mgr::notify( pMsg, bPassThrough );
        }

        int fill( xos::i_msg *& pMsg, int nThreadType, int nMsgType, int nMsg, bool bSlowJob )
        {
            int ret = 0;
            set_thread_type( pMsg, nThreadType );
            set_msg_type( pMsg, nMsgType );
            set_slow( pMsg, bSlowJob );
            pMsg->set_int( 0, nMsg );
            return ret;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int notify_main( xos::i_msg *& pMsg, int nMsgType, int nMsg, bool bPassThrough )
        {
            set_msg_type( pMsg, nMsgType );
            pMsg->set_int( 0, nMsg );
            return notify_main( pMsg, bPassThrough );
        }

        int notify_sub( xos::i_msg *& pMsg, int nMsgType, int nMsg, bool bSlowJob, bool bPassThrough )
        {
            set_msg_type( pMsg, nMsgType );
            pMsg->set_int( 0, nMsg );
            return notify_sub( pMsg, bSlowJob, bPassThrough );
        }

        int notify_main_type( xos::i_msg *& pMsg, int nMsgType, bool bPassThrough )
        {
            set_msg_type( pMsg, nMsgType );
            return notify_main( pMsg, bPassThrough );
        }

        int notify_sub_type( xos::i_msg *& pMsg, int nMsgType, bool bSlowJob, bool bPassThrough )
        {
            set_msg_type( pMsg, nMsgType );
            return notify_sub( pMsg, bSlowJob, bPassThrough );
        }

        int notify_main_msg( xos::i_msg *& pMsg, int nMsg, bool bPassThrough )
        {
            pMsg->set_int( 0, nMsg );
            return notify_main( pMsg, bPassThrough );
        }

        int notify_sub_msg( xos::i_msg *& pMsg, int nMsg, bool bSlowJob, bool bPassThrough )
        {
            pMsg->set_int( 0, nMsg );
            return notify_sub( pMsg, bSlowJob, bPassThrough );
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int notify_main( xos::i_msg *& pMsg, bool bPassThrough )
        {
            set_thread_type( pMsg, THREAD_TYPE_MAIN );
            return mgr::notify( pMsg, bPassThrough );
        }

        int notify_sub( xos::i_msg *& pMsg, bool bSlowJob, bool bPassThrough )
        {
            set_thread_type( pMsg, THREAD_TYPE_WORKER );
            set_slow( pMsg, bSlowJob );
            return mgr::notify( pMsg, bPassThrough );
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int notify_main_main( xos::i_msg *& pMsg, int nMsg, bool bPassThrough )
        {
            return notify( pMsg, THREAD_TYPE_MAIN, MSG_TYPE_MAIN, nMsg, false, bPassThrough );
        }

        int notify_main_main( int nMsg, bool bPassThrough )
        {
            xos::i_msg * pMsg = mgr::xos()->msg();
            return notify_main_main( pMsg, nMsg, bPassThrough );
        }

        int notify_package( xos::i_msg *& pMsg, int nMsg, bool bPassThrough )
        {
            int ret = 0;
            ret = notify( pMsg, THREAD_TYPE_PACKET, MSG_TYPE_PACKET, nMsg, false, false );
            return ret;
        }

        int notify_net( xos::i_msg *& pMsg, bool bPassThrough )
        {
            int ret = 0;
            ret = notify( pMsg, THREAD_TYPE_NET, MSG_TYPE_NET, false, false );
            return ret;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息修改函数
        // 

        int set_thread_type( xos::i_msg * pMsg, int nThreadType )
        {
            int ret = 0;
            int nNum = xos::i_msg::EXTRA_PARAM_NUM;
            pMsg->set_int( nNum - 2, nThreadType );
            return ret;
        }

        int get_thread_type( xos::i_msg * pMsg )
        {
            int nNum = xos::i_msg::EXTRA_PARAM_NUM;
            return pMsg->get_int( nNum - 2, 0 );
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int set_msg_type( xos::i_msg * pMsg, int nMsgType )
        {
            int ret = 0;
            int nNum = xos::i_msg::EXTRA_PARAM_NUM;
            pMsg->set_int( nNum - 1, nMsgType );
            return ret;
        }

        int get_msg_type( xos::i_msg * pMsg )
        {
            int nNum = xos::i_msg::EXTRA_PARAM_NUM;
            return pMsg->get_int( nNum - 1, 0 );
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        int set_slow( xos::i_msg * pMsg, bool bSlowJob )
        {
            int ret = 0;
            int nNum = xos::i_msg::EXTRA_PARAM_NUM;
            pMsg->set_bool( nNum - 1, bSlowJob );
            return ret;
        }

        bool get_slow( xos::i_msg * pMsg )
        {
            int nNum = xos::i_msg::EXTRA_PARAM_NUM;
            return pMsg->get_bool( nNum - 1, 0 );
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 
        // 消息响应函数
        // 

        int on_aio( xos::i_msg *& pMsg )
        {
            return notify( pMsg, THREAD_TYPE_NET, MSG_TYPE_AIO, false, false );
        }

        int on_msg( xos::i_msg *& pMsg )
        {
            int ret = 0;
            helper_proc obj;
            obj.proc( pMsg );
            return ret;
        }

        int on_heart()
        {
            int ret = 0;
            if( !state::can_send_heart() )
            {
                return ret;
            }
            notify_main_main( MAIN_HEART, false );
            return ret;
        }

    } // msg
} // xos
