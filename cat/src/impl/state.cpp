/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "state.h"

namespace cat
{
    namespace state
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        enum enumState
        {
            STATE_INITTING,
            STATE_STARTING,
            STATE_RUNNING,
            STATE_QUITTING,
            STATE_FLUSHING,
            STATE_STOPPED
        };

        static int sys_state_n = STATE_STOPPED;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 

        bool can_send_heart()
        {
            return running() || quitting();
        }

        bool initting()
        {
            return STATE_INITTING == sys_state_n;
        }

        bool running()
        {
            return STATE_RUNNING == sys_state_n;
        }

        bool starting()
        {
            return STATE_STARTING == sys_state_n;
        }

        bool quitting()
        {
            return STATE_QUITTING == sys_state_n;
        }

        bool flushing()
        {
            return STATE_FLUSHING == sys_state_n;
        }

        bool stopped()
        {
            return STATE_STOPPED == sys_state_n;
        }

        int set_initting()
        {
            int ret = 0;
            sys_state_n = STATE_INITTING;
            return ret;
        }

        int set_running()
        {
            int ret = 0;
            sys_state_n = STATE_RUNNING;
            return ret;
        }

        int set_starting()
        {
            int ret = 0;
            sys_state_n = STATE_STARTING;
            return ret;
        }

        int set_quitting()
        {
            int ret = 0;
            sys_state_n = STATE_QUITTING;
            return ret;
        }

        int set_flushing()
        {
            int ret = 0;
            sys_state_n = STATE_FLUSHING;
            return ret;
        }

        int set_stopped()
        {
            int ret = 0;
            sys_state_n = STATE_STOPPED;
            return ret;
        }

    } // state
} // cat
