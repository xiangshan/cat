/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "helper_package.h"
#include "pkg_udp.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	pkg_udp::pkg_udp()
	{
	}

	pkg_udp::~pkg_udp()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int pkg_udp::proc( xos::i_msg *& pMsg )
	{
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_UDP_UN_PACKAGE_FLUSH:
            {
                flush( pMsg );
            }
            break;
        case PACKET_UDP_UN_PACKAGE:
            {
                un_package( pMsg );
            }
            break;
        case PACKET_UDP_PACKAGE:
            {
                package( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int pkg_udp::un_package( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        request * pRequest = pTask->m_pRequest;

        if( 0 == ret )
        {
            ret = helper_package::get()->un_pack( pRequest->m_pUnPackageList, pRequest->m_pUnEnctyptList, pConnect->m_str_key.c_str() );
        }

        if( 0 == ret )
        {
            msg::notify_package( pMsg, PACKET_UDP_UN_COMPRESS, false );
        }

        if( ( ret < 0 ) && pConnect->running() )
        {
            pConnect->set_data_err();
            pConnect->lock_server();
            pMsg->set_void( 0, pConnect );
            pMsg->set_int( 0, NET_UDP_DATA_ERR );
            msg::notify_net( pMsg, false );
        }

        return ret;
    }

    int pkg_udp::package( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        response * pResponse = pTask->m_pResponse;

        if( 0 == ret )
        {
            ret = helper_package::get()->pack( pResponse->m_pPackageList, pResponse->m_pCompressList, pConnect->m_str_key.c_str() );
        }

        if( 0 == ret )
        {
            msg::notify_package( pMsg, PACKET_UDP_ENCRYPT, false );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int pkg_udp::flush( xos::i_msg *& pMsg )
    {
        int ret = 0;

        msg::notify_package( pMsg, PACKET_UDP_UN_COMPRESS_FLUSH, false );

        return ret;
    }

} // cat
