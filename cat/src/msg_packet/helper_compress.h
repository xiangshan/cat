/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_PACKET_MSG_COMPRESS_H__
#define __MSG_PACKET_MSG_COMPRESS_H__

namespace cat
{

    class helper_compress
    {
    public:
        helper_compress();
        ~helper_compress();

    public:
        static xos_compress::i_protocol * get();
        static int init();
        static int term();

    public:
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_PACKET_MSG_COMPRESS_H__
