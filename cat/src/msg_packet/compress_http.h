/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_PACKET_COMPRESS_HTTP_H__
#define __MSG_PACKET_COMPRESS_HTTP_H__

namespace cat
{

    class compress_http
    {
    public:
        compress_http();
        ~compress_http();

    protected:
        int un_compress( xos::i_msg *& pMsg );
        int compress( xos::i_msg *& pMsg );

        int flush( xos::i_msg *& pMsg );

    public:
		int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_PACKET_COMPRESS_HTTP_H__
