/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../impl/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "helper_package.h"
#include "pkg_http.h"
#include "pkg_tcp.h"
#include "pkg_udp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static xos_package::i_package * package_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    helper_package::helper_package()
    {
    }

    helper_package::~helper_package()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int helper_package::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->package()->create( &package_ptr );
        }

        return ret;
    }

    int helper_package::term()
    {
        int ret = 0;
        xos_stl::release_interface( package_ptr );
        return ret;
    }

    xos_package::i_package * helper_package::get()
    {
        return package_ptr;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

    int helper_package::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_HTTP_UN_PACKAGE_FLUSH:
        case PACKET_HTTP_UN_PACKAGE:
        case PACKET_HTTP_PACKAGE:
            {
                pkg_http obj;
                obj.proc( pMsg );
            }
            break;
        case PACKET_TCP_UN_PACKAGE_FLUSH:
        case PACKET_TCP_UN_PACKAGE:
        case PACKET_TCP_PACKAGE:
            {
                pkg_tcp obj;
                obj.proc( pMsg );
            }
            break;
        case PACKET_UDP_UN_PACKAGE_FLUSH:
        case PACKET_UDP_UN_PACKAGE:
        case PACKET_UDP_PACKAGE:
            {
                pkg_udp obj;
                obj.proc( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
    }

} // cat
