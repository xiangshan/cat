/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "compress_http.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	compress_http::compress_http()
	{
	}

	compress_http::~compress_http()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int compress_http::proc( xos::i_msg *& pMsg )
	{
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_HTTP_UN_COMPRESS_FLUSH:
            {
                flush( pMsg );
            }
            break;
        case PACKET_HTTP_UN_COMPRESS:
            {
                un_compress( pMsg );
            }
            break;
        case PACKET_HTTP_COMPRESS:
            {
                compress( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int compress_http::un_compress( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        request * pRequest = pTask->m_pRequest;

        pRequest->m_pUnCompressList->add_to_tail( pRequest->m_pUnEnctyptList );
        msg::notify_sub( pMsg, MSG_TYPE_PROC, PROC_HTTP, false, false );

        return ret;
    }

    int compress_http::compress( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        response * pResponse = pTask->m_pResponse;

        pResponse->m_pCompressList->add_to_tail( pResponse->m_pRawDataList );
        msg::notify_package( pMsg, PACKET_HTTP_PACKAGE, false );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int compress_http::flush( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        pConnect->un_lock_server();

        return ret;
    }

} // cat
