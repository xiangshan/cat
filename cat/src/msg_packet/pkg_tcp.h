/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_PACKET_PKG_TCP_H__
#define __MSG_PACKET_PKG_TCP_H__

namespace cat
{

    class pkg_tcp
    {
    public:
        pkg_tcp();
        ~pkg_tcp();

    protected:
        int un_package( xos::i_msg *& pMsg );
        int package( xos::i_msg *& pMsg );

        int flush( xos::i_msg *& pMsg );

    public:
		int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_PACKET_PKG_TCP_H__
