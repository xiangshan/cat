/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../tools/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "helper_encrypt.h"
#include "encrypt_http.h"
#include "encrypt_tcp.h"
#include "encrypt_udp.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    helper_encrypt::helper_encrypt()
    {
    }

    helper_encrypt::~helper_encrypt()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int helper_encrypt::init()
    {
        int ret = 0;

        {
            char file[4096];
            tools::full_config_path_file( file, sizeof( file ), "server.crt" );
            mgr::xos()->encrypt()->set_server_cert( file );
        }

        {
            char file[4096];
            tools::full_config_path_file( file, sizeof( file ), "server.key" );
            mgr::xos()->encrypt()->set_server_key( file );
        }

        return ret;
    }

    int helper_encrypt::term()
    {
        int ret = 0;
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int helper_encrypt::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_HTTP_UN_ENCRYPT_FLUSH:
        case PACKET_HTTP_UN_ENCRYPT:
        case PACKET_HTTP_ENCRYPT:
            {
                encrypt_http obj;
                obj.proc( pMsg );
            }
            break;
        case PACKET_TCP_UN_ENCRYPT_FLUSH:
        case PACKET_TCP_UN_ENCRYPT:
        case PACKET_TCP_ENCRYPT:
            {
                encrypt_tcp obj;
                obj.proc( pMsg );
            }
            break;
        case PACKET_UDP_UN_ENCRYPT_FLUSH:
        case PACKET_UDP_UN_ENCRYPT:
        case PACKET_UDP_ENCRYPT:
            {
                encrypt_udp obj;
                obj.proc( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
    }

} // cat
