/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "encrypt_http.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	encrypt_http::encrypt_http()
	{
	}

	encrypt_http::~encrypt_http()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int encrypt_http::proc( xos::i_msg *& pMsg )
	{
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_HTTP_UN_ENCRYPT_FLUSH:
            {
                flush( pMsg );
            }
            break;
        case PACKET_HTTP_UN_ENCRYPT:
            {
                un_encrypt( pMsg );
            }
            break;
        case PACKET_HTTP_ENCRYPT:
            {
                encrypt( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    //  
    // -1 : 解密数据失败，需要断掉连接
    //  0 : 成功
    //  1 : 需要再运行本函数一次
    // 
    int encrypt_http::helper_un_encrypt( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        xos_encrypt::i_encrypt * pEncrypt = pConnect->m_pEncrypt;
        tcp * pTcp = pConnect->m_pTcp;
        request * pRequest = pTask->m_pRequest;

        if( 0 == ret )
        {
            ret = pEncrypt->decrypt( pRequest->m_pRecvList, pRequest->m_pUnEnctyptList );
        }

        if( -1 == ret )
        {
            if( pConnect->m_bSslHandShaked )
            {
                LOG4( "(%s:%d)(%s:%d) - decode https data failed", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum );
            }
            else
            {
                LOG4( "(%s:%d)(%s:%d) - ssl shake hand failed", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum );
            }
        }

        if( ( 0 == ret ) && !pConnect->m_bSslHandShaked && pEncrypt->ready() )
        {
            LOG4( "(%s:%d)(%s:%d) ---------- shake hand ok", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum );
        }

        if( ( 0 == ret ) && ( pRequest->m_pUnEnctyptList->size() > 0 ) )
        {
            if( pConnect->m_bSslHandShaked )
            {
                pMsg->set_void( 0, pTask );
                msg::notify_package( pMsg, PACKET_HTTP_UN_PACKAGE, false );
            }
            else
            {
                aio_tcp obj;
                obj.post_send( pConnect, pRequest->m_pUnEnctyptList );
            }
        }

        if( ( 0 == ret ) && !pConnect->m_bSslHandShaked && pEncrypt->ready() )
        {
            LOG4( "------------------shakehand ok, need proc again-----------------------" );
            pConnect->m_bSslHandShaked = true;
            pConnect->net_timeout_ms = 0;
            ret = 1;
        }

        return ret;
    }

    int encrypt_http::un_encrypt( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        tcp * pTcp = pConnect->m_pTcp;
        request * pRequest = pTask->m_pRequest;
        xos::i_buf * pBuf = pMsg->get_buf( 0, 0 );

        if( 0 == ret )
        {
            pRequest->m_pRecvList->push_back( pBuf );
            pTask->lock_server_for_request_data();
            pConnect->un_lock_server();
            pBuf = 0;
            LOG4( "un_encrypt 1 (%s:%d)(%s:%d)(%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum );
        }

        if( 0 == ret )
        {
            if( !pConnect->ssl() )
            {
                pRequest->m_pUnEnctyptList->add_to_tail( pRequest->m_pRecvList );
                pMsg->set_void( 0, pTask );
                msg::notify_package( pMsg, PACKET_HTTP_UN_PACKAGE, false );
            }
            else
            {
                ret = helper_un_encrypt( pMsg );
                if( 1 == ret )
                {
                    ret = helper_un_encrypt( pMsg );
                }
            }
        }

        {
            pTask->un_lock_server_for_request_data();
            LOG4( "un_encrypt 2 (%s:%d)(%s:%d)(%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum );
        }

        if( ( ret < 0 ) && pConnect->running() )
        {
            pConnect->set_data_err();
            pConnect->lock_server();
            pMsg->set_void( 0, pConnect );
            pMsg->set_int( 0, NET_TCP_DATA_ERR );
            msg::notify_net( pMsg, false );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int encrypt_http::helper_encrypt( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        response * pResponse = pTask->m_pResponse;
        xos_encrypt::i_encrypt * pEncrypt = pConnect->m_pEncrypt;

        if( 0 == ret )
        {
            ret = pEncrypt->encrypt( pResponse->m_pPackageList, pResponse->m_pEnctyptList );
        }

        return ret;
    }

    int encrypt_http::encrypt( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        response * pResponse = pTask->m_pResponse;

        if( !pConnect->ssl() )
        {
            pResponse->m_pEnctyptList->add_to_tail( pResponse->m_pPackageList );
        }
        else
        {
            ret = helper_encrypt( pMsg );
        }

        if( 0 == ret )
        {
            pTask->quit_close_test();
        }

        {
            tcp * pTcp = pConnect->m_pTcp;
            LOG4( "done : (%s:%d)(%s:%d)(%d)", pTcp->m_local_ip.c_str(), pTcp->m_nLocalPort, pTcp->m_peer_ip.c_str(), pTcp->m_nPeerPort, pConnect->m_nLockNum );
        }

        if( 0 == ret )
        {
            aio_tcp obj;
            obj.post_send( pConnect, pResponse->m_pEnctyptList );
        }

        if( ( ret < 0 ) && pConnect->running() )
        {
            pConnect->set_data_err();
            pConnect->lock_server();
            pMsg->set_void( 0, pConnect );
            pMsg->set_int( 0, NET_TCP_DATA_ERR );
            msg::notify_net( pMsg, false );
        }

        task::term_task( pTask );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int encrypt_http::flush( xos::i_msg *& pMsg )
    {
        int ret = 0;

        msg::notify_package( pMsg, PACKET_HTTP_UN_PACKAGE_FLUSH, false );

        return ret;
    }

} // cat
