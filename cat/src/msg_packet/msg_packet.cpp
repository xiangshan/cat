/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "helper_compress.h"
#include "helper_package.h"
#include "helper_encrypt.h"
#include "msg_packet.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    msg_packet::msg_packet()
    {
    }

    msg_packet::~msg_packet()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int msg_packet::proc( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( 0 == ret )
        {
            helper_encrypt obj;
            ret = obj.proc( pMsg );
        }

        if( 0 == ret )
        {
            helper_package obj;
            ret = obj.proc( pMsg );
        }

        if( 0 == ret )
        {
            helper_compress obj;
            ret = obj.proc( pMsg );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

} // cat
