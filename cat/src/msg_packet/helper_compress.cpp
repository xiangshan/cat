/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "helper_compress.h"
#include "compress_http.h"
#include "compress_tcp.h"
#include "compress_udp.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static xos_compress::i_protocol * compress_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    helper_compress::helper_compress()
    {
    }

    helper_compress::~helper_compress()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int helper_compress::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::xos()->compress()->create( xos_compress::OBJ_PROTOCOL, ( void** )&compress_ptr );
        }

        return ret;
    }

    int helper_compress::term()
    {
        int ret = 0;
        xos_stl::release_interface( compress_ptr );
        return ret;
    }

    xos_compress::i_protocol * helper_compress::get()
    {
        return compress_ptr;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // proc

    // 
    // 
    // 
    int helper_compress::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_HTTP_UN_COMPRESS_FLUSH:
        case PACKET_HTTP_UN_COMPRESS:
        case PACKET_HTTP_COMPRESS:
            {
                compress_http obj;
                obj.proc( pMsg );
            }
            break;
        case PACKET_TCP_UN_COMPRESS_FLUSH:
        case PACKET_TCP_UN_COMPRESS:
        case PACKET_TCP_COMPRESS:
            {
                compress_tcp obj;
                obj.proc( pMsg );
            }
            break;
        case PACKET_UDP_UN_COMPRESS_FLUSH:
        case PACKET_UDP_UN_COMPRESS:
        case PACKET_UDP_COMPRESS:
            {
                compress_udp obj;
                obj.proc( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
    }

} // cat
