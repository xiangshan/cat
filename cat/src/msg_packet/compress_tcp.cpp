/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../data_struct/head.h"
#include "../msg_aio/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "../impl/head.h"
#include "helper_package.h"
#include "compress_tcp.h"

namespace cat
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 

	compress_tcp::compress_tcp()
	{
	}

	compress_tcp::~compress_tcp()
	{
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// proc

	int compress_tcp::proc( xos::i_msg *& pMsg )
	{
        int ret = 1;

        int nMsg = pMsg->get_int( 0, 0 );

        switch( nMsg )
        {
        case PACKET_TCP_UN_COMPRESS_FLUSH:
            {
                flush( pMsg );
            }
            break;
        case PACKET_TCP_UN_COMPRESS:
            {
                un_compress( pMsg );
            }
            break;
        case PACKET_TCP_COMPRESS:
            {
                compress( pMsg );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
	}

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int compress_tcp::un_compress( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        request * pRequest = pTask->m_pRequest;
        xos::i_buf * pBuf = 0;

        {
            pRequest->m_pUnCompressList->add_to_tail( pRequest->m_pUnPackageList );
        }

        while( ( pBuf = ( xos::i_buf * )pRequest->m_pUnCompressList->front( 0 ) ) )
        {
            pRequest->m_pUnCompressList->pop_front();
            task * pT = task::init_task( pConnect, false );
            pT->m_pRequest->m_pUnCompressList->push_back( pBuf );
            xos::i_msg * pM = mgr::xos()->msg();
            pM->set_void( 0, pT );
            pT = 0;
            msg::notify_sub( pM, MSG_TYPE_PROC, PROC_TCP, false, false );
        }

        pTask->un_lock_server_for_request_data();

        return ret;
    }

    int compress_tcp::compress( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        response * pResponse = pTask->m_pResponse;

        pResponse->m_pCompressList->add_to_tail( pResponse->m_pRawDataList );
        msg::notify_package( pMsg, PACKET_TCP_PACKAGE, false );

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int compress_tcp::flush( xos::i_msg *& pMsg )
    {
        int ret = 0;

        task * pTask = ( task * )pMsg->get_void( 0, 0 );
        connection * pConnect = pTask->m_pConnection;
        request * pRequest = pTask->m_pRequest;

        pRequest->m_pUnCompressList->release_all();
        pRequest->m_pUnPackageList->release_all();
        pRequest->m_pUnEnctyptList->release_all();

        pTask->un_lock_server_for_request_data();
        pConnect->un_lock_server();

        return ret;
    }

} // cat
