/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../helper_pass_through/head.h"
#include "../helper_webapp/head.h"
#include "../data_struct/head.h"
#include "../msg_net/net_tcp.h"
#include "../msg_aio/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "../msg/head.h"
#include "msg_main.h"

namespace cat
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    msg_main::msg_main()
    {
    }

    msg_main::~msg_main()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    // 
    // int[0] : msg type
    // 
    int msg_main::proc( xos::i_msg *& pMsg )
    {
        int ret = 1;

        switch( pMsg->get_int( 0, 0 ) )
        {
        case MAIN_START_HTTP_SERVER:
            {
                start_http_server( pMsg );
            }
            break;
        case MAIN_PASS_THROUGH:
            {
                helper_pass_through obj;
                obj.proc( pMsg );
            }
            break;
            // operation result
        case MAIN_STARTED:
            {
                on_started( pMsg );
            }
            break;
        case MAIN_QUITED:
            {
                on_quitted( pMsg );
            }
            break;
        case MAIN_HEART:
            {
                on_heart( pMsg );
            }
            break;
            // operation
        case MAIN_STARTING:
            {
                start( pMsg );
            }
            break;
        case MAIN_QUITING:
            {
                quit( pMsg );
            }
            break;
        case MAIN_FLUSH:
            {
                mgr::xos()->log()->flush();
                state::set_flushing();
                msg::notify_main_main( pMsg, MAIN_QUITED, true );
            }
            break;
        default:
            {
                ret = 0;
            }
            break;
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int msg_main::helper_start_http_server( int nPort )
    {
        int ret = 0;

        connection * pConnect = 0;
        tcp * pTcp = 0;

        if( 0 == ret )
        {
            ret = tcp::init( pTcp, pConnect );
        }

        if( 0 == ret )
        {
            pTcp->m_local_ip = "";
            pTcp->m_nLocalPort = nPort;
            pConnect->set_http_listen();
        }

        if( 0 == ret )
        {
            aio_tcp obj;
            obj.post_init( pConnect );
        }

        return ret;
    }

    int msg_main::start_http_server( xos::i_msg *& pMsg )
    {
        int ret = 0;

        int nPorts[1024] = { 0 };
        int nNum = 0;

        if( 0 == ret )
        {
            ret = helper_webapp::get_ports( nPorts, sizeof( nPorts ) / sizeof( nPorts[0] ), &nNum );
        }

        if( ( 0 == ret ) && ( 0 == nNum ) )
        {
            LOG4( "not find webapps, server closing..." );
            msg::notify_main_main( pMsg, MAIN_QUITING, false );
            ret = 1;
        }

        if( 0 == ret )
        {
            net_tcp::set_server_num( nNum );
        }

        for( int i = 0; ( 0 == ret ) && ( i < nNum ); ++i )
        {
            helper_start_http_server( nPorts[i] );
        }

        return ret;
    }

    int msg_main::start( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( ( 0 == ret ) && state::starting() )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            state::set_starting();
        }

        if( 0 == ret )
        {
            msg::notify_main_main( pMsg, MAIN_START_HTTP_SERVER, false );
        }

        return ret;
    }

    int msg_main::quit( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( ( 0 == ret ) && state::quitting() )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            state::set_quitting();
        }

        if( 0 == ret )
        {
            LOG4( "quit" );
            pMsg->set_int( 0, NET_QUITTING );
            msg::notify_net( pMsg, false );
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int msg_main::on_started( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( ( 0 == ret ) && state::running() )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            state::set_running();
        }

        return ret;
    }

    int msg_main::on_quitted( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( ( 0 == ret ) && state::stopped() )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            state::set_stopped();
        }

        return ret;
    }

    int msg_main::on_heart( xos::i_msg *& pMsg )
    {
        int ret = 0;

        if( ( 0 == ret ) && !state::can_send_heart() )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            pMsg->set_int( 0, NET_HEART );
            msg::notify_net( pMsg, false );
        }

        if( ( 0 == ret ) && !state::running() )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            mgr::xos()->log()->flush();
        }

        return ret;
    }

} // cat
