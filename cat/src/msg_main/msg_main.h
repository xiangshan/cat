/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MSG_MAIN_MSG_MAIN_H__
#define __MSG_MAIN_MSG_MAIN_H__

namespace cat
{

    class msg_main
    {
    public:
        msg_main();
        ~msg_main();

    protected:
        int start_http_server( xos::i_msg *& pMsg );
        int start( xos::i_msg *& pMsg );
        int quit( xos::i_msg *& pMsg );
        int helper_start_http_server( int nPort );

    protected:
        int on_started( xos::i_msg *& pMsg );
        int on_quitted( xos::i_msg *& pMsg );
        int on_heart( xos::i_msg *& pMsg );

    public:
        int proc( xos::i_msg *& pMsg );

    };

} // cat

#endif // __MSG_MAIN_MSG_MAIN_H__
