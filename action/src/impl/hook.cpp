/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../settings/head.h"
#include "../config/head.h"
#include "impl.h"
#include "hook.h"

namespace action
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    hook::hook()
    {
    }

    hook::~hook()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int hook::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = settings::init();
        }

        if( 0 == ret )
        {
            ret = config::init();
        }

        if( 0 == ret )
        {
            ret = impl::init_all();
        }

        return ret;
    }

    int hook::term()
    {
        int ret = 0;

        impl::term_all();
        config::term();
        settings::term();

        return ret;
    }

} // action
