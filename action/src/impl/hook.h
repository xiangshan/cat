/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_HOOK_H__
#define __IMPL_HOOK_H__

namespace action
{

    class hook
    {
    public:
        hook();
        ~hook();

    public:
        static int init();
        static int term();

    };

} // action

#endif // __IMPL_HOOK_H__
