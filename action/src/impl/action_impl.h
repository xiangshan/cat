/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_AGG_BASE_H__
#define __OBJECTS_AGG_BASE_H__

#define CLASS_NAME( x ) ( #x )

#include "../impl/container.h"
#include "../config/head.h"

namespace action
{

    template< class CHILD >
    class action_impl : public icat::i_action
    {
    public:
        typedef xos::com_object_agg_only< CHILD > T;

    public:
        action_impl( const char * lpszChildClassName ) : m_pszChildClassName( lpszChildClassName )
        {
            m_pProperty = 0;
        }
        ~action_impl()
        {
            xos_stl::release_interface( m_pProperty );
        }

    public:
        icat::i_plugin::enumRetCode proc( icat::i_task * pTask )
        {
            icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
            CHILD * pThis = static_cast< CHILD* >( this );

            // ����
            {
                pThis->proc_task( pTask );
            }

            // ����
            if( m_ret_code == "async" )
            {
                result = icat::i_plugin::RET_CODE_ASYNC;
            }
            else if( m_ret_code == "quit" )
            {
                result = icat::i_plugin::RET_CODE_QUIT;
            }
            else if( m_ret_code == "json" )
            {
                fmt_json( pTask );
            }
            else if( !m_ret_code.empty() )
            {
                result = call( pTask );
            }

            return result;
        }
        int reset()
        {
            int ret = 0;
            term();
            return ret;
        }

    public:
        int put_back_to_pool( CHILD * pT, bool bLock )
        {
            int ret = 0;
            delete pT;
            return ret;
        }
        int init()
        {
            CHILD * pThis = static_cast< CHILD* >( this );
            int ret = 0;

            if( 0 == ret )
            {
                ret = container()->common()->create( xos_common::OBJ_PROPERTY, ( void** )&m_pProperty );
            }

            if( 0 == ret )
            {
                ret = pThis->init_obj();
            }

            return ret;
        }
        int term()
        {
            CHILD * pThis = static_cast< CHILD* >( this );
            int ret = 0;

            if( 0 == ret )
            {
                m_pProperty->release_all();
            }

            if( 0 == ret )
            {
                ret = pThis->term_obj();
            }

            m_ret_code = "";

            return ret;
        }

    public:
        xos_common::i_property * prop()
        {
            return m_pProperty;
        }

    protected:
        int fmt_json( icat::i_task * pTask )
        {
            int ret = 0;

            icat::i_response * pResponse = pTask->get_response();
            xos_common::i_property * pResTag = pResponse->get_tag();
            xos::i_big_buf * pBigBuf = mgr::container()->big_buf();
            char * lpszBuf = pBigBuf->get_data( 0, 0, 0 );
            {
                int nSize = xos::i_big_buf::BUF_SIZE;
                int nLen = 0;
                lpszBuf[0] = 0;
                mgr::container()->json()->prop_to_json( prop(), lpszBuf, nSize, &nLen );
                pTask->add_ret_data( lpszBuf, nLen );
            }
            {
                pResTag->set( xos_http::HTTP_CONTENT_TYPE, "application/json" );
            }
            xos_stl::release_interface( pBigBuf );

            return ret;
        }
        icat::i_plugin::enumRetCode call( icat::i_task * pTask )
        {
            icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
            xos::i_crt * pCrt = container()->crt();
            char file[1024];
            file[0] = 0;

            pCrt->strcpy( file, sizeof( file ), m_ret_code.c_str() );
            pCrt->strcat( file, sizeof( file ), ".jsp" );

            result = pTask->get_chain()->call_jsp( file );

            return result;
        }
        void set_ret_code( const char * lpszRetCode )
        {
            m_ret_code = lpszRetCode;
        }

    private:
        xos_common::i_property * m_pProperty;
        const char * m_pszChildClassName;
        std::string m_ret_code;

    };

} // action

#endif // __OBJECTS_AGG_BASE_H__
