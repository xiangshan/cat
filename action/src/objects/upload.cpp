/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "upload.h"

namespace action
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    upload::upload() : BASE( CLASS_NAME( upload ) )
    {
        init_data();
    }

    upload::~upload()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    void upload::proc_task( icat::i_task * pTask )
    {
        icat::i_response * pResponse = pTask->get_response();
        xos_common::i_property * pResTag = pResponse->get_tag();
        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pReqTag = pRequest->get_tag();
        xos_common::i_property * pFormData = 0;
        int ret = 0;
        xos::i_buf * pBufHtml = container()->buf();
        xos::i_buf * pBuf = container()->buf();

        if( 0 == ret )
        {
            const char * lpszCmd = pReqTag->str( xos_http::HTTP_REQUEST_CMD );
            if( !lpszCmd || ( 0 != container()->crt()->strcmp( lpszCmd, "POST" ) ) )
            {
                pBuf->add_str( "not http post" );
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            pFormData = pReqTag->prop( xos_http::MULTIPART_BODY_LIST );
            if( !pFormData )
            {
                pBuf->add_str( "not find post items" );
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            xos_common::i_property * pPropData = 0;
            const char * lpszKey = 0;
            for( pFormData->reset(); pFormData->name_data( &lpszKey, 0, 0, 0 ); pFormData->name_next() )
            {
                pPropData = pFormData->prop( lpszKey );
                show_form_data( pBuf, pPropData );
            }
        }

        {
            char * lpszHtml = pBufHtml->get_data( 0, 0, 0 );
            char * lpszData = pBuf->get_data( 0, 0, 0 );
            int nLen = pBuf->get_len( 0 );
            lpszData[nLen] = 0;
            int nHtmlLen = 0;
            container()->http()->fmt_simple_html( lpszHtml, xos::i_buf::BUF_SIZE, &nHtmlLen, "upload info", lpszData );
            pBufHtml->set_len( nHtmlLen );
            pTask->add_ret_data( pBufHtml );
        }

        pResTag->set( xos_http::HTTP_CONTENT_TYPE, "text/html" );
        xos_stl::release_interface( pBufHtml );
        xos_stl::release_interface( pBuf );
    }

    int upload::show_form_data( xos::i_buf * pBuf, xos_common::i_property * pPropData )
    {
        int ret = 0;

        const char * lpszKey = pPropData->str( xos_http::MULTIPART_BODY_KEY );
        xos_common::i_variant * pVT = 0;

        if( 0 == ret )
        {
            pBuf->add_str( "\r\nkey = " );
            pBuf->add_str( lpszKey );
        }

        if( ( 0 == ret ) && ( pVT = pPropData->vt( xos_http::MULTIPART_BODY_VALUE ) ) )
        {
            char buf[4096];
            container()->crt()->sprintf( buf, sizeof( buf ), ", type = value, value = %s<br>", pVT->str() );
            pBuf->add_str( buf );
            ret = 1;
        }

        if( ( 0 == ret ) && ( pVT = pPropData->vt( xos_http::MULTIPART_BODY_FILE ) ) )
        {
            xos::i_list * pList = ( xos::i_list * )pVT->obj();
            int nSize = 0;
            for( pList->reset(); pList->data(); pList->next() )
            {
                xos::i_buf * pBuf = ( xos::i_buf * )pList->data();
                nSize += pBuf->get_len( 0 );
            }
            char buf[4096];
            container()->crt()->sprintf( buf, sizeof( buf ), ", type = file, file size = %d<br>", nSize );
            pBuf->add_str( buf );
            ret = 1;
        }

        if( ( 0 == ret ) && ( pVT = pPropData->vt( xos_http::MULTIPART_BODY_LIST ) ) )
        {
            xos::i_list * pList = ( xos::i_list * )pVT->obj();
            char buf[4096];
            int nNum = pList->size();
            container()->crt()->sprintf( buf, sizeof( buf ), ", type = list, list item num = %d<br>", nNum );
            pBuf->add_str( buf );
            ret = 1;
        }

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int upload::init_data()
    {
        int ret = 0;
        return ret;
    }

    int upload::init_obj()
    {
        int ret = 0;
        return ret;
    }

    int upload::term_obj()
    {
        int ret = 0;
        init_data();
        return ret;
    }

} // action
