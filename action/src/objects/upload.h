/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_UPLOAD_H__
#define __OBJECTS_UPLOAD_H__

#include "../impl/action_impl.h"

namespace action
{

    class upload : public action_impl< upload >
    {
    public:
        typedef action_impl< upload > BASE;

    public:
        upload();
        ~upload();

        // 
        // icat::i_plugin methods
        // 
    public:
        void proc_task( icat::i_task * pTask );

    protected:
        int show_form_data( xos::i_buf * pBuf, xos_common::i_property * pPropData );
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // action

#endif // __OBJECTS_UPLOAD_H__
