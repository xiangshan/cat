/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_QUIT_H__
#define __OBJECTS_QUIT_H__

#include "../impl/action_impl.h"

namespace action
{

    class quit : public action_impl< quit >
    {
    public:
        typedef action_impl< quit > BASE;

    public:
        quit();
        ~quit();

    public:
        void proc_task( icat::i_task * pTask );

    protected:
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // action

#endif // __OBJECTS_QUIT_H__
