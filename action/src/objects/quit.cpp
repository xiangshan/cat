/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "quit.h"

namespace action
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    quit::quit() : BASE( CLASS_NAME( quit ) )
    {
        init_data();
    }

    quit::~quit()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    void quit::proc_task( icat::i_task * pTask )
    {
        {
            char html[4096];
            html[0] = 0;
            container()->http()->fmt_simple_html( html, sizeof( html ), 0, "quit", "quit ok" );
            pTask->add_ret_str( html );
        }
        BASE::set_ret_code( "quit" );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int quit::init_data()
    {
        int ret = 0;
        return ret;
    }

    int quit::init_obj()
    {
        int ret = 0;
        return ret;
    }

    int quit::term_obj()
    {
        int ret = 0;
        init_data();
        return ret;
    }

} // action
