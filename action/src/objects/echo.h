/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_ECHO_H__
#define __OBJECTS_ECHO_H__

#include "../impl/action_impl.h"

namespace action
{

    class echo : public action_impl< echo >
    {
    public:
        typedef action_impl< echo > BASE;

    public:
        echo();
        ~echo();

    public:
        void proc_task( icat::i_task * pTask );

    protected:
        void default_handler( icat::i_task * pTask );
        void login( icat::i_task * pTask );
        void test( icat::i_task * pTask );
        void json( icat::i_task * pTask );
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // action

#endif // __OBJECTS_ECHO_H__
