/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "echo.h"

namespace action
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    echo::echo() : BASE( CLASS_NAME( echo ) )
    {
        init_data();
    }

    echo::~echo()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    void echo::proc_task( icat::i_task * pTask )
    {
        icat::i_chain * pChain = pTask->get_chain();
        const char * lpszMethod = pChain->method();

        xos::i_crt * pCrt = container()->crt();

        if( 0 == pCrt->strcmp( lpszMethod, "login" ) )
        {
            login( pTask );
        }
        else if( 0 == pCrt->strcmp( lpszMethod, "test" ) )
        {
            test( pTask );
        }
        else if( 0 == pCrt->strcmp( lpszMethod, "json" ) )
        {
            json( pTask );
        }
        else
        {
            default_handler( pTask );
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    
    void echo::default_handler( icat::i_task * pTask )
    {
        prop()->set( "xoskit", "supper good from action.echo.default method" );
        prop()->set( "score", 100 );
        BASE::set_ret_code( "default_success" );
    }

    void echo::login( icat::i_task * pTask )
    {
        const char * lpszPassword = pTask->str( "password" );
        const char * lpszAccount = pTask->str( "account" );
        xos::i_crt * pCrt = mgr::container()->crt();

        if( ( 0 == pCrt->strcmp( lpszAccount, "gaohh" ) ) && ( 0 == pCrt->strcmp( lpszPassword, "123456" ) ) )
        {
            BASE::set_ret_code( "login_success" );
        }
        else
        {
            BASE::set_ret_code( "login_page" );
        }
    }

    void echo::test( icat::i_task * pTask )
    {
        prop()->set( "xoskit", "supper good from action.echo.test method" );
        prop()->set( "score", 100 );
        BASE::set_ret_code( "test_success" );
    }

    void echo::json( icat::i_task * pTask )
    {
        prop()->set( "xoskit", "supper good from action.echo.test method" );
        prop()->set( "score", 100 );
        prop()->set( "name", "gaohh" );
        prop()->set( "password", "123456" );
        BASE::set_ret_code( "json" );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int echo::init_data()
    {
        int ret = 0;
        return ret;
    }

    int echo::init_obj()
    {
        int ret = 0;
        return ret;
    }

    int echo::term_obj()
    {
        int ret = 0;
        init_data();
        return ret;
    }

} // action
