/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __TOOLS_THREAD_LOCK_H__
#define __TOOLS_THREAD_LOCK_H__

namespace action
{
    class thread_lock
    {
    public:
        thread_lock();
        ~thread_lock();

    protected:
        xos::i_lock * m_pLock;

    public:
        int un_lock();
        int lock();
    };
} // action

#endif // __TOOLS_THREAD_LOCK_H__
