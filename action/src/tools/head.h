/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __TOOLS_HEAD_H__
#define __TOOLS_HEAD_H__

#include "thread_lock.h"
#include "tools.h"

#endif // __TOOLS_HEAD_H__
