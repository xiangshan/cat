/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __FA7B2694_E3CB_4255_AC42_619665E780F7__
#define __FA7B2694_E3CB_4255_AC42_619665E780F7__

#include "../../../interface/include/declare.h"

namespace action
{
    const char * cls_module_id = "18F35DD5_1FF1_4B5D_9000_7655E2771964";
    const char * cls_module_name = "module";
}

#endif // __FA7B2694_E3CB_4255_AC42_619665E780F7__
