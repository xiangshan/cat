/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __5CC1694D_30CE_4B58_BB4E_9BE001B4C4A9__
#define __5CC1694D_30CE_4B58_BB4E_9BE001B4C4A9__

namespace action
{
    extern const char * cls_module_id;
    extern const char * cls_module_name;
}

#endif // __5CC1694D_30CE_4B58_BB4E_9BE001B4C4A9__
