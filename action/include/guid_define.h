/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __2F18882D_2C53_4DDC_88CA_67164557C5D7__
#define __2F18882D_2C53_4DDC_88CA_67164557C5D7__

#include "../src/interface/guid_define.h"

#endif // __2F18882D_2C53_4DDC_88CA_67164557C5D7__
