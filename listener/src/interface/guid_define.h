/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __5A492762_601C_4513_8EAA_935B75736E84__
#define __5A492762_601C_4513_8EAA_935B75736E84__

#include "../../../interface/include/declare.h"

namespace listener
{
    const char * cls_module_id = "304E1B59_FB13_4C7F_BF89_2061B64DFBDD";
    const char * cls_module_name = "module";
}

#endif // __5A492762_601C_4513_8EAA_935B75736E84__
