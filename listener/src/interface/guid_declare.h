/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __DE299D67_F145_41FA_9A29_A074F86D56E6__
#define __DE299D67_F145_41FA_9A29_A074F86D56E6__

namespace listener
{
    extern const char * cls_module_id;
    extern const char * cls_module_name;
}

#endif // __DE299D67_F145_41FA_9A29_A074F86D56E6__
