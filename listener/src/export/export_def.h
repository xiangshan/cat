/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __EXPORT_EXPORT_DEF_H__
#define __EXPORT_EXPORT_DEF_H__

#if defined( XOS_WIN32 ) || defined( XOS_WIN64 )
    #define EXPORT_FUNCTION extern "C" __declspec( dllexport )
#else
    #define EXPORT_FUNCTION extern "C"
#endif

#endif // __EXPORT_EXPORT_DEF_H__
