/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __TOOLS_TOOLS_H__
#define __TOOLS_TOOLS_H__

////////////////////////////////////////////////////////////////////////
// 

namespace listener
{
    namespace tools
	{
		int full_path_file( char * lpszFullFile, int nBufSize, const char * lpszFile );
        int full_path( char * lpszFullPath, int nBufSize );
    }
} // listener

#endif // __TOOLS_TOOLS_H__
