/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_RECV_H__
#define __OBJECTS_RECV_H__

#include "../impl/listener_impl.h"

namespace listener
{

    class recv : public listener_impl< recv >
    {
    public:
        typedef listener_impl< recv > BASE;

    public:
        recv();
        ~recv();

        // 
        // icat::i_listener methods
        // 
    public:
        icat::i_plugin::enumRetCode proc_task( icat::i_task * pTask );

    protected:
        icat::i_plugin::enumRetCode come( icat::i_task * pTask, icat::i_chain * pChain, 
            icat::i_request * pRequest, 
            icat::i_response * pResponse );
        icat::i_plugin::enumRetCode go( icat::i_task * pTask, icat::i_chain * pChain, 
            icat::i_request * pRequest, 
            icat::i_response * pResponse );
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // listener

#endif // __OBJECTS_RECV_H__
