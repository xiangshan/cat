/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../macro/head.h"
#include "send.h"

namespace listener
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    send::send() : BASE( CLASS_NAME( send ) )
    {
        init_data();
    }

    send::~send()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode send::proc_task( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        icat::i_response * pResponse = pTask->get_response();
        icat::i_request * pRequest = pTask->get_request();
        icat::i_chain * pChain = pTask->get_chain();

        if( !pChain->is_go_back() )
        {
            result = come( pTask, pChain, pRequest, pResponse );
        }
        else
        {
            result = go( pTask, pChain, pRequest, pResponse );
        }

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 
    
    icat::i_plugin::enumRetCode send::come( icat::i_task * pTask, icat::i_chain * pChain, 
        icat::i_request * pRequest, 
        icat::i_response * pResponse )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        const char * lpszAdvise = 0;
        const char * lpszEvent = 0;
        const char * lpszType = 0;
        static int N = 0;

        pChain->get_listen_event( lpszType, lpszEvent, lpszAdvise );

        {
            char buf[4096];
            container()->crt()->sprintf( buf, sizeof( buf ), "module = %s, listener = %s, %02d, type = %s, event = %s, advise = %s",
                cls_module_name,
                "send",
                N,
                lpszType,
                lpszEvent,
                lpszAdvise );
            LOG4( buf );
        }

        N++;

        return result;
    }

    icat::i_plugin::enumRetCode send::go( icat::i_task * pTask, icat::i_chain * pChain, 
        icat::i_request * pRequest, 
        icat::i_response * pResponse )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        const char * lpszAdvise = 0;
        const char * lpszEvent = 0;
        const char * lpszType = 0;
        static int N = 0;

        pChain->get_listen_event( lpszType, lpszEvent, lpszAdvise );

        {
            char buf[4096];
            container()->crt()->sprintf( buf, sizeof( buf ), "module = %s, listener = %s, %02d, type = %s, event = %s, advise = %s",
                cls_module_name,
                "send",
                N,
                lpszType,
                lpszEvent,
                lpszAdvise );
            LOG4( buf );
        }

        N++;

        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int send::init_data()
    {
        int ret = 0;
        return ret;
    }

    int send::init_obj()
    {
        int ret = 0;
        return ret;
    }

    int send::term_obj()
    {
        int ret = 0;
        init_data();
        return ret;
    }

} // listener
