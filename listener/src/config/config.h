/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __CONFIG_CONFIG_H__
#define __CONFIG_CONFIG_H__

namespace listener
{

    class config
    {
    public:
        config();
        ~config();

    public:
        static xos_common::i_property * prop();
        static int init();
        static int term();

    };

} // listener

#endif // __CONFIG_CONFIG_H__
