/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_MGR_H__
#define __IMPL_MGR_H__

#define XOS_OBJ_ENTRY_LISTENER( x ) XOS_OBJ_ENTRY_AGG( x, listener::impl )

#include "../interface/guid_declare.h"
#include "../objects/head.h"
#include "impl.h"

namespace listener
{

    class mgr : public icat::i_plugin_mgr
    {
    public:
        typedef xos::com_object_no_ref< mgr > T;

    public:
        XOS_BEGIN_OBJ_MAP( icat::i_plugin, icat::i_plugin_id )
            XOS_OBJ_ENTRY_LISTENER( recv )
            XOS_OBJ_ENTRY_LISTENER( send )
        XOS_END_OBJ_MAP()

        XOS_BEGIN_COM_MAP( mgr, icat::i_plugin_mgr )
            XOS_COM_INTERFACE_ENTRY( icat::i_plugin_mgr )
        XOS_END_COM_MAP()

    public:
        mgr();
        ~mgr();

    public:
        static int static_init( xos_container::i_container * pContainer, xos::i_dynamic * pModule, const char * lpszModule, icat::i_plugin_mgr ** ppv );
        static xos_container::i_container * container();
        static mgr * get();

    public:
        int put_back_to_pool( mgr * pT, bool bLock );
        int init();
        int term();

        // 
        // i_plugin_mgr methods.create is generate by XOS_BEGIN_OBJ_MAP.
        // 
    public:
        int set_module( xos::i_dynamic * pModule );
        xos::i_dynamic * get_module();

    };

} // listener

#endif // __IMPL_MGR_H__
