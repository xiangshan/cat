/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __E0A1FF65_7381_4EDB_B7D5_CB334D467124__
#define __E0A1FF65_7381_4EDB_B7D5_CB334D467124__

#include "../src/interface/guid_define.h"

#endif // __E0A1FF65_7381_4EDB_B7D5_CB334D467124__
