/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __95477F23_09C3_4FDC_9C7C_5ACE30F64D9D__
#define __95477F23_09C3_4FDC_9C7C_5ACE30F64D9D__

#include "../src/interface/guid_declare.h"

#endif // __95477F23_09C3_4FDC_9C7C_5ACE30F64D9D__
