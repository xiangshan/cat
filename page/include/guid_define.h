/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __A2A95F6C_C02F_4C8F_94A8_8E7C7A8400C5__
#define __A2A95F6C_C02F_4C8F_94A8_8E7C7A8400C5__

#include "../src/interface/guid_define.h"

#endif // __A2A95F6C_C02F_4C8F_94A8_8E7C7A8400C5__
