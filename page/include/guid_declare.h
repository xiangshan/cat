/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __E2A8F26D_6B7B_4E22_9988_D74671F836A3__
#define __E2A8F26D_6B7B_4E22_9988_D74671F836A3__

#include "../src/interface/guid_declare.h"

#endif // __E2A8F26D_6B7B_4E22_9988_D74671F836A3__
