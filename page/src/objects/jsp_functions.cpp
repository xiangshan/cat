/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../macro/head.h"
#include "jsp_functions.h"

namespace jsp
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int supper_variable_to_html( icat::i_task * pTask, const char * lpszVariable )
    {
        int ret = 0;
        const char * lpszStr = pTask->str( lpszVariable );
        if( lpszStr )
        {
            pTask->add_ret_str( lpszStr );
        }
        return ret;
    }

    int html_to_html( icat::i_task * pTask, xos::i_buf * pBuf )
    {
        int ret = 0;
        pTask->add_ret_data( pBuf->get_data( 0, 0, 0 ), pBuf->get_len( 0 ) );
        return ret;
    }

} // jsp
