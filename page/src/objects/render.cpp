/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "jsp_functions.h"

const int JSP_BODY_NUM = 1;
int g_jsp_body_num = JSP_BODY_NUM;
xos::i_buf * g_pHtmlBuf[JSP_BODY_NUM] = { 0 };
const char * g_pszBase64Html[JSP_BODY_NUM] = 
{
"PCFET0NUWVBFIGh0bWw+CjxodG1sPgogIDxoZWFkPgogIAk8bWV0YSBodHRwLWVxdWl2PSJDb250ZW50LXR5cGUiIGNvbnRlbnQ9InRleHQvaHRtbDsgY2hhcnNldD11dGYtOCI+CiAgICA8dGl0bGU+bG9naW4gcGFnZTwvdGl0bGU+CiAgPC9oZWFkPgogIAogIDxib2R5PgogIAk8Zm9ybSBhY3Rpb249ImVjaG8hbG9naW4uYWN0aW9uIg0KICAgICAgICBlbmN0eXBlPSJtdWx0aXBhcnQvZm9ybS1kYXRhIg0KICAgICAgICBtZXRob2Q9InBvc3QiPgogIAkJPGxhYmVsPueZu+mZhuezu+e7nzwvbGFiZWw+PGJyLz4KICAgICAgICA8bGFiZWw+5ZCN56ewPC9sYWJlbD4KICAJCTxpbnB1dCB0eXBlPSJ0ZXh0IiBuYW1lPSJhY2NvdW50Ii8+PGJyLz4KICAgICAgICA8bGFiZWw+5a+G56CBPC9sYWJlbD4KICAJCTxpbnB1dCB0eXBlPSJwYXNzd29yZCIgbmFtZT0icGFzc3dvcmQiLz48YnIvPgogIAkJPGlucHV0IHR5cGU9InN1Ym1pdCIgdmFsdWU9ImxvZ2luIj48L3N1Ym1pdD48YnIvPgogICAgICAgIDxpbnB1dCB0eXBlPSJyZXNldCI+CiAgCTwvZm9ybT4KICA8L2JvZHk+CjwvaHRtbD4K"
};

void page_render( icat::i_task * pTask,
    icat::i_request * pRequest,
    icat::i_response * pResponse,
    xos_common::i_property * pReqTag,
    xos_common::i_property * pResTag )
{
    using namespace jsp;
    html_to_html( pTask, g_pHtmlBuf[0] );
}
