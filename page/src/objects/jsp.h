/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_JSP_H__
#define __OBJECTS_JSP_H__

#include "../impl/page_impl.h"

namespace page
{

    class jsp : public page_impl< jsp >
    {
    public:
        typedef page_impl< jsp > BASE;

    public:
        jsp();
        ~jsp();

    public:
        static int static_init();
        static int static_term();
        static int init_html();
        static int term_html();

    public:
        icat::i_plugin::enumRetCode proc_task( icat::i_task * pTask );

    protected:
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // page

#endif // __OBJECTS_JSP_H__
