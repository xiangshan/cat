/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../macro/head.h"
#include "../impl/head.h"
#include "render.h"
#include "jsp.h"

namespace page
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    jsp::jsp() : BASE( CLASS_NAME( jsp ) )
    {
        init_data();
    }

    jsp::~jsp()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int jsp::static_init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = init_html();
        }

        return ret;
    }

    int jsp::static_term()
    {
        int ret = 0;

        term_html();

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int jsp::init_html()
    {
        int ret = 0;

        xos_common::i_misc * pMisc = page::container()->common_misc();
        xos::i_crt * pCrt = page::container()->crt();

        for( int i = 0; i < g_jsp_body_num; ++i )
        {
            const char * lpszHtml = g_pszBase64Html[i];
            xos::i_buf * pBuf = g_pHtmlBuf[i];
            if( !lpszHtml || pBuf )
            {
                continue;
            }
            pBuf = mgr::container()->buf();
            char * lpszBuf = pBuf->get_data( 0, 0, 0 );
            int encode_len = pCrt->strlen( lpszHtml );
            pMisc->base64_decode( lpszHtml, encode_len, lpszBuf, xos::i_buf::BUF_SIZE );
            int decode_len = pCrt->strlen( lpszBuf );
            pBuf->set_len( decode_len );
            g_pHtmlBuf[i] = pBuf;
        }

        return ret;
    }

    int jsp::term_html()
    {
        int ret = 0;

        for( int i = 0; i < g_jsp_body_num; ++i )
        {
            xos::i_buf * pBuf = g_pHtmlBuf[i];
            if( !pBuf )
            {
                continue;
            }
            xos_stl::release_interface( pBuf );
            g_pHtmlBuf[i] = 0;
        }

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    icat::i_plugin::enumRetCode jsp::proc_task( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;

        icat::i_response * pResponse = pTask->get_response();
        xos_common::i_property * pResTag = pResponse->get_tag();
        icat::i_request * pRequest = pTask->get_request();
        xos_common::i_property * pReqTag = pRequest->get_tag();

        {
            page_render( pTask, pRequest, pResponse, pReqTag, pResTag );
        }

        pResTag->set( xos_http::HTTP_CONTENT_TYPE, "text/html" );

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //

    int jsp::init_data()
    {
        int ret = 0;
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int jsp::init_obj()
    {
        int ret = 0;
        return ret;
    }

    int jsp::term_obj()
    {
        int ret = 0;

        init_data();

        return ret;
    }

} // page
