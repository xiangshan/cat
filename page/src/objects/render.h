/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_RENDER_H__
#define __OBJECTS_RENDER_H__

extern const char * g_pszBase64Html[];
extern xos::i_buf * g_pHtmlBuf[];
extern int g_jsp_body_num;

void page_render( icat::i_task * pTask,
    icat::i_request * pRequest,
    icat::i_response * pResponse,
    xos_common::i_property * pReqTag,
    xos_common::i_property * pResTag );

#endif // __OBJECTS_RENDER_H__
