/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_JSP_FUNCTIONS_H__
#define __OBJECTS_JSP_FUNCTIONS_H__

#include "../import/head.h"
#include "../macro/head.h"
#include "../impl/head.h"

namespace jsp
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    template< class T >
    inline int variable_to_html( icat::i_task * pTask, T v )
    {
        using namespace page;
        xos_common::i_variant * pVT = 0;
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::container()->common()->create( xos_common::OBJ_VARIANT, ( void** )&pVT );
        }

        if( 0 == ret )
        {
            pVT->set( v );
        }

        if( 0 == ret )
        {
            const char * pStr = pVT->str();
            pTask->add_ret_str( pStr );
        }

        xos_stl::release_interface( pVT );

        return ret;
    }

    int supper_variable_to_html( icat::i_task * pTask, const char * lpszVariable );
    int html_to_html( icat::i_task * pTask, xos::i_buf * pBuf );

} // jsp

#endif // __OBJECTS_JSP_FUNCTIONS_H__
