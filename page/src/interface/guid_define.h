/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __4B6D0081_8944_483B_91D5_F80D3BB1D8CA__
#define __4B6D0081_8944_483B_91D5_F80D3BB1D8CA__

#include "../../../interface/include/declare.h"

namespace page
{
    const char * cls_module_id = "DB2D8280_3810_48DD_A46C_39B8E1AC4A8B";
    const char * cls_module_name = "module";
}

#endif // __4B6D0081_8944_483B_91D5_F80D3BB1D8CA__
