/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/mgr.h"
#include "export.h"

#if defined( _DEBUG )
#if defined( XOS_WIN32 )
#include "../../../../xoskit/xos_lib/win32/vld/include/vld.h"
#elif defined( XOS_WIN64 )
#include "../../../../xoskit/xos_lib/x64/vld/include/vld.h"
#endif
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace page
{
    int create( xos_container::i_container * pContainer, xos::i_dynamic * pModule, 
        const char * lpszModule, 
        icat::i_plugin_mgr ** ppv )
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = mgr::static_init( pContainer, pModule, lpszModule, ppv );
        }

        return ret;
    }
} // page
