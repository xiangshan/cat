/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __EXPORT_EXPORT_H__
#define __EXPORT_EXPORT_H__

#include "export_def.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace page
{
    EXPORT_FUNCTION int create( xos_container::i_container * pContainer, xos::i_dynamic * pModule, 
        const char * lpszModule, 
        icat::i_plugin_mgr ** ppv );
}

#endif // __EXPORT_EXPORT_H__
