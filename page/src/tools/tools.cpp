/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "tools.h"

namespace page
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

	namespace tools
	{
		int full_path_file( char * lpszFullFile, int nBufSize, const char * lpszFile )
		{
			int ret = 0;
            full_path( lpszFullFile, nBufSize );
			container()->misc()->path_append( lpszFullFile, lpszFile );
			return ret;
		}

        int full_path( char * lpszFullPath, int nBufSize )
        {
            int ret = 0;
            xos::i_dynamic * pDll = mgr::get()->get_module();
            pDll->get_full_path( lpszFullPath, nBufSize );
            return ret;
        }
	}

} // page
