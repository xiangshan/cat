/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../impl/head.h"
#include "thread_lock.h"

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace page
{
    thread_lock::thread_lock()
    {
        container()->xos()->create( xos::i_xos::XOS_OBJ_SPIN_LOCK, ( void ** )&m_pLock );
    }

    thread_lock::~thread_lock()
    {
        xos_stl::release_interface( m_pLock );
    }

    int thread_lock::un_lock()
    {
        int nRet = 0;
        m_pLock->un_lock();
        return nRet;
    }

    int thread_lock::lock()
    {
        int nRet = 0;
        m_pLock->lock();
        return nRet;
    }

} // page
