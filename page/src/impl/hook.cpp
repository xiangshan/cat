/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../objects/head.h"
#include "impl.h"
#include "hook.h"

namespace page
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    hook::hook()
    {
    }

    hook::~hook()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int hook::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = impl::init_all();
        }

        if( 0 == ret )
        {
            ret = jsp::static_init();
        }

        return ret;
    }

    int hook::term()
    {
        int ret = 0;

        jsp::static_term();
        impl::term_all();

        return ret;
    }

} // page
