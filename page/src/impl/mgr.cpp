/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../macro/head.h"
#include "container.h"
#include "hook.h"
#include "impl.h"
#include "mgr.h"

namespace page
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    xos_container::i_container * container_ptr = 0;
    static mgr * plugin_mgr_ptr = 0;
    xos::i_dynamic * module_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    mgr::mgr()
    {
    }

    mgr::~mgr()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int mgr::static_init( xos_container::i_container * pContainer, xos::i_dynamic * pModule, const char * lpszModule, icat::i_plugin_mgr ** ppv )
    {
        int ret = 0;

        if( ( 0 == ret ) && plugin_mgr_ptr )
        {
            ret = 1;
        }

        if( 0 == ret )
        {
            container_ptr = pContainer;
            module_ptr = pModule;
        }

        if( 0 == ret )
        {
            plugin_mgr_ptr = new mgr::T;
            if( !plugin_mgr_ptr )
            {
                ret = 1;
            }
        }

        if( 0 == ret )
        {
            plugin_mgr_ptr->set_module_name( lpszModule );
        }

        if( 0 == ret )
        {
            ret = plugin_mgr_ptr->init();
            if( 0 != ret )
            {
                ret = 1;
            }
        }
        
        if( 0 != ret )
        {
            xos_stl::release_interface( plugin_mgr_ptr );
            container_ptr = 0;
            module_ptr = 0;
        }
        else
        {
            *ppv = plugin_mgr_ptr;
        }

        return ret;
    }

    xos_container::i_container * mgr::container()
    {
        return container_ptr;
    }

    mgr * mgr::get()
    {
        return plugin_mgr_ptr;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int mgr::put_back_to_pool( mgr * pT, bool bLock )
    {
        int ret = 0;
        delete pT;
        return ret;
    }

    int mgr::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            container()->log()->add_log_module( LOG_NAME, 1 );
            container()->log()->add_log_module( LOG_NAME, 2 );
            container()->log()->add_log_module( LOG_NAME, 3 );
            container()->log()->add_log_module( LOG_NAME, 4 );
        }

        if( 0 == ret )
        {
            ret = hook::init();
        }

        return ret;
    }

    int mgr::term()
    {
        int ret = 0;

        hook::term();

        container_ptr = 0;
        plugin_mgr_ptr = 0;
        module_ptr = 0;

        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int mgr::set_module( xos::i_dynamic * pModule )
    {
        int ret = 0;
        module_ptr = pModule;
        return ret;
    }

    xos::i_dynamic * mgr::get_module()
    {
        return module_ptr;
    }

} // page
