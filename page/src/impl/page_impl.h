/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_AGG_BASE_H__
#define __OBJECTS_AGG_BASE_H__

#define CLASS_NAME( x ) ( #x )

#include "../impl/container.h"

namespace page
{

    template< class CHILD >
    class page_impl : public icat::i_page
    {
    public:
        typedef xos::com_object_agg_only< CHILD > T;

    public:
        page_impl( const char * lpszChildClassName ) : m_pszChildClassName( lpszChildClassName )
        {
            m_pProperty = 0;
        }
        ~page_impl()
        {
            xos_stl::release_interface( m_pProperty );
        }

    public:
        icat::i_plugin::enumRetCode proc( icat::i_task * pTask )
        {
            icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
            CHILD * pThis = static_cast< CHILD* >( this );
            int ret = 0;

            if( 0 == ret )
            {
                result = pThis->proc_task( pTask );
            }

            return result;
        }
        int reset()
        {
            int ret = 0;
            term();
            return ret;
        }

    public:
        int put_back_to_pool( CHILD * pT, bool bLock )
        {
            int ret = 0;
            delete pT;
            return ret;
        }
        int init()
        {
            int ret = 0;

            CHILD * pThis = static_cast< CHILD* >( this );

            if( 0 == ret )
            {
                ret = container()->common()->create( xos_common::OBJ_PROPERTY, ( void** )&m_pProperty );
            }

            if( 0 == ret )
            {
                ret = pThis->init_obj();
            }

            return ret;
        }
        int term()
        {
            CHILD * pThis = static_cast< CHILD* >( this );
            int ret = 0;

            if( 0 == ret )
            {
                m_pProperty->release_all();
            }

            if( 0 == ret )
            {
                ret = pThis->term_obj();
            }

            return ret;
        }

    public:
        xos_common::i_property * prop()
        {
            return m_pProperty;
        }

    private:
        xos_common::i_property * m_pProperty;
        const char * m_pszChildClassName;

    };

} // page

#endif // __OBJECTS_AGG_BASE_H__
