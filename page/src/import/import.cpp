/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/

#if defined( XOS_WIN32 ) || defined( XOS_WIN64 )
#pragma warning(disable:4828)
#endif

#include "../import/head.h"

#include "../../../../xoskit/xos_core/include/guid_define.h"
#include "../../../../xoskit/xos_http/include/guid_define.h"
#include "../../../interface/include/define.h"
#include "../interface/guid_define.h"
