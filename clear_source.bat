:: 删除vs2015生成的文件
del *.VC.db /s

:: 删除suo文件
del *.suo /a:h /s

:: 删除usr文件
del *.user /s

:: 删除ncb文件
del *.ncb /s

:: 删除linux临时文件
del *.o /s
del *.d /s

:: 删除python临时文件
del *.pyc /s
