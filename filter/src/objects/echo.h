/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_ECHO_H__
#define __OBJECTS_ECHO_H__

#include "../impl/filter_impl.h"

namespace filter
{

    class echo : public filter_impl< echo >
    {
    public:
        typedef filter_impl< echo > BASE;

    public:
        echo();
        ~echo();

    public:
        icat::i_plugin::enumRetCode proc_task( icat::i_task * pTask );

    protected:
        icat::i_plugin::enumRetCode come( icat::i_task * pTask, icat::i_chain * pChain, 
            icat::i_request * pRequest, 
            icat::i_response * pResponse,
            const char * lpszAction,
            const char * lpszMethod );
        icat::i_plugin::enumRetCode go( icat::i_task * pTask, icat::i_chain * pChain, 
            icat::i_request * pRequest, 
            icat::i_response * pResponse,
            const char * lpszAction,
            const char * lpszMethod );
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // filter

#endif // __OBJECTS_ECHO_H__
