/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../config/head.h"
#include "../macro/head.h"
#include "echo.h"

namespace filter
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    echo::echo() : BASE( CLASS_NAME( echo ) )
    {
        init_data();
    }

    echo::~echo()
    {
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode echo::proc_task( icat::i_task * pTask )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        icat::i_response * pResponse = pTask->get_response();
        icat::i_request * pRequest = pTask->get_request();
        icat::i_chain * pChain = pTask->get_chain();

        if( !pChain->is_go_back() )
        {
            result = come( pTask, pChain, pRequest, pResponse, pChain->action(), pChain->method() );
        }
        else
        {
            result = go( pTask, pChain, pRequest, pResponse, pChain->action(), pChain->method() );
        }

        return result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    icat::i_plugin::enumRetCode echo::come( icat::i_task * pTask, icat::i_chain * pChain, 
        icat::i_request * pRequest, 
        icat::i_response * pResponse,
        const char * lpszAction,
        const char * lpszMethod )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        static int N = 0;
        {
            char buf[1024] = { 0 };
            container()->crt()->sprintf( buf, sizeof( buf ), "%s, action = %s, method = %s, is go back = %d, ret = %d",
                1 == ( N % 2 ) ? "async" : " sync",
                lpszAction, 
                lpszMethod,
                pChain->is_go_back(),
                N );
            LOG4( buf );
        }
        N++;
        return result;
    }

    icat::i_plugin::enumRetCode echo::go( icat::i_task * pTask, icat::i_chain * pChain, 
        icat::i_request * pRequest, 
        icat::i_response * pResponse,
        const char * lpszAction,
        const char * lpszMethod )
    {
        icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
        static int N = 0;
        {
            char buf[1024] = { 0 };
            container()->crt()->sprintf( buf, sizeof( buf ), "%s, action = %s, method = %s, is go back = %d, ret = %d",
                1 == ( N % 2 ) ? "async" : " sync",
                lpszAction, 
                lpszMethod,
                pChain->is_go_back(),
                N );
            LOG4( buf );
        }
        N++;
        return result;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int echo::init_data()
    {
        int ret = 0;
        return ret;
    }

    int echo::init_obj()
    {
        int ret = 0;
        return ret;
    }

    int echo::term_obj()
    {
        int ret = 0;
        init_data();
        return ret;
    }

} // filter
