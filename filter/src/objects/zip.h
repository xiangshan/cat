/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_ZIP_H__
#define __OBJECTS_ZIP_H__

#include "../impl/filter_impl.h"

namespace filter
{

    class zip : public filter_impl< zip >
    {
    public:
        typedef std::map< std::string, int > STR_MAP;
        typedef STR_MAP::iterator STR_ITER;

        typedef filter_impl< zip > BASE;

    public:
        zip();
        ~zip();

    public:
        icat::i_plugin::enumRetCode proc_task( icat::i_task * pTask );

    protected:
        int compress_size;
        STR_MAP m_map;

    protected:
        int compress( icat::i_response * pResponse );
        bool can_compress( const char * lpszFmt );
        int init_data();

    public:
        int init_obj();
        int term_obj();

    };

} // filter

#endif // __OBJECTS_ZIP_H__
