/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __B3E8246F_1238_4242_A8E6_C7D676BACCBD__
#define __B3E8246F_1238_4242_A8E6_C7D676BACCBD__

namespace filter
{
    extern const char * cls_module_id;
    extern const char * cls_module_name;
}

#endif // __B3E8246F_1238_4242_A8E6_C7D676BACCBD__
