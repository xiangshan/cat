/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __96BBBFC7_A822_4B93_AD06_3102A2B15B29__
#define __96BBBFC7_A822_4B93_AD06_3102A2B15B29__

#include "../../../interface/include/declare.h"

namespace filter
{
    const char * cls_module_id = "BDDB69C0_25EA_428D_B138_66A415F745DD";
    const char * cls_module_name = "module";
}

#endif // __96BBBFC7_A822_4B93_AD06_3102A2B15B29__
