/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __MACRO_MACRO_H__
#define __MACRO_MACRO_H__

#include "../impl/head.h"

// 
// ��־�궨��
// 
#define LOG_NAME                "FILTE"
//*
#define LOG1( fmt, ... )        container()->log()->log_info( LOG_NAME, 1, __FILE__, __LINE__, fmt, ##__VA_ARGS__ )
#define LOG2( fmt, ... )        container()->log()->log_info( LOG_NAME, 2, __FILE__, __LINE__, fmt, ##__VA_ARGS__ )
#define LOG3( fmt, ... )        container()->log()->log_info( LOG_NAME, 3, __FILE__, __LINE__, fmt, ##__VA_ARGS__ )
#define LOG4( fmt, ... )        container()->log()->log_info( LOG_NAME, 4, __FILE__, __LINE__, fmt, ##__VA_ARGS__ )
/*/
#define LOG1( fmt, ... )    
#define LOG2( fmt, ... )    
#define LOG3( fmt, ... )    
#define LOG4( fmt, ... )
//*/

#endif // __MACRO_MACRO_H__
