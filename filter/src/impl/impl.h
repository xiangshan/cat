/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __IMPL_IMPL_H__
#define __IMPL_IMPL_H__

#include "../tools/head.h"

namespace filter
{
    class impl : public xos_stl::mem_item< xos::com_object_no_ref< impl >, thread_lock >, 
        public xos::i_unknown
    {
    public:
        typedef xos::com_object_no_ref< impl > T;
        typedef xos_stl::mem_item< T, thread_lock > BASE;

    public:
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        static int get_addr( POOL *** pppPool, void *** pppGroup, LIST *** pppList, T *** pppObj, void *** pppFields );

    public:
        XOS_BEGIN_COM_MAP_WITHOUT_CLASS( impl )
            XOS_COM_INTERFACE_ENTRY( xos::i_unknown )
            XOS_COM_INTERFACE_ENTRY_MEMBER( icat::i_filter, m_pAggedObject )
            XOS_COM_INTERFACE_ENTRY_MEMBER( icat::i_plugin, m_pAggedObject )
            XOS_COM_INTERFACE_ENTRY_MEMBER( icat::i_agged, m_pAggedObject )
        XOS_END_COM_MAP()

    public:
        impl();
        ~impl();

    public:
        static T* get_from_pool( const char * lpszClsName );
        static void put_back_to_pool( T * pT, bool bLock );
        static int static_user_init();
        static int static_user_term();

    public:
        int set_agged_obj( icat::i_plugin * pObj );
        icat::i_plugin * get_agged_obj();
        int init();
        int term();

    protected:
        int init_data();

    protected:
        icat::i_plugin * m_pAggedObject;

    };

} // filter

#endif // __IMPL_IMPL_H__
