/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __OBJECTS_AGG_BASE_H__
#define __OBJECTS_AGG_BASE_H__

#define CLASS_NAME( x ) ( #x )

#include "../impl/container.h"
#include "../config/head.h"

namespace filter
{

    template< class CHILD >
    class filter_impl : public icat::i_filter
    {
    public:
        typedef xos::com_object_agg_only< CHILD > T;

    public:
        filter_impl( const char * lpszChildClassName ) : m_pszChildClassName( lpszChildClassName )
        {
        }
        ~filter_impl()
        {
        }

    public:
        icat::i_plugin::enumRetCode proc( icat::i_task * pTask )
        {
            icat::i_plugin::enumRetCode result = icat::i_plugin::RET_CODE_DONE;
            CHILD * pThis = static_cast< CHILD* >( this );
            int ret = 0;

            if( 0 == ret )
            {
                result = pThis->proc_task( pTask );
            }

            return result;
        }
        int reset()
        {
            int ret = 0;
            return ret;
        }

    public:
        int put_back_to_pool( CHILD * pT, bool bLock )
        {
            int ret = 0;
            delete pT;
            return ret;
        }
        int init()
        {
            int ret = 0;

            CHILD * pThis = static_cast< CHILD* >( this );
            xos::i_unknown * pUnk = 0;
            int nPriority = 0;

            if( 0 == ret )
            {
                nPriority = config::prop()->prop( "filters" )->prop( m_pszChildClassName )->it( "priority" );
                query_interface( xos::i_unknown_id, ( void** )&pUnk );
            }

            if( 0 == ret )
            {
                pUnk->set_priority( nPriority );
            }

            if( 0 == ret )
            {
                ret = pThis->init_obj();
            }

            return ret;
        }
        int term()
        {
            int ret = 0;

            CHILD * pThis = static_cast< CHILD* >( this );

            if( 0 == ret )
            {
                ret = pThis->term_obj();
            }

            return ret;
        }

    private:
        const char * m_pszChildClassName;

    };

} // filter

#endif // __OBJECTS_AGG_BASE_H__
