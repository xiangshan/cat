/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "../tools/tools.h"
#include "../impl/head.h"
#include "config.h"

namespace filter
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static xos_common::i_property * property_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    config::config()
    {
    }

    config::~config()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int config::init()
    {
        int ret = 0;

        char file[4096] = { 0 };

        if( 0 == ret )
        {
            tools::full_path_file( file, sizeof( file ), mgr::get()->module_name() );
            container()->crt()->strcat( file, sizeof( file ), ".xml" );
        }

        if( 0 == ret )
        {
            ret = container()->xml()->load_xml_file_to_property( file, ( xos::i_unknown** )&property_ptr, 0 );
        }

        return ret;
    }

    int config::term()
    {
        int ret = 0;
        xos_stl::release_interface( property_ptr );
        return ret;
    }

    xos_common::i_property * config::prop()
    {
        return property_ptr;
    }

} // filter
