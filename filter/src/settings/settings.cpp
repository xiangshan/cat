/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#include "../import/head.h"
#include "settings.h"

namespace filter
{

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    static settings * settings_ptr = 0;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    settings::settings()
    {
    }

    settings::~settings()
    {
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int settings::init_data()
    {
        int ret = 0;
        return ret;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 

    int settings::init()
    {
        int ret = 0;

        if( 0 == ret )
        {
            ret = xos_stl::init_obj( settings_ptr );
        }

        if( 0 == ret )
        {
            ret = settings_ptr->init_data();
        }

        return ret;
    }

    int settings::term()
    {
        int ret = 0;
        ret = xos_stl::term_obj( settings_ptr );
        return ret;
    }

    settings * settings::get()
    {
        return settings_ptr;
    }

} // filter
