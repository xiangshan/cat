/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __1A150043_04D0_4535_BCB4_ABA543F4698D__
#define __1A150043_04D0_4535_BCB4_ABA543F4698D__

#include "../src/interface/guid_declare.h"

#endif // __1A150043_04D0_4535_BCB4_ABA543F4698D__
