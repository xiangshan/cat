/*----------------------------------------------------------------------------------------
*
*  Copyright 2019, Gao Hai Hui, <fromzeropoint@126.com>.  All rights reserved.
*  https://gitee.com/helloworldghh/cat.git
*  Use of this source code is governed by a MIT license
*  that can be found in the License file.
*
----------------------------------------------------------------------------------------*/
#ifndef __079A6071_9CAC_4E0E_A643_9EC916161B49__
#define __079A6071_9CAC_4E0E_A643_9EC916161B49__

#include "../src/interface/guid_define.h"

#endif // __079A6071_9CAC_4E0E_A643_9EC916161B49__
